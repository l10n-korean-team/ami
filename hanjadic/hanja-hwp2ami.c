
/*
 황 치덕 (hwang@mizi.co.kr)
 hwp 한자사전으로부터 ami에서 쓸수있는 한자사전형태로 변경하는
 프로그램. 결과파일이 완성형 코드를 쓰는 관계로 많은 단어가 삭제됩니다.

 hwp 한자사전의 내부 형식은 박재현님의 벼루와 같이 쓰도록 만든
 dic2cd프로그램을 참조했습니다.
 hwp의 버전에 따라 이 프로그램이 작동을 안할 가능성이 있습니다.
 왜냐하면 한컴측에서 이 파일에관한 포멧을 공개한 것이 아니어서
 정확한 spec을 가지고 짠것이 아님니다.

 컴파일:
 make

 사용법:
    hanja-hwp2ami hwp한자사전 > hanja.dic
    시스템 사양에 따라서 시간이 조금(>1분) 걸릴수도 있습니다.
    그런 이후 이 hanja.dic를 ami가 찾는 위치로 옮겨주시면 됩니다.
    Intel machine이 아닌 곳에서는 정상작동하지않을 가능성이 있습니다.

*/

#include <glib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>


#define page_size 94

#define MAX_WORDSIZE 20
#define MAX_HANJA 200

#include "ksctable.c"

#define is_hanja(x) ((x)>=0x4000 && (x) < 0x8000)
#define is_hangul(x) ((x) >= 0x8000)

guint16
to_hanja(guint16 x)
{
    guint page, offset;
    if (x < 0x4000) return 0;
    if (x >= 0x5318) return 0;
    x = x - 0x4000;
    page = x / page_size;
    offset = x % page_size;
    return (page+0xca) << 8 | (offset + 0xa1);
}



int
find_ksc_index(guint16 x)
{
    register int b, e, i;
    if (x == ksc_table[0]) return 0;
    b = 0;
    e = sizeof(ksc_table)/sizeof(ksc_table[0]) - 1;
    i = (b + e)/2;
    while(b + 1 < e) {
	i = (b+e)/2;
	if (x < ksc_table[i]) {
	    e = i;
	} else if (x > ksc_table[i]) {
	    b = i;
	} else 
	    return i;
    }
    return -1;
}

guint16
to_hangul(guint16 x)
{
    int i;
    int page, offset;
    if (x < 0x8861) return 0;
    if (x > 0xd3b7) return 0;
    i = find_ksc_index(x);
    if (i < 0) return 0;
    page = i / page_size;
    offset = i % page_size;
    x=((page+0xb0) << 8) | (offset+0xa1);
    return x;
}

void
puttc(guint16 x, char *s)
{
    *s++ = x >> 8;
    *s = x & 0xff;
}

char **dic_array = NULL;
int array_size = 0;
int dic_size = 0;

void
dic_append(char *s)
{
    if (dic_size >= array_size) {
	if (array_size == 0) array_size = 20000;
	else array_size *= 2;
	dic_array = g_realloc(dic_array, array_size*sizeof(char *));
    }
    dic_array[dic_size] = s;
    dic_size++;
}

int
compare(unsigned char **s1, unsigned char **s2)
{
    register unsigned char *ss1 = *s1;
    register unsigned char *ss2 = *s2;
    while (1) {
	if (*ss1 > *ss2) return 1;
	else if (*ss1++ < *ss2++) return -1;
    }
}

void
dic_sort(void)
{
    qsort(dic_array, dic_size, sizeof(char *), compare);
}

char *
line_merge(char *s1, char *s2)
{
    GString *str;
    str = g_string_new(s1);
    s2 = strtok(s2, " ");
    while ((s2 = strtok(NULL, " "))) {
	if (strstr(s1, s2)) {
	    g_string_append(str, " ");
	    g_string_append(str, s2);
	}
    }
    g_free(s1); 
    s1 = str->str;
    g_string_free(str, FALSE);
    return s1;
}

void
dic_merge(void)
{
    char *s1, *s2;
    int i;
    char *l1, *l2;

    for(i=1;i<dic_size;i++) {
	if (dic_array[i-1][0] == dic_array[i][0]) {
	    s1 = (char *)&dic_array[i-1][1];
	    s2 = (char *)&dic_array[i][1];
	    l1 = strchr(s1, ' ');
	    l2 = strchr(s2, ' ');
	    if (l1 == l2 && (strncmp(s1, s2, l1 - s1) == 0)) {
		dic_array[i] = line_merge(dic_array[i-1], dic_array[i]);
		dic_array[i-1] = NULL;
	    }
	}
    }
}

void
dic_print(void)
{
    int i;
    for(i=0;i<dic_size;i++) {
	if (dic_array[i]) g_print("%s\n", dic_array[i]);
    }
}

void
extract_dic(guint16 *buf, int size)
{
    guint16 han_buf[MAX_WORDSIZE];
    int han_len;
    guint16 hanja_buf[MAX_HANJA][MAX_WORDSIZE];
    guint16 x;
    int n=0, m, i, j;
    gboolean discard;

    while(1) {
	while(n < size && !is_hangul(*buf)) {
	    buf++;
	    n++;
	}
	if (n >= size) break;

	han_len = 0;
	discard = 0;
	while(is_hangul(*buf)) {
	    if (!discard) {
		x = to_hangul(*buf);
		if (!x) discard = 1;
	    }
	    han_buf[han_len++] = x;
	    buf++;
	}
	n += han_len;

	for(m=0;n < size && is_hanja(*buf); buf += han_len, n += han_len) {
	    for(i=0;!discard && i<han_len;i++) {
		x = to_hanja(buf[i]);
		if (!x) break;
		hanja_buf[m][i] = x;
	    }
	    if(i == han_len) m++;
	}

	if (m && !discard) {
	    char *str, *s;
	    str = s = g_new(gchar, han_len*2*(1+m)+m+1);
	    for(i=0;i<han_len;i++,s+=2) puttc(han_buf[i], s);
	    *s++ = ' ';
	    for(j=0;j<m;j++) {
		for(i=0;i<han_len;i++, s += 2) puttc(hanja_buf[j][i],s);
		*s++ = ' ';
	    }
	    *(s-1) = 0;
	    dic_append(str);
	}
    }
    dic_sort();
    dic_merge();
    dic_print();
}

int
main(int argc, char *argv[])
{
    int fd;
    long int size;
    int offset;
    guint16 *buf;
    char *filename = argv[1];

    if (argc < 2) {
	g_print("Usage: %s hwp한자사전 > hanja.dic\n", argv[0]);
	g_print("이 프로그램은 hwp에서 쓰이는 한자사전을 아미에서 쓸 수있는\n"
	        "형태로 변환하는 프로그램입니다. hanja.dic를 만드신 이것을\n"
		"/usr/share/ami/아래로 옮겨놓으시기 바랍니다.\n");
	return -1;
    }
    fd = open(filename, O_RDONLY);
    if (fd < 0) {
	g_warning("fail to open %s", filename);
	return -1;
    }
    size = lseek(fd, 0, SEEK_END);
    buf = g_new(guint16, size/2 + 1);
    lseek(fd, 0, SEEK_SET);
    read(fd, buf, size);
    buf[size/2] = 0;
    extract_dic(buf, size/2);
    return 0;
}
