#!/bin/sh
# Run this to generate all the initial makefiles, etc.

PACKAGE="ami"
SUBPACKAGES=""

STRICTNESS=--foreign
USE_LIBTOOL="no"

echo "Processing ..."
if [ x$USE_LIBTOOL = xyes ]
then
  libtoolize --force --automake
fi
gettextize -c -f --intl --no-changelog
aclocal-1.4 $ACLOCAL_FLAGS
autoheader
automake-1.4 --add-missing --copy $STRICTNESS
autoconf

for i in $SUBPACKAGES
do
  echo "Processing subpackage \"$i\" ..."
  (cd $i; \
   NOCONFIGURE=yes; export NOCONFIGURE; \
   ./autogen.sh)
done

if test x$NOCONFIGURE = x; then
  echo "running ./configure --enable-maintainer-mode --enable-compiler-warnings=yes $@"
  ./configure --enable-maintainer-mode "$@" \
  && echo Now type \`make\' to compile the $PACKAGE.
else
  echo Skipping configure process.
fi
