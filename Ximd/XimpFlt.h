void _XimpRegisterDestroyFilter(XIMS xims, XimpClient *client);
void _XimpRegisterKeyPressFilter(XIMS xims, XimpClient *client);
void _XimpRegisterKeyReleaseFilter(XIMS xims, XimpClient *client);
void _XimpUnregisterDestroyFilter(XIMS xims, XimpClient *client);
void _XimpUnregisterKeyPressFilter(XIMS xims, XimpClient *client);
void _XimpUnregisterKeyReleaseFilter(XIMS xims, XimpClient *client);
