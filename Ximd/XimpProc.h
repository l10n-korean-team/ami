

Bool _XimpProcKeyPress(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcKeyRelease(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcCreate(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcDestroy(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcStart(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcEnd(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcSetFocus(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcUnsetFocus(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcClientWin(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcFocusWin(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcMove(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcSetValues(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcGetValues(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcReset(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcEventMask(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcExtension(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcPreStartReturn(XIMS xims, XClientMessageEvent *ev);
Bool _XimpProcPreCaretReturn(XIMS xims, XClientMessageEvent *ev);
