

Bool _XimpSetMatch(XimpClient *client, CARD32 icid, CARD32 kcode, CARD32 state);
Bool _XimpProcessMatch(XimpClient *client, CARD32 icid, CARD32 kcode, CARD32 state);
XimpClient * _XimpFindClient(XIMPCore core, CARD32 icid, IMPProtocol *call_data);
XimpClient * _XimpNewClient(XIMPCore core, XClientMessageEvent *ev);
void _XimpDeleteClient(XIMPCore core, CARD32 icid);
void _XimpSendByClientMessage(XIMPCore core, CARD32 icid, char *ctext, int length);
void _XimpSendByProperty(XIMPCore core, CARD32 icid, char *ctext, int length);
void _XimpSendIMProtocol(XIMPCore core, XimpClient *client, int type, unsigned long l1, unsigned long l2, unsigned long l3, unsigned long l4);
Atom _XimpReplacePropPool(XIMPCore core, char *text, int len);
