

int _XimpGeometryCallback(XIMS xims, IMPProtocol *call_data);
int _XimpPreeditStartCallback(XIMS xims, IMPProtocol *call_data);
int _XimpPreeditDoneCallback(XIMS xims, IMPProtocol *call_data);
int _XimpPreeditDrawCallback(XIMS xims, IMPProtocol *call_data);
int _XimpPreeditDrawCMCallback(XIMS xims, IMPProtocol *call_data);
int _XimpPreeditCaretCallback(XIMS xims, IMPProtocol *call_data);
int _XimpStatusStartCallback(XIMS xims, IMPProtocol *call_data);
int _XimpStatusDoneCallback(XIMS xims, IMPProtocol *call_data);
int _XimpStatusDrawCallback(XIMS xims, IMPProtocol *call_data);
int _XimpStatusDrawCMCallback(XIMS xims, IMPProtocol *call_data);
