/******************************************************************
 
         Copyright 1994, 1995 by Sun Microsystems, Inc.
         Copyright 1993, 1994 by Hewlett-Packard Company
 
Permission to use, copy, modify, distribute, and sell this software
and its documentation for any purpose is hereby granted without fee,
provided that the above copyright notice appear in all copies and
that both that copyright notice and this permission notice appear
in supporting documentation, and that the name of Sun Microsystems, Inc.
and Hewlett-Packard not be used in advertising or publicity pertaining to
distribution of the software without specific, written prior permission.
Sun Microsystems, Inc. and Hewlett-Packard make no representations about
the suitability of this software for any purpose.  It is provided "as is"
without express or implied warranty.
 
SUN MICROSYSTEMS INC. AND HEWLETT-PACKARD COMPANY DISCLAIMS ALL
WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
SUN MICROSYSTEMS, INC. AND HEWLETT-PACKARD COMPANY BE LIABLE FOR ANY
SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 
  Author: Hidetoshi Tajima(tajima@Eng.Sun.COM) Sun Microsystems, Inc.
 
******************************************************************/
#include <stdlib.h>
#include <X11/Xlib.h>
#include <string.h>
#include "IMdkit.h"
#if NeedVarargsPrototypes
# include <stdarg.h>
# define Va_start(a,b) va_start(a,b)
#else
# include <varargs.h>
# define Va_start(a,b) va_start(a)
#endif

static void
#if NeedVarargsPrototypes
_IMCountVaList(va_list var, int *total_count)
#else
_IMCountVaList(var, total_count)
    va_list var;
    int *total_count;
#endif
{
    char *attr;

    *total_count = 0;

    for (attr = va_arg(var, char*); attr; attr = va_arg(var, char*)) {
	va_arg(var, XIMArg*);
	++(*total_count);
    }
}

static void
#if NeedVarargsPrototypes
_IMVaToNestedList(va_list var, int max_count, XIMArg **args_return)
#else
_IMVaToNestedList(var, max_count, args_return)
    va_list var;
    int max_count;
    XIMArg **args_return;
#endif
{
    XIMArg *args;
    char   *attr;

    if (max_count <= 0) {
	*args_return = (XIMArg *)NULL;
	return;
    }

    args = (XIMArg *)malloc((unsigned)(max_count + 1) * sizeof(XIMArg));
    *args_return = args;
    if (!args) return;

    for (attr = va_arg(var, char*); attr; attr = va_arg(var, char*)) {
	args->name = attr;
	args->value = va_arg(var, XPointer);
	args++;
    }
    args->name = (char*)NULL;
}

static char *
_FindModifiers(XIMArg *args)
{
    char *modifiers;

    while (args->name) {
	if (!strcmp(args->name, IMModifiers)) {
	    modifiers = args->value;
	    return modifiers;
	} else {
	    args++;
	}
    }
    return NULL;
}

XIMS
_GetIMS(char *modifiers)
{
    XIMS ims;
#ifdef Use_Xi18n
    extern IMMethodsRec Xi18n_im_methods;
#endif
#ifdef Use_Ximp
    extern IMMethodsRec Ximp_im_methods;
#endif

    if ((ims = (XIMS)calloc(1, sizeof(XIMProtocolRec))) == NULL) {
	return NULL;
    }

#ifdef Use_Xi18n
    if (!*modifiers || !modifiers || !strcmp(modifiers, "Xi18n")) {
	ims->methods = &Xi18n_im_methods;
    }
#endif
#ifdef Use_Ximp
    if (!*modifiers || !modifiers || !strcmp(modifiers, "XIMP")) {
	ims->methods = &Ximp_im_methods;
    } 
#endif
    if (!ims->methods) {
	XFree(ims);
	return (XIMS)NULL;
    }
    return ims;
}

#if NeedVarargsPrototypes
XIMS
IMOpenIM(Display *display, ...)
#else
XIMS
IMOpenIM(display, va_alist)
Display *display;
va_dcl
#endif
{
    va_list var;
    int total_count;
    XIMArg *args;
    XIMS ims;
    char *modifiers;
    Status ret;

    Va_start(var, display);
    _IMCountVaList(var, &total_count);
    va_end(var);

    Va_start(var, display);
    _IMVaToNestedList(var, total_count, &args);
    va_end(var);

    modifiers = _FindModifiers(args);

    ims = _GetIMS(modifiers);
    if (ims == (XIMS)NULL) return (XIMS)NULL;

    ims->core.display = display;

    ims->protocol = (*ims->methods->setup)(display, args);
    XFree(args);
    if (ims->protocol == (void *)NULL) {
	XFree(ims);
	return (XIMS)NULL;
    }
    ret = (ims->methods->openIM)(ims);
    if (ret == False) {
	XFree(ims);
	return (XIMS)NULL;
    }
    return (XIMS)ims;
}

Status
IMCloseIM(XIMS ims)
{
    (ims->methods->closeIM)(ims);
    XFree(ims);
    return True;
}
