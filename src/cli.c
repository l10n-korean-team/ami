/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "config.h"
#include "cli.h"
#include "ic.h"
#include "dw.h"
#include "edit.h"

void 
cli_win_is_dead(IC *ic, gpointer data)
{
    if (ic->client_win == data) {
	ic_destroy(ic);
    }
}

GdkFilterReturn
cli_win_monitor(XEvent *xev, GdkEvent *ev, gpointer data)
{
    GdkWindow *client = gdk_window_lookup(xev->xany.window);
    if (client == NULL) {
	g_warning("Unregistered Window death signal captured: %#lx", 
	    xev->xany.window);
	return GDK_FILTER_REMOVE;
    }
    if(xev->xany.type == DestroyNotify) {
	/*
	gdk_error_trap_push();
	XSelectInput(gdk_display, xev->xany.window, 0);
	gdk_error_trap_pop();
	*/
	ic_list_foreach(cli_win_is_dead, client);
	/* FIXME ***** */
	gdk_window_destroy_notify(client);
	//g_print("final refcount=%d\n", private->ref_count);
	return GDK_FILTER_REMOVE;
    }
    return GDK_FILTER_REMOVE;
}

void
cli_win_register(GdkWindow *win)
{
    GdkWindowPrivate *private;

    g_return_if_fail(win != NULL);

    gdk_window_set_events(win, 0);
    gdk_window_add_filter(win, (GdkFilterFunc)cli_win_monitor, NULL); 

    private = (GdkWindowPrivate *) win;
    if (private->parent) {
	GdkWindowPrivate *parent = (GdkWindowPrivate *) private->parent;
	parent->children = g_list_remove(parent->children, win);
	private->parent = NULL;
    }
}

void
clean_han_buf(IC *ic)
{
    if (ic && ic_is_valid(ic) && ic->han) {
	if (ic->han->aux_input) {
	    gtk_object_set_data((GtkObject *)ic->han->aux_input, "ic", NULL);
	    gtk_widget_destroy(ic->han->aux_input);
	}
	ic_han_free(ic->han);
	ic->han = NULL;
    }
}

GdkWindow *
cli_win_new(Window win, IC *ic)
{
    GdkWindow *window;
    window = gdk_window_lookup(win);
    if (window) {
	  gdk_window_ref(window);
    } else {
	  window = gdk_window_foreign_new(win);
	  cli_win_register(window);
    }
    if (window == NULL) {
	g_warning("%s: created client window is NULL: ic=%d",__FUNCTION__, ic->id);
    }
    return window;
}

IC *
cli_get_edit_ic(IC *ic)
{
    IC *edit_ic;
    g_return_val_if_fail (ic != NULL, NULL);
    /* Netscape (4.x?) sometimes loses ic even before setting up
     * client_win. */ 
    if (ic->client_win == NULL) return NULL;
    g_return_val_if_fail (ic->top_win != NULL, NULL);
    edit_ic = g_dataset_get_data(ic->top_win, "edit_ic");
    if (edit_ic && ic_is_valid(edit_ic)) return edit_ic;
    return NULL;
}

IC *
cli_get_status_ic(IC *ic)
{
    IC *status_ic;
    g_return_val_if_fail (ic != NULL, NULL);
    /* Netscape (4.x?) sometimes loses ic even before setting up
     * client_win. */ 
    if (ic->client_win == NULL) return NULL;
    g_return_val_if_fail (ic->top_win != NULL, NULL);
    status_ic = g_dataset_get_data(ic->top_win, "status_ic");
    if (status_ic && ic_is_valid(status_ic)) return status_ic;
    return NULL;
}

void
cli_unset_edit_ic(IC *ic)
{
    g_return_if_fail (ic->top_win != NULL);
    g_dataset_set_data_full(ic->top_win, "edit_ic", NULL, NULL);
}

void
cli_set_edit_ic(IC *ic)
{
    IC *prev;
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->top_win != NULL);
    prev = g_dataset_get_data(ic->top_win, "edit_ic");
    if (ic == prev) {
	if (ic->edit_win) {
	    if (ic->han->composing_hangul) dw_show(ic->edit_win);
	    dw_set_focus(ic->edit_win);
	}
	return;
    }
    if (prev && ic_is_valid(prev)) {
	if (prev->edit_win) dw_hide(prev->edit_win);
	ic->han = prev->han;
	prev->han = NULL;
    }
    if (ic->han == NULL) {
	ic->han = ic_han_new();
	if (prev == NULL && 
	    g_dataset_get_data(ic->top_win, "composing_hangul")) 
	    ic->han->composing_hangul = 1;
    }
    g_dataset_set_data_full(ic->top_win, "edit_ic", ic, 
    	(GDestroyNotify)clean_han_buf);
    if (ic->edit_win) {
	editing_edit_win_update(ic);
	dw_set_focus(ic->edit_win);
	if (ic->han->composing_hangul) {
	    dw_show(ic->edit_win);
	}
    }
}

void
cli_unset_status_ic(IC *ic)
{
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->top_win != NULL);
    if (!ic->han) return;
    if (ic->han->composing_hangul) {
	g_dataset_set_data(ic->top_win, "composing_hangul", GINT_TO_POINTER(1));
    } else {
	g_dataset_set_data(ic->top_win, "composing_hangul", NULL);
    }
    //g_dataset_set_data(ic->top_win, "status_ic", NULL);
    //dw_hide(ic->status_win);
}

void
cli_set_status_ic(IC *ic)
{
    IC *prev;
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->top_win != NULL);
    prev = g_dataset_get_data(ic->top_win, "status_ic");
    if (ic == prev) {
	if (ic->status_win) {
	    editing_status_win_update(ic);
	    dw_show(ic->status_win);
	    dw_set_focus(ic->status_win);
	}
	return;
    }
    if (prev && ic_is_valid(prev) && prev->status_win) {
	dw_hide(prev->status_win);
    }
    g_dataset_set_data(ic->top_win, "status_ic", ic);
    if (ic->status_win) {
	editing_status_win_update(ic);
	dw_show(ic->status_win);
	dw_set_focus(ic->status_win);
    }
}

