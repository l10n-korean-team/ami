/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "config.h"

/*
Some of routines below come from parsekey.c of kinput2.
*/

/*- 
 * Copyright (c) 1991, 1994  Software Research Associates, Inc.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose and without fee is hereby granted, provided
 * that the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Software Research Associates not be
 * used in advertising or publicity pertaining to distribution of the
 * software without specific, written prior permission.  Software Research
 * Associates makes no representations about the suitability of this software
 * for any purpose.  It is provided "as is" without express or implied
 * warranty.
 *
 * Author:  Makoto Ishisone, Software Research Associates, Inc., Japan
 */

#include <stdio.h>
#include <string.h>

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkx.h>

#include "keyparse.h"

/*- parse_modifiers: parse modifier list -*/
static void
parse_modifiers(char *s, CARD32 *modp, CARD32 *chkmodp)
{
    char *p;
    int i;
    int c;
    static struct _moddesc_ {
	char *modname;		/* modifier name */
	CARD32 modmask;		/* modifier mask */
    } mods[] = {
	{ "Shift",	ShiftMask },
	{ "Lock",	LockMask },
	{ "Ctrl",	ControlMask },
	{ "Meta",	Mod1Mask },
	{ "Alt",	Mod1Mask },
	{ "Mod1",	Mod1Mask },
	{ "Mod2",	Mod2Mask },
	{ "Mod3",	Mod3Mask },
	{ "Mod4",	Mod4Mask },
	{ "c",		ControlMask },
	{ "s",		ShiftMask },
	{ "l",		LockMask },
	{ "m",		Mod1Mask },
	{ "a",		Mod1Mask },
    };

    *modp = *chkmodp = 0L;

#define SKIPBLANK \
    while ((c = *s) == ' ' || c == '\t' || c == '\n') s++; \
    if (c == '\0') return;
#define SEARCHBLANK \
    p = s; while ((c = *p) != '\0' && c != ' ' && c != '\t' && c != '\n') p++;

    while (*s != '\0') {
	int tilde = 0;

	SKIPBLANK;
	if (c == '~') {
	    tilde = 1;
	    s++;
	    SKIPBLANK;
	}
	SEARCHBLANK;
	*p = '\0';
	for (i = 0; i < sizeof(mods)/sizeof(mods[0]); i++) {
	    if (!strcmp(s, mods[i].modname)) {
		*chkmodp |= mods[i].modmask;
		if (!tilde) *modp |= mods[i].modmask;
		break;
	    }
	}
	if (c == '\0') break;
	s = p + 1;
    }
#undef SKIPBLANK
#undef SEARCHBLANK
}

int
parse_key_event(char *s, CARD32 *keysymp, CARD32 *modp, CARD32 *chkmodp)
{
    char *key;
    char *p;
    KeySym keysym;

    /*
     * keyevent description (stored in  argument 's') must be of the
     * following format (subset of Xt translation table syntax):
     *		modifier-list<Key>keysym
     * modifier-list is a combination of:
     *		Ctrl, Shift, Lock, Meta, Alt, Mod1, Mod2, Mod3, Mod4, Mod5
     * if '~' is prepended before a modifier, it means the modifier key should
     * not be pressed.
     */

    /* find "<Key>" */
    if ((p = strstr(s, "<Key>")) != NULL) {
	key = p + 5;	/* p + strlen("<Key>") */
    } else if ((p = strstr(s, "<KeyPress>")) != NULL) {
	key = p + 10;	/* p + strlen("<KeyPress>") */
    } else if ((p = strstr(s, "<KeyDown>")) != NULL) {
	key = p + 9;	/* p + strlen("<KeyDown>") */
    } else {
	return 0;
    }
    *p = '\0';
    while (*key == ' ' || *key == '\t') key++;
    p = key;
    while (*p != '\0' && *p != ' ' && *p != '\t') p++;
    *p = '\0';

    /* get modifier mask */
    parse_modifiers(s, modp, chkmodp);

    /* get keycode */
    if ((keysym = XStringToKeysym(key)) == NoSymbol) return 0;

    *keysymp = keysym;

    return 1;
}

int 
make_trigger_keys(XIMTriggerKey **key, gchar *str) 
{
    CARD32 keysym, mod, modmask;
    char *s;
    GSList *list = NULL, *list_head;
    XIMTriggerKey *keys;
    XIMTriggerKey *triggerkey;
    XIMTriggerKey null = {0, 0, 0};
    int i, num;

    g_return_val_if_fail (key != NULL, 0);
    g_return_val_if_fail (str != NULL, 0);

    str = g_strdup(str);
    s = strtok(str, ",");
    while (s) {
	if (parse_key_event(s, &keysym, &mod, &modmask)) {
	    triggerkey = g_new(XIMTriggerKey, 1);
	    triggerkey->keysym = keysym;
	    triggerkey->modifier = mod;
	    triggerkey->modifier_mask = modmask;
	    list = g_slist_prepend(list, triggerkey);
	} else {
	    g_warning("Invalid Key Specification: %s", s);
	}
	s = strtok(NULL, ",");
    }
    num = g_slist_length(list);
    keys = g_new(XIMTriggerKey, num + 1);
    list_head = list; i = 0;
    while (list) {
	keys[i] = *(XIMTriggerKey *)list->data;
	g_free(list->data);
	list = list->next; i++;
    }
    g_slist_free(list_head);
    g_free(str);
    keys[i] = null;
    if (*key) g_free(*key);
    *key = keys;
    return num;
}


char * 
trigger_keys_to_str(XIMTriggerKey *key)
{
    GString *str;
    char *s;
    int i;
    CARD32 forbidden_mod;
    static struct _moddesc_ {
	char *modname;		/* modifier name */
	CARD32 modmask;		/* modifier mask */
    } mods[] = {
	{ "Shift",	ShiftMask },
	{ "Lock",	LockMask },
	{ "Ctrl",	ControlMask },
	{ "Meta",	Mod1Mask },
	{ "Alt",	Mod1Mask },
	{ "Mod1",	Mod1Mask },
	{ "Mod2",	Mod2Mask },
	{ "Mod3",	Mod3Mask },
	{ "Mod4",	Mod4Mask },
    };
    str = g_string_new("");
    while(key->keysym) {
	forbidden_mod = key->modifier_mask & (~key->modifier);
	for(i=0;i<sizeof(mods)/sizeof(mods[0]);i++) {
	    if (forbidden_mod & mods[i].modmask) {
		g_string_append(str, "~");
		g_string_append(str, mods[i].modname);
		g_string_append(str, " ");
	    }
	}
	for(i=0;i<sizeof(mods)/sizeof(mods[0]);i++) {
	    if (key->modifier & mods[i].modmask) {
		g_string_append(str, mods[i].modname);
		g_string_append(str, " ");
	    }
	}
	g_string_append(str, "<Key>");
	g_string_append(str, XKeysymToString(key->keysym));
	key++;
	if (key->keysym) g_string_append(str, ","); 
    }
    s = str->str;
    g_string_free(str, FALSE);
    return s;
}

