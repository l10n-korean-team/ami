/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "config.h"
#include "amitype.h"
#include "hinput.h"
#include "handler.h"
#include "ic.h"
#include "dw.h"
#include "ami.h"
#include "cli.h"
#include "applet.h"

static IC *ic_list = (IC *)NULL;
static IC *free_list = (IC *)NULL;

/*
#define XID_DEBUG
*/

#ifdef XID_DEBUG
static gint gdk_xid_table_size(void);
#endif

static int
check_focus_win(Window win)
{
    IC *ic = ic_list;
    while(ic) {
	if (ic->focus_win && GDK_WINDOW_XWINDOW(ic->focus_win) == win) {
	    if (debug) g_print("%s: found ic=%d\n",__FUNCTION__, ic->id);
	    return TRUE;
	}
	ic = ic->next;
    }
    return FALSE;
}

int han_count = 0;
HangulState *
ic_han_new(void)
{
    HangulState *han;
    han = g_new0(HangulState, 1);
    han->composer = composer_new();
    if (ami_codeset == AMI_UTF8) 
      composer_set_output_encoding(han->composer, "UTF-8");
    han->buf = g_new0(char, 256);
    han->buf_size = 256;
    return han;
}

void
ic_han_free(HangulState *han)
{
    if (han == NULL) return;
    composer_free(han->composer);
    g_free(han->buf);
    g_free(han);
}

static IC *
ic_new(void)
{
    static CARD16 icid = 0;
    IC *ic;

    if (free_list != NULL) {
	ic = free_list;
	free_list = free_list->next;
    } else {
	ic = g_new(IC, 1);
    }
    memset(ic, 0, sizeof(IC));
    ++icid;
    while (icid == 0 || ic_is_valid_id(icid)) {
	++icid;
    }
    ic->id = icid;

    ic->next = ic_list;
    ic_list = ic;
    //g_print("%s: ic: %p id: %d\n", __FUNCTION__, ic, ic->id);
    return ic;
}

void
ic_list_foreach(void (*func)(IC *ic, gpointer data), gpointer data)
{
    IC *list = ic_list;
    while (list) {
	IC *next = list->next;
	func(list, data);
	list = next;
    }
}

int
ic_is_valid(IC *ic)
{
    IC *list;
    CARD16 id;
    if (ic == NULL) return FALSE;
    id = ic->id;
    list = ic_list;
    while (list) {
	if (id == list->id) return TRUE;
	list = list->next;
    }
    return FALSE;
}

int
ic_is_valid_id(CARD16 id)
{
    IC *list = ic_list;
    while (list) {
	if (id == list->id) return TRUE;
	list = list->next;
    }
    return FALSE;
}

void
ic_remove(IC *ic)
{
    IC *list;
    if (ic_list == ic) {
	ic_list = ic->next;
	ic->next = free_list;
	free_list = ic;
#if 0
	if (ic_is_valid(ic)) {
	    g_warning("ic was destroyed, but is still in ic_list");
	}
#endif
	return;
    }
    list = ic_list;
    while (list) {
	if (list->next == ic) {
	    list->next = ic->next;
	    break;
	}
	list = list->next;
    }
    if (list == NULL) {
	g_warning("IC(%p id=%d) to delete is not in ic_list", ic, ic->id);
    }
    ic->next = free_list;
    free_list = ic;
#if 0
    if (ic_is_valid(ic)) {
	g_warning("ic was destroyed, but is still in ic_list");
    }
#endif
}


void
ic_destroy(IC *ic)
{
    g_return_if_fail(ic != NULL);
    if(debug) g_print("%s: %p id=%d, connect_id=%d\n",__FUNCTION__, ic, ic->id, ic->connect_id);
    if (ic->top_win && ic == cli_get_status_ic(ic)) {
	cli_unset_status_ic(ic);
    }
    if (ic->top_win && ic == cli_get_edit_ic(ic)) {
	draw_applet_image(AMI_PIXMAP_NONE);
	cli_unset_edit_ic(ic);
    }

    if (ic->top_win && ic->client_win != ic->top_win && !unique_han && ic->client_win != GDK_ROOT_PARENT()) {
	GdkWindowPrivate *private = (GdkWindowPrivate *)ic->top_win;
        gdk_window_unref(ic->top_win);
	if (private->ref_count == 1) gdk_window_destroy_notify(ic->top_win);
    }
    ic->top_win = NULL;

    if(ic->edit_win) {
	dw_destroy(ic->edit_win);
	ic->edit_win = NULL;
    }
    ic->destroyed = 1;
    if(ic->status_win) {
	dw_destroy(ic->status_win);
	ic->status_win = NULL;
    }
    if (ic->focus_win != ic->client_win && ic->focus_win) {
	/**** FIXME ****/
	GdkWindowPrivate *private = (GdkWindowPrivate *)ic->focus_win;
	gdk_window_unref(ic->focus_win);
	if (private->ref_count == 1) gdk_window_destroy_notify(ic->focus_win);
    }
    ic->focus_win = NULL;
	
    if (ic->client_win && ic->client_win != GDK_ROOT_PARENT()) {
	GdkWindowPrivate *private = (GdkWindowPrivate *)ic->client_win;
	gdk_window_unref(ic->client_win);
	//if (private->ref_count == 1) gdk_window_destroy_notify(ic->client_win);
	ic->client_win = NULL;
    }
    if (ic == current_focus_ic) current_focus_ic = NULL;

    if (ic->han) ic_han_free(ic->han);
    ic_remove(ic);
    if(debug) g_print("ic (%d) is gone\n", ic->id);
#ifdef XID_DEBUG
    g_print("total # of registered windows  is %d\n", gdk_xid_table_size());
#endif
}

static void
ic_store(IC *ic, IMChangeICStruct *call_data)
{
    XICAttribute *ic_attr = call_data->ic_attr;
    XICAttribute *pre_attr = call_data->preedit_attr;
    XICAttribute *sts_attr = call_data->status_attr;
    register int i;

    for (i = 0; i < (int)call_data->ic_attr_num; i++, ic_attr++) {
	//g_print("%s: ic_attr = %s; request\n", __FUNCTION__,ic_attr->name);
	if (!strcmp(XNInputStyle, ic_attr->name)) {
	  ic->input_style = *(INT32*)ic_attr->value;
	  if(debug) {
	    g_print("input_style: id = %d\n", ic->id);

#define pr_flag(a) if (ic->input_style & a) g_print( #a " ");

	    pr_flag(XIMPreeditArea)
	    pr_flag(XIMPreeditPosition)
	    pr_flag(XIMPreeditCallbacks)
	    pr_flag(XIMPreeditNothing)
	    pr_flag(XIMPreeditNone)

	    pr_flag(XIMStatusCallbacks)
	    pr_flag(XIMStatusArea)
	    pr_flag(XIMStatusNothing)
	    pr_flag(XIMStatusNone)
	    g_print("\n");
	  }
	  if (ic->input_style & (XIMPreeditArea|XIMPreeditPosition)) {
	     ic->edit_win = dw_new(ic);
	  }
	  if (ic->input_style & (XIMStatusArea)) {
	    ic->status_win = dw_new(ic);
	    dw_set_text(ic->status_win, english_mode_label, -1, 0);
	  }
	  XFree(ic_attr->value);
	}
    }
    ic_attr = call_data->ic_attr;
    for (i = 0; i < (int)call_data->ic_attr_num; i++, ic_attr++) {
	//g_print("%s: ic_attr = %s; request\n", __FUNCTION__,ic_attr->name);
	if (!strcmp(XNClientWindow, ic_attr->name)) {
	  Window win = (Window)*(INT32*)ic_attr->value;
	  if (debug) g_print("client win set: xwin=%#lx ic = %d\n", win, ic->id);
	  if (ic->client_win) {
	    g_warning("client win is specified repeatedly");
	    XFree(ic_attr->value);
	    continue;
	  }
	  ic->client_win = cli_win_new(win, ic);
	  g_assert(ic->client_win != NULL);
	  if (ic->edit_win && ic->focus_win == NULL) {
	     dw_set_parent(ic->edit_win, ic->client_win);
	     if (debug) g_print("new edit win of ic(%d): gdkwin=%p xwin=%#lx\n", ic->id, ic->edit_win->win, GDK_WINDOW_XWINDOW(ic->edit_win->win));
	     dw_hide(ic->edit_win);
	  }
	  if (ic->status_win) {
	     dw_set_parent(ic->status_win, ic->client_win);
	     if (debug) g_print("new status win of ic(%d): %p %#lx\n", ic->id, ic->status_win->win, GDK_WINDOW_XWINDOW(ic->status_win->win));
	  }
	  XFree(ic_attr->value);
	  if (debug) g_print("client_win = %p; ic=%d\n", ic->client_win,ic->id);
	} else if (!strcmp(XNFocusWindow, ic_attr->name)) {
	  Window win;
	  GdkWindow *fwin;

	  /* bogus */
	  if (!(ic->input_style & XIMPreeditPosition)) {
	      XFree(ic_attr->value);
	      continue;
	  }

	  win = (Window)*(INT32*)ic_attr->value;
	  fwin = gdk_window_lookup(win);

	  if (fwin && (/*fwin == ic->client_win ||*/ fwin == ic->focus_win)) {
	     XFree(ic_attr->value);
	     continue;
	  }
	  if (ic->focus_win) gdk_window_unref(ic->focus_win);

	  if (fwin == NULL) fwin = gdk_window_foreign_new(win);

	  if (ic->edit_win && (ic->input_style & XIMPreeditPosition)) {
	     dw_set_parent(ic->edit_win, fwin);
	     if(debug) g_print("new edit win of ic(%d): %p %#lx\n", ic->id, ic->edit_win, GDK_WINDOW_XWINDOW(ic->edit_win));
	  }

	    if (check_focus_win(win)) {
		GdkWindowPrivate *private = (GdkWindowPrivate *)fwin;
		g_warning("focus win (%p) was already registered: ref=%d", fwin,private->ref_count);
	    }
	    gdk_window_ref(fwin);
	    ic->focus_win = fwin;
	  if (debug) g_print("focus_win = %p; ic=%d\n", ic->focus_win, ic->id);
	  XFree(ic_attr->value);
	} else if (strcmp(XNInputStyle, ic_attr->name) != 0) {
	  g_warning("%s: fail: %s", __FUNCTION__, ic_attr->name);
	  XFree(ic_attr->value);
	}
    }

    for (i = 0; i < (int)call_data->preedit_attr_num; i++, pre_attr++) {
	if (ic->edit_win == NULL) {
	    /* bogus */
	    XFree(pre_attr->value);
	    continue;
	}
	//g_print("%s: pre_attr = %s; request\n", __FUNCTION__,pre_attr->name);
	if (!strcmp(XNArea, pre_attr->name)) {
	    //g_print("set_area: ic=%d edit\n", ic->id);
	  dw_set_area(ic->edit_win, (XRectangle*)pre_attr->value);
	} else if (!strcmp(XNAreaNeeded, pre_attr->name)) {
	  dw_set_area_needed(ic->edit_win, (XRectangle*)pre_attr->value);
	} else if (!strcmp(XNSpotLocation, pre_attr->name)) {
	  dw_set_pos(ic->edit_win, (XPoint *)pre_attr->value);
	} else if (!strcmp(XNColormap, pre_attr->name))
	  dw_set_cmap(ic->edit_win, (Colormap*)pre_attr->value);
	else if (!strcmp(XNStdColormap, pre_attr->name))
	  dw_set_cmap(ic->edit_win, (Colormap*)pre_attr->value);
	else if (!strcmp(XNForeground, pre_attr->name)) {
	  dw_set_fg(ic->edit_win, *(CARD32*)pre_attr->value);
	} else if (!strcmp(XNBackground, pre_attr->name)) {
	  dw_set_bg(ic->edit_win, *(CARD32*)pre_attr->value);
	} else if (!strcmp(XNBackgroundPixmap, pre_attr->name))
	  dw_set_bg_pixmap(ic->edit_win, (Pixmap*)pre_attr->value);
	else if (!strcmp(XNFontSet, pre_attr->name)) {
	  dw_set_font(ic->edit_win, pre_attr->value);
	} else if (!strcmp(XNLineSpace, pre_attr->name)) 
	  ic->edit_win->linespace = *(CARD32*)pre_attr->value;
	else if (!strcmp(XNCursor, pre_attr->name))
	  dw_set_cursor(ic->edit_win, (Cursor*)pre_attr->value);
	else {
	  g_warning("%s: fail: %s", __FUNCTION__, pre_attr->name);
	}
	XFree(pre_attr->value);
    }
    for (i = 0; i < (int)call_data->status_attr_num; i++, sts_attr++) {
	if (ic->status_win == NULL) {
	    /* bogus */
	    XFree(sts_attr->value);
	    continue;
	}
	if (!strcmp(XNArea, sts_attr->name)) {
	  dw_set_area(ic->status_win, (XRectangle*)sts_attr->value);
	} else if (!strcmp(XNAreaNeeded, sts_attr->name)) {
	  dw_set_area_needed(ic->status_win, (XRectangle*)sts_attr->value);
	} else if (!strcmp(XNColormap, sts_attr->name))
	  dw_set_cmap(ic->status_win,(Colormap*)sts_attr->value);
	else if (!strcmp(XNStdColormap, sts_attr->name))
	  dw_set_cmap(ic->status_win, (Colormap*)sts_attr->value);
	else if (!strcmp(XNForeground, sts_attr->name)) {
	  dw_set_fg(ic->status_win, *(CARD32*)sts_attr->value);
	} else if (!strcmp(XNBackground, sts_attr->name)) {
	  dw_set_bg(ic->status_win,*(CARD32*)sts_attr->value);
	} else if (!strcmp(XNBackgroundPixmap, sts_attr->name))
	  dw_set_bg_pixmap(ic->status_win, (Pixmap*)sts_attr->value);
	else if (!strcmp(XNFontSet, sts_attr->name)) {
	  dw_set_font(ic->status_win, sts_attr->value);
	} else if (!strcmp(XNLineSpace, sts_attr->name))
	  ic->status_win->linespace= *(CARD32*)sts_attr->value;
	else if (!strcmp(XNCursor, sts_attr->name))
	  dw_set_cursor(ic->status_win, (Cursor*)sts_attr->value);
	else {
	  g_warning("%s: fail: %s", __FUNCTION__, sts_attr->name);
	}
	XFree(sts_attr->value);
    }
}

IC *
ic_find_with_id(CARD16 icid)
{
    IC *ic = ic_list;

    while (ic != NULL) {
	if (ic->id == icid)
	    return ic;
	ic = ic->next;
    }
    return NULL;
}

IC *
ic_find(gpointer data)
{
    CARD16 id;
    id = ((IMChangeICStruct *)data)->icid;
    return ic_find_with_id(id);
}

int
call_geometry_cb(gpointer data)
{
    IC *ic = data;
    IMGeometryCBStruct geometry;
    if (!ic_is_valid(ic)) return 0;
    //g_print("%s: ic=%d\n",__FUNCTION__, ic->id);
    geometry.major_code = XIM_GEOMETRY;
    geometry.icid = ic->id;
    geometry.connect_id = ic->connect_id;
    IMCallCallback(xims, (gpointer)&geometry);
    return 0;
}

void
ic_create(IMChangeICStruct *call_data)
{
    IC *ic;

    ic = ic_new();
    if (ic == NULL)
      return;
    ic_store(ic, call_data);
    call_data->icid = ic->id;
    ic->connect_id = call_data->connect_id;
    if (ic->status_win && ic->status_win->area.x < 0) {
	/* seem to be have no effects */
	//call_geometry_cb(ic);
    }
}

void
ic_set(IMChangeICStruct *call_data)
{
    IC *ic = ic_find(call_data);
    //g_print("%s\n",__FUNCTION__);

    if (ic == NULL)
      return;
    ic_store(ic, call_data);
    return;
}

void
ic_get(IMChangeICStruct *call_data)
{
    XICAttribute *ic_attr = call_data->ic_attr;
    XICAttribute *pre_attr = call_data->preedit_attr;
    XICAttribute *sts_attr = call_data->status_attr;
    register int i;
    IC *ic = ic_find(call_data);

    if (ic == NULL)
      return;
    for (i = 0; i < (int)call_data->ic_attr_num; i++, ic_attr++) {
	//g_print("%s: attr = %s; request\n", __FUNCTION__,ic_attr->name);
	if(ic_attr->name == NULL) continue;
	if (!strcmp(XNInputStyle, ic_attr->name)) {
	  ic_attr->value = (void *)malloc(sizeof(CARD32));
	  *(CARD32*)ic_attr->value = ic->input_style;
	  ic_attr->value_length = sizeof(CARD32);
	} else if (!strcmp(XNFilterEvents, ic_attr->name)) {
	    ic_attr->value = (void *)malloc(sizeof(CARD32));
	    *(CARD32*)ic_attr->value = KeyPressMask|KeyReleaseMask;
	    //*(CARD32*)ic_attr->value = NoEventMask;
	    ic_attr->value_length = sizeof(CARD32);
	} else if (!strcmp(XNPreeditState, ic_attr->name)) {
	    ic_attr->value = (void *)malloc(sizeof(CARD32));
	    *(CARD32*)ic_attr->value = ic->composing_hangul;
	    ic_attr->value_length = sizeof(CARD32);
	} else {
	    g_print("%s: attr = %s; fail\n", __FUNCTION__,ic_attr->name);
	}
    }

    /* preedit attributes */
    for (i = 0; i < (int)call_data->preedit_attr_num; i++, pre_attr++) {
	if (ic->edit_win == NULL) break;
	if (!strcmp(XNArea, pre_attr->name)) {
	    pre_attr->value = (void *)malloc(sizeof(XRectangle));
	    dw_get_area(ic->edit_win, (XRectangle*)pre_attr->value);
	    pre_attr->value_length = sizeof(XRectangle);
	} else if (!strcmp(XNAreaNeeded, pre_attr->name)) {
	    pre_attr->value = (void *)malloc(sizeof(XRectangle));
	    dw_get_area_needed(ic->edit_win, (XRectangle*)pre_attr->value);
	    pre_attr->value_length = sizeof(XRectangle);
	} else if (!strcmp(XNSpotLocation, pre_attr->name)) {
	    pre_attr->value = (void *)malloc(sizeof(XPoint));
	    dw_get_pos(ic->edit_win, (XPoint*)pre_attr->value);
	    pre_attr->value_length = sizeof(XPoint);
	} else if (!strcmp(XNFontSet, pre_attr->name)) {
	    CARD16 base_len = (CARD16)strlen(ic->edit_win->fontname);
	    int total_len = sizeof(CARD16) + (CARD16)base_len;
	    char *p;

	    pre_attr->value = (void *)malloc(total_len);
	    p = (char *)pre_attr->value;
	    memmove(p, &base_len, sizeof(CARD16));
	    p += sizeof(CARD16);
	    strncpy(p, ic->edit_win->fontname, base_len);
	    pre_attr->value_length = total_len;
	} else if (!strcmp(XNForeground, pre_attr->name)) {
	    pre_attr->value = (void *)malloc(sizeof(long));
	    *(long*)pre_attr->value = ic->edit_win->fg;
	    pre_attr->value_length = sizeof(long);
	} else if (!strcmp(XNBackground, pre_attr->name)) {
	    pre_attr->value = (void *)malloc(sizeof(long));
	    *(long*)pre_attr->value = ic->edit_win->bg;
	    pre_attr->value_length = sizeof(long);
	} else if (!strcmp(XNLineSpace, pre_attr->name)) {
	    pre_attr->value = (void *)malloc(sizeof(long));
	    *(long*)pre_attr->value = ic->edit_win->linespace;
	    pre_attr->value_length = sizeof(long);
	} else {
	    g_warning("%s: preedit_attr = %s; fail", __FUNCTION__,pre_attr->name);
	}
    }

    /* status attributes */
    for (i = 0; i < (int)call_data->status_attr_num; i++, sts_attr++) {
	if (ic->status_win == NULL) break;
	//    g_print("%s: sts_attr = %s; request\n", __FUNCTION__,sts_attr->name);
	if (!strcmp(XNArea, sts_attr->name)) {
	    sts_attr->value = (void *)malloc(sizeof(XRectangle));
	    dw_get_area(ic->status_win, (XRectangle*)sts_attr->value);
	    sts_attr->value_length = sizeof(XRectangle);
	} else if (!strcmp(XNAreaNeeded, sts_attr->name)) {
	    sts_attr->value = (void *)malloc(sizeof(XRectangle));
	    dw_get_area_needed(ic->status_win, (XRectangle*)sts_attr->value);
	    sts_attr->value_length = sizeof(XRectangle);
	} else if (!strcmp(XNFontSet, sts_attr->name)) {
	    CARD16 base_len = (CARD16)strlen(ic->status_win->fontname);
	    int total_len = sizeof(CARD16) + (CARD16)base_len;
	    char *p;

	    sts_attr->value = (void *)malloc(total_len);
	    p = (char *)sts_attr->value;
	    memmove(p, &base_len, sizeof(CARD16));
	    p += sizeof(CARD16);
	    strncpy(p, ic->status_win->fontname, base_len);
	    sts_attr->value_length = total_len;
	} else if (!strcmp(XNForeground, sts_attr->name)) {
	    sts_attr->value = (void *)malloc(sizeof(long));
	    *(long*)sts_attr->value = ic->status_win->fg;
	    sts_attr->value_length = sizeof(long);
	} else if (!strcmp(XNBackground, sts_attr->name)) {
	    sts_attr->value = (void *)malloc(sizeof(long));
	    *(long*)sts_attr->value = ic->status_win->bg;
	    sts_attr->value_length = sizeof(long);
	} else if (!strcmp(XNLineSpace, sts_attr->name)) {
	    sts_attr->value = (void *)malloc(sizeof(long));
	    *(long*)sts_attr->value = ic->status_win->linespace;
	    sts_attr->value_length = sizeof(long);
	} else {
	    g_warning("%s: sts_attr = %s; fail", __FUNCTION__,sts_attr->name);
	}
    }
}


#ifdef XID_DEBUG
#include <gdk/gdkprivate.h>
#include <stdio.h>

static guint gdk_xid_hash    (XID *xid);
static gint  gdk_xid_compare (XID *a,
			      XID *b);


static GHashTable *xid_ht = NULL;


void
gdk_xid_table_insert (XID      *xid,
		      gpointer  data)
{
  g_return_if_fail (xid != NULL);

  if (!xid_ht)
    xid_ht = g_hash_table_new ((GHashFunc) gdk_xid_hash,
			       (GCompareFunc) gdk_xid_compare);

  g_hash_table_insert (xid_ht, xid, data);
}

void
gdk_xid_table_remove (XID xid)
{
  if (!xid_ht)
    xid_ht = g_hash_table_new ((GHashFunc) gdk_xid_hash,
			       (GCompareFunc) gdk_xid_compare);

  g_hash_table_remove (xid_ht, &xid);
}

gpointer
gdk_xid_table_lookup (XID xid)
{
  gpointer data = NULL;

  if (xid_ht)
    data = g_hash_table_lookup (xid_ht, &xid);
  
  return data;
}


static guint
gdk_xid_hash (XID *xid)
{
  return *xid;
}

static gint
gdk_xid_compare (XID *a,
		 XID *b)
{
  return (*a == *b);
}

static gint
gdk_xid_table_size(void)
{
  if (xid_ht) return g_hash_table_size(xid_ht);
  return 0;
}
#endif
