/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "ic.h"
#include "ami.h"
#include "applet.h"
#include "hinput.h"
#include "dw.h"
#include "edit.h"
#include "handler.h"
#include "cli.h"
#include "cb.h"
#include "si.h"
#include "hanja.h"
#include "util.h"
#include <gdk/gdkkeysyms.h>
#include "comp.h"
#include "utf8.h"

IC *current_focus_ic = NULL;


int
handler_ic_get(XIMS ims, IMChangeICStruct *call_data)
{
    ic_get(call_data);
    return TRUE;
}

int
handler_ic_set(XIMS ims, IMChangeICStruct *call_data)
{
    ic_set(call_data);
    return TRUE;
}

int
handler_open(XIMS ims, IMOpenStruct *call_data)
{
    if (debug) g_print("%s\n", __FUNCTION__);
    return TRUE;
}

int
handler_close(XIMS ims, IMCloseStruct *call_data)
{
    if (debug) g_print("%s\n", __FUNCTION__);
    return TRUE;
}

int
handler_ic_create(XIMS ims, IMChangeICStruct *call_data)
{
    if (debug) g_print("%s\n",__FUNCTION__);
    ic_create(call_data);
    return TRUE;
}

int
handler_ic_destroy(XIMS ims, IMChangeICStruct *call_data)
{
    IC *ic;
    ic = ic_find(call_data);
    if (ic == NULL) {
	return TRUE;
    }
    if (debug) g_print("%s ic=%d\n",__FUNCTION__, ic->id);
    ic_destroy(ic);
    return TRUE;
}

static int
handler_match_keys2( int state, KeySym keysym, XIMTriggerKey *trigger)
{
    while (trigger->keysym) {
	if (((KeySym)trigger->keysym == keysym)
	    && ((state & trigger->modifier_mask) == trigger->modifier))
	  return TRUE;
	trigger++;
    }
    return FALSE;
}

#define STRBUFLEN 64
static int
handler_match_keys(XIMS ims, IMForwardEventStruct *call_data, XIMTriggerKey *trigger)
{
    char strbuf[STRBUFLEN];
    KeySym keysym;
    XKeyEvent *kev;

    memset(strbuf, 0, STRBUFLEN);
    kev = (XKeyEvent*)&call_data->event;
    XLookupString(kev, strbuf, STRBUFLEN, &keysym, NULL);
    return handler_match_keys2(kev->state, keysym, trigger);
}

static int
check_hotkey(IC *ic, int state, KeySym keysym)
{
    if (ami_editing_mode == AMI_EDITING_WORD_MODE) {
	/* check valid key stroke for line editing */
	if (keysym == GDK_Left || keysym == GDK_KP_Left) {
	    editing_clear_temp(ic);
	    if (editing_move(ic, -1)) {
		return HOTKEY_DONE;
	    } else {
		return HOTKEY_FORWARD;
	    }
	}
	if (keysym == GDK_Right || keysym == GDK_KP_Right) {
	    editing_clear_temp(ic);
	    if (editing_move(ic, 1)) {
		return HOTKEY_DONE;
	    } else {
		return HOTKEY_FORWARD;
	    }
	}
	if (keysym == GDK_Delete || keysym == GDK_KP_Delete) {
	    editing_clear_temp(ic);
	    if (editing_forward_delete(ic)) {
		return HOTKEY_DONE;
	    } else {
		return HOTKEY_FORWARD;
	    }
	}
	if (keysym == GDK_a && (state & ControlMask)) {
	    editing_clear_temp(ic);
	    if (editing_move(ic, -1000)) {
		return HOTKEY_DONE;
	    } else {
		return HOTKEY_FORWARD;
	    }
	}
	if (keysym == GDK_e && (state & ControlMask)) {
	    editing_clear_temp(ic);
	    if (editing_move(ic, 1000)) {
		return HOTKEY_DONE;
	    } else {
		return HOTKEY_FORWARD;
	    }
	}
    }
    if ( handler_match_keys2 (state, keysym, flush_keys) ) {
	editing_flush(ic);
	return HOTKEY_DONE;
    }
    if (handler_match_keys2(state, keysym, hanja_keys)) {
	editing_clear_temp(ic);
	ic->han->aux_input = hanja_dialog(ic->han);
	if (ic->han->aux_input) {
	    gtk_object_set_data((GtkObject *)ic->han->aux_input, "ic", ic);
	    gtk_object_set_data((GtkObject *)ic->han->aux_input, "han", ic->han);
	}
	return HOTKEY_DONE;
    }
    if (handler_match_keys2(state, keysym, special_char_keys)) {
	editing_clear_temp(ic);
	ic->han->aux_input = special_char_input();
	gtk_object_set_data((GtkObject *)ic->han->aux_input, "ic", ic);
	gtk_object_set_data((GtkObject *)ic->han->aux_input, "han", ic->han);
	return HOTKEY_DONE;
    }
    if (keysym >= GDK_KP_Space && keysym <= GDK_KP_9) {
	/* numeric key pad */
	return HOTKEY_FORWARD;
    }

    if (keysym >= GDK_Shift_L && keysym <= GDK_Hyper_R) return HOTKEY_DONE;
    if (keysym >= GDK_Mode_switch && keysym <= GDK_Num_Lock) return HOTKEY_DONE;

    /* FIXME */
    if (keysym == GDK_Delete || state & ( ControlMask | Mod1Mask | Mod4Mask)) {
	return HOTKEY_FORWARD;
    }
    return HOTKEY_CONTINUE;
}

static int
real_event_send(IMForwardEventStruct *call_data)
{
    IMForwardEvent(xims, (gpointer)call_data);
    g_free(call_data);
    return 0;
}

/*
 * When commit and ForwardEvent are sent together, some clients get them
 * in the reverse order. To avoid that, we send ForwardEvent after
 * some delay. idle_add wouldn't help here. 
*/
static int
send_event_later(IMForwardEventStruct *call_data)
{
    IMForwardEventStruct *data = g_new(IMForwardEventStruct, 1);
    *data = *call_data;
    gtk_timeout_add(50, (GtkFunction)real_event_send, (gpointer)data);
    return 0;
}


int
handler_process_key(XIMS ims, IMForwardEventStruct *call_data)
{
    char strbuf[STRBUFLEN];
    KeySym keysym;
    XKeyEvent *kev;
    int count;
    int hot_key;
    IC *ic;
    guchar c;
    Composer *composer;

    ic = ic_find(call_data);
    if (!ic->composing_hangul || !ic->han) {
	/* If it's in English mode or it doesn't have the focus, 
	 * return the event. */
	IMForwardEvent(ims, (gpointer)call_data);
	return 0;
    };

    if (ic->han->aux_input) {
	GtkWidget *w = ((GtkWindow *)ic->han->aux_input)->focus_widget;
	GdkEventKey ev;
	gboolean ret = FALSE;
	XLookupString((XKeyEvent *)&call_data->event, strbuf, STRBUFLEN, &keysym, NULL);
	ev.keyval = keysym;
	if (w)
	    gtk_signal_emit_by_name((GtkObject *)w, "key_press_event", &ev, &ret);
	if (!ret) {
	    gtk_signal_emit_by_name((GtkObject *)ic->han->aux_input, "key_press_event", &ev, &ret);
	}
	return 0;
    }

    memset(strbuf, 0, STRBUFLEN);
    kev = (XKeyEvent*)&call_data->event;

    if (kev->state & LockMask) kev->state &= ~LockMask;

    count = XLookupString(kev, strbuf, STRBUFLEN, &keysym, NULL);
    strbuf[count] = 0;

    if (ic != current_focus_ic) {
	g_warning("%s: this ic(%d) is not current focus ic(%d)",__FUNCTION__, ic->id, current_focus_ic?current_focus_ic->id:-1);
	handler_ic_focus_set(ims, (IMChangeFocusStruct *)call_data);
    }

    hot_key = check_hotkey(ic, kev->state, keysym);
    composer = ic->han->composer;

    if (hot_key == HOTKEY_DONE)
	return 1;
    c = strbuf[0];
    if (keysym == GDK_BackSpace) c = '\b';
    if (c != '\b' &&
        (hot_key == HOTKEY_FORWARD || (count == 0) ||
	    (keysym == GDK_space && ic->han->pos == ic->han->len) ||
	    (count == 1 && (c < '!' || c > '~')))) {
	int l;

	composer_reset(composer);
	l = ic->han->len;
	if (l > 0) {
	    editing_flush(ic);
	}
	if (esc_han_toggle && keysym == GDK_Escape) {
	    editing_toggle(ic);
	    draw_applet_image(AMI_PIXMAP_ENGLISH);
	    IMPreeditEnd(ims, (gpointer)call_data);
	}
	if (l > 0)
	    send_event_later(call_data);
	else
	    IMForwardEvent(ims, (gpointer)call_data);
	return 1;
    }
    switch (composer_process (composer, strbuf[0])) {
	case COMPOSER_RAW:
	    if (c == '\b') {
		if (ic->han->pos > 0) {
		    editing_backward_delete(ic);
		} else {
		    IMForwardEvent(ims, (gpointer)call_data);
		}
		return 1;
	    }
	    editing_flush_with_str(ic, strbuf);
	    break;
	case COMPOSER_NULL:
	    editing_backward_delete(ic);
	    break;
	case COMPOSER_NOT_HAN:
	    editing_flush_with_str(ic, composer_get_buffer(composer));
	    break;
	case COMPOSER_CORRECT:
	    editing_correct(ic, composer_get_buffer(composer));
	    break;
	case COMPOSER_NEW:
	{
	    char *inputbuf = composer_get_buffer(composer);
	    int len=2;
            if ( ami_codeset==AMI_UTF8 && IS_UTF8_3BYTE(inputbuf[0])) 
		len=3;
	    if (ami_editing_mode == AMI_EDITING_CHAR_MODE
		&& ic->han->pos > 0) {
		editing_flush_without_reset(ic);
		editing_insert(ic, inputbuf, len);
		return 1;
	    }
	    editing_insert(ic, inputbuf, len);
	    break;
	}
	case COMPOSER_CORRECT_NEW:
	    if (ami_editing_mode == AMI_EDITING_CHAR_MODE) {
		char buf[5];
		char *inputbuf = composer_get_buffer(composer);
		int len=MBC_LEN(inputbuf[0]);
		strncpy(buf,inputbuf,len);
 	        buf[len] = '\0';

		editing_correct(ic, buf);
		editing_flush_without_reset(ic);
		editing_insert(ic, inputbuf + len, len);
		return 1;
	    }
	    editing_correct_insert(ic, composer_get_buffer(composer));
	    break;
	case COMPOSER_ERROR:
	    XBell(gdk_display, (bell_intensity>-80)?bell_intensity-20:-100);
	    break;

	default:
	    break;
    }
    return TRUE;
}

int
handler_forward_event(XIMS ims, IMForwardEventStruct *call_data)
{
    /* Lookup KeyPress Events only */
    if (call_data->event.type != KeyPress) return TRUE;

    /* In case of Static Event Flow */
    if (!use_trigger) {
	IC *ic = ic_find(call_data);
	if (ic == NULL || ic->han == NULL) return TRUE;
	if (handler_match_keys(ims, call_data, trigger_keys)) {
	    editing_toggle(ic);
	    draw_applet_image(AMI_PIXMAP_ENGLISH);
	    return TRUE;
	}
	if (!ic->han->composing_hangul) {
	    IMForwardEvent(ims, (gpointer)call_data);
	    return TRUE;
	}
    }

    /* In case of Dynamic Event Flow without registering OFF keys,
       the end of preediting must be notified from IMserver to
       IMlibrary. */
    if (use_trigger && !use_offkey) {
	if (handler_match_keys(ims, call_data, trigger_keys)) {
	    IC *ic = ic_find(call_data);
	    if (ic == NULL || ic->han == NULL) return TRUE;
	    editing_toggle(ic);
	    draw_applet_image(AMI_PIXMAP_ENGLISH);
	    IMPreeditEnd(ims, (gpointer)call_data);
	    if (debug) g_print("%s:preedit_end \n", __FUNCTION__);
	    return TRUE;
	}
    }
    handler_process_key(ims, call_data);
    return TRUE;
}

int
handler_trigger_notify(XIMS ims, IMTriggerNotifyStruct *call_data)
{
    IC *ic;

    draw_applet_image(AMI_PIXMAP_HANGUL);
    if (call_data->flag == 0) {	/* on key */
	/* Here, the start of preediting is notified from IMlibrary, which
	   is the only way to start preediting in case of Dynamic Event
	   Flow, because ON key is mandatary for Dynamic Event Flow. */
	ic = ic_find(call_data);
	if (debug) g_print("%s:preedit_start new_ic=%d\n",__FUNCTION__, ic->id);
	if (ic != current_focus_ic) handler_ic_focus_set(ims, (IMChangeFocusStruct *)call_data);

	IMPreeditStart(ims, (gpointer)call_data);
	if (!ic->composing_hangul) editing_toggle(ic);
	return TRUE;
    } else if (use_offkey && call_data->flag == 1) {	/* off key */
	/* Here, the end of preediting is notified from the IMlibrary, which
	   happens only if OFF key, which is optional for Dynamic Event Flow,
	   has been registered by IMOpenIM or IMSetIMValues, otherwise,
	   the end of preediting must be notified from the IMserver to the
	   IMlibrary. */
	if (debug) g_print("%s:preedit_end\n",__FUNCTION__);
	ic = ic_find(call_data);
	editing_toggle(ic);
	return TRUE;
    } else {
	/* never happens */
	if (debug) g_print("%s:never\n",__FUNCTION__);
	return FALSE;
    }
}

int
handler_preedit_start_reply(XIMS ims, IMPreeditCBStruct *call_data)
{
    if (debug) g_print("%s\n", __FUNCTION__);
    return TRUE;
}

int
handler_preedit_caret_reply(XIMS ims, IMPreeditCBStruct *call_data)
{
    g_warning("NYI: %s", __FUNCTION__);
    return TRUE;
}

int
handler_str_conversion_reply(XIMS ims, IMStrConvCBStruct *call_data)
{
    /* IMdkit does not seem to support this protocol */
    IC *ic = ic_find(call_data);
    g_return_val_if_fail(ic != NULL, FALSE);
    return TRUE;
}

int
handler_ic_focus_unset(XIMS ims, IMChangeFocusStruct *call_data)
{
    IC *ic;
    ic = ic_find(call_data);
    if (ic == NULL) return 0;
    if(debug) g_print("%s: ic=%d\n", __FUNCTION__, ic->id);
    if (ic->han == NULL) return FALSE;
    if (ic != current_focus_ic) return FALSE;
    if (ic->edit_win) {
	dw_unset_focus(ic->edit_win);
    }
    if (ic->status_win) {
	dw_unset_focus(ic->status_win);
	cli_unset_status_ic(ic);
    }
    current_focus_ic = NULL;
    draw_applet_image(AMI_PIXMAP_NONE);
    return TRUE;
}

int
handler_ic_focus_set(XIMS ims, IMChangeFocusStruct *call_data)
{
    IC *ic;
    ic = ic_find(call_data);

    if (ic_is_valid(current_focus_ic)) {
	/* If focus set is invoked without unset focus called upon,
	 * the order of Focus events can be reversed when the
	 * focusing moves from one window to the other (???)
	 */
	if (current_focus_ic->edit_win) {
	    dw_unset_focus(current_focus_ic->edit_win);
	}
	if (current_focus_ic->status_win) {
	    dw_unset_focus(current_focus_ic->status_win);
	}
	current_focus_ic = NULL;
    }
    g_return_val_if_fail(ic != NULL, FALSE);

    if (ic->top_win == NULL) {
	if (!ic->client_win) ic->client_win = GDK_ROOT_PARENT();
	/* Obtain top level window on X window. */
	/* In case client window and focus window are different as in Netscape
	 * 4.x(?), assume the correct client window info. is carried over.
	 */
	if (ic->focus_win && ic->client_win != ic->focus_win) {
	    ic->top_win = ic->client_win;
	} else if (ic->client_win == GDK_ROOT_PARENT()) {
	    ic->top_win = GDK_ROOT_PARENT();
	} else {
	    /* For gtk+ entry and text widgets, client window is always
	     * the same as focus window so that we need to get it for 
	     * ourselves.*/
	    ic->top_win = util_get_top_window (ic->client_win);
	    if (ic->top_win == NULL)
	    {
		if (debug) g_print("Cannot get ic top_win\n");
		ic->top_win = ic->client_win;
	    }
	}
    }

    cli_set_edit_ic(ic);
    if (debug) g_print("%s: ic=%d han_len=%d han=%p "
                       "han->buf=%p han->buf=%s\n", __FUNCTION__,
		       ic->id, ic->han->len, ic->han,
		       ic->han->buf, ic->han->buf);
    if (ic->composing_hangul != ic->han->composing_hangul) {
	if (ic->han->composing_hangul) {
	    IMPreeditStart(ims, (gpointer)call_data);
	} else {
	    IMPreeditEnd(ims, (gpointer)call_data);
	}
	ic->composing_hangul = ic->han->composing_hangul;
    }
    if (ic->input_style & XIMStatusCallbacks) {
	cb_status_draw(ic);
    }
    if (ic->han->composing_hangul)  {
	    draw_applet_image(AMI_PIXMAP_HANGUL);
    } else {
	    draw_applet_image(AMI_PIXMAP_ENGLISH);
    }
    cli_set_status_ic(ic);
    current_focus_ic = ic;
    if (ic->han->aux_input) {
	gtk_object_set_data((GtkObject *)ic->han->aux_input, "ic", ic);
    }
    return TRUE;
}

void (*cb_cleanup)(CARD16) = NULL;

static void
do_cb_cleanup(CARD16 id)
{
    IC *ic = ic_find_with_id(id);
    if (ic->han && ic->han->buf) editing_reset(ic);
    else cb_edit_reset(ic);
    cb_cleanup = NULL;
}

int
handler_ic_reset(XIMS ims, IMResetICStruct *call_data)
{
    /* FIXME */
    IC *ic;
    char *commit_string;
    ic = ic_find(call_data);
    g_return_val_if_fail (ic != NULL, FALSE);
    if (debug) g_print("%s ic=%d\n",__FUNCTION__, ic->id);
    if (ic->han == NULL || ic->han->buf == NULL) {
	commit_string = g_strdup(""); //editing_mbstocts(ic->han->buf);
	call_data->commit_string = commit_string;
	call_data->length = strlen(commit_string);
        if (ic->input_style & XIMPreeditCallbacks)
	    cb_cleanup = do_cb_cleanup;
	return TRUE;
    }
    composer_reset(ic->han->composer);
    commit_string = editing_mbstocts(ic->han->buf);
    call_data->commit_string = commit_string;
    call_data->length = strlen(commit_string);
    if (ic->input_style & XIMPreeditCallbacks) {
	cb_cleanup = do_cb_cleanup;
    } else {
	editing_reset(ic);
    }
    return TRUE;
}
