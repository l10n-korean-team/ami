/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "config.h"

#include <gdk/gdkkeysyms.h>
#include <sys/stat.h>
#include <unistd.h>

#include "amitype.h"
#include "ic.h"
#include "edit.h"
#include "chartable.h"
#include "ami.h"
#include "util.h"
#include "utf8.h"

typedef struct {
    off_t filesize;
    int fd;
    char *buf;
    int ref;
} HanjaDic;

static HanjaDic *hanjadic = NULL;

static void hanja_dialog_destroyed(GtkWidget *widget);
static void hanja_dict_load(void);
static void hanja_dialog_selected(GtkWidget *widget, char *selected);
static void hanja_dict_close(void);
static char * hanja_find_word(char *search);


guchar **
hanja_find_candidates(guchar *hanbuf, int current_pos, int *num)
{
    guchar *search;
    guchar *buf[1000];
    guchar **candidates;
    int i;
    *num = 0;
    if (current_pos <= 0) return NULL;
    search = g_new(guchar, current_pos + 2);
    strncpy(search, hanbuf, current_pos);
    search[current_pos] = ' ';
    search[current_pos+1] = '\0';
    for (i = 0; i < current_pos; i += MBC_LEN(search[i])) {
	char *s = hanja_find_word(search + i);
	if (s) {
	    char *last = strchr(s, '\n');
	    char *s2;
	    int len;
	    if (last == NULL) len = strlen(s);
	    else len = last - s;
	    s2 = g_new0(char, len+1);
	    strncpy(s2, s, len);
	    s = strtok(s2, " ");
	    while (s) {
		s = strtok(NULL, " ");
		if (s) buf[(*num)++] = g_strdup(s);
	    }
	    g_free(s2);
	}
	    g_free(s);
    }
    g_free(search);
    if (*num == 0) return NULL;
    candidates = g_new (guchar *, (*num)+1);
    for (i=0;i<(*num);i++) candidates[i] = buf[i];
    candidates[(*num)] = NULL;
    return candidates;
}

static int
key_press_cb(GtkWidget *w, GdkEventKey *ev)
{
    if (ev->keyval == GDK_Escape) {
	gtk_widget_destroy(w);
	return TRUE;
    }
    return FALSE;
}

static int
hanja_get_total_len(guchar **words, int max_col)
{
    int total_len = 0;
    int col = 0;

    while (*words) {
	int len;
	len = util_get_mb_strlen(*words,-1);
	if (col + len > max_col) {
	    total_len += max_col - col;
	    col = len;
	} else {
	    col += len;
	}
	total_len += len;
	words++;
	if (col == max_col) col = 0;
    }
    return total_len;
}

static void
hanja_subst_mode_toggle_cb(GtkWidget *w, gpointer data)
{
    if (GTK_TOGGLE_BUTTON(w)->active) {
	ami_hanja_subst_mode = GPOINTER_TO_INT(data);
#if 0
	CharTable *table;
	table = gtk_object_get_data((GtkObject *)gtk_widget_get_toplevel(w), "ctable");
	char_table_selected(table);
#endif
    }
}

GtkWidget *
hanja_dialog(HangulState *han)
{
    GtkWidget *win, *vbox, *ctable, *frame;
    GtkWidget *hbox, *b;
    guchar **candidates;
    guchar **table_buf;
    guchar **s;
    gchar **hanja_formats;
    GString *fmts_xlat;
    int col, max_col, total_len, len, max_len;
    int i, k, num;
    GSList *group;

    if (!hanjadic) hanja_dict_load();
    if (!hanjadic) return NULL;
    candidates = hanja_find_candidates(han->buf, han->pos, &num);
    if (candidates == NULL) return NULL;

    max_len = util_get_mb_strlen(candidates[0],-1);

    if (num < 20) max_col = MAX(max_len, 5);
    else max_col = MAX(max_len, 10);

    total_len = hanja_get_total_len(candidates, max_col);

    if (total_len < max_col) max_col = total_len;
    else if (total_len < max_len*max_len) {
	max_col = max_len;
	total_len = hanja_get_total_len(candidates, max_len);
    }

    table_buf = g_new(guchar *, total_len);
    col = 0;
    k = 0;
    s = candidates;
    while (*s) {
	len = util_get_mb_strlen(*s,-1);
	if (col + len > max_col) {
	    while(col++<max_col) table_buf[k++] = NULL;
	    col = len;
	} else {
	    col += len;
	}
	table_buf[k++] = g_strdup(*s);
	if (len > 1) {
	    for(i=1;i<len;i++) table_buf[k++] = NULL;
	}
	s++;
	if (col == max_col) col = 0;
    }

    win = gtk_window_new(GTK_WINDOW_DIALOG);
    gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_CENTER);
    gtk_window_set_title(GTK_WINDOW(win), _("Ami: Hanja Input"));
    gtk_signal_connect((GtkObject *)win, "key_press_event", (GtkSignalFunc)key_press_cb, NULL);
    gtk_container_set_border_width(GTK_CONTAINER(win), 10);
    gtk_signal_connect(GTK_OBJECT(win), "destroy",
    	(GtkSignalFunc)hanja_dialog_destroyed, NULL);

    vbox = gtk_vbox_new(FALSE, 2);
    gtk_container_add(GTK_CONTAINER(win), vbox);

    frame = gtk_frame_new(NULL);
    gtk_frame_set_shadow_type((GtkFrame *)frame, GTK_SHADOW_ETCHED_OUT);
    gtk_container_add(GTK_CONTAINER(vbox), frame);

    ctable = char_table_new((char **)table_buf, max_col, total_len);
    gtk_object_set_data(GTK_OBJECT(win), "ctable", ctable);
    gtk_container_add((GtkContainer *)frame, ctable);
    gtk_signal_connect(GTK_OBJECT(ctable), "selected",
    	(GtkSignalFunc)hanja_dialog_selected, NULL);

    for(i=0;i<num;i++) g_free(candidates[i]);
    g_free(candidates);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(vbox), hbox);

    fmts_xlat = util_xlate_hanja_fmts();
    hanja_formats = g_strsplit(fmts_xlat->str, ":", -1);
    group = NULL;
    for(i=0;hanja_formats[i] != NULL; i++) {
	b = gtk_radio_button_new_with_label(group, hanja_formats[i]);
	group = gtk_radio_button_group(GTK_RADIO_BUTTON(b));
	gtk_container_add(GTK_CONTAINER(hbox), b);
	if (ami_hanja_subst_mode == (i+1))
	    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b), TRUE);
	gtk_signal_connect(GTK_OBJECT(b), "toggled",
		(GtkSignalFunc)hanja_subst_mode_toggle_cb,
		GINT_TO_POINTER(i+1));
    }
    g_strfreev(hanja_formats);
    g_string_free(fmts_xlat, TRUE);

    gtk_widget_show_all(win);
    gtk_widget_grab_focus(ctable);
    return win;
}

static void
hanja_dialog_selected(GtkWidget *widget, char *selected)
{
    GtkWidget *top;
    IC *ic;

    top = gtk_widget_get_toplevel(widget);
    ic = (IC *)gtk_object_get_data((GtkObject *)top, "ic");
    if (ic == NULL || !ic_is_valid(ic) || ic->han == NULL) {
	g_warning("%s: Invalid IC", __FUNCTION__);
    } else {
	if (ic->han == NULL) {
	    g_warning("%s: Hangul Buffer attached to special chars dialog is null",__FUNCTION__);
	} else {
	    if (ic->han == gtk_object_get_data((GtkObject *)top, "han"))
		editing_hanja_replace(ic, selected);
	    else g_warning("%s: han is different from original.",__FUNCTION__);
	}
    }
    gtk_widget_destroy(top);
}

static void
hanja_dialog_destroyed(GtkWidget *widget)
{
    IC *ic;
    HangulState *han;
    ic = (IC *)gtk_object_get_data((GtkObject *)widget, "ic");
    if (ic == NULL) return;
    han = (HangulState *)gtk_object_get_data((GtkObject *)widget, "han");
    if (ic->han != han || han == NULL) return;
    han->aux_input = NULL;
    hanja_dict_close();
}


static void
hanja_dict_load(void)
{
    struct stat statbuf;
    off_t filesize;
    int fd;
    char *buf;

    if (hanjadic) {
	hanjadic->ref++;
	return;
    }

    fd = open("./hanja.dic", O_RDONLY);
    if (fd < 0) {
	fd = open(hanja_dic_filename, O_RDONLY);
    }
    if (fd < 0) {
	g_warning(_("Failed to open hanja dictionary %s."), hanja_dic_filename);
	g_print(_("To enter Hanja, you need to install Hanja dictionary %s.\n"
		  "Alternatively, you can convert a Hanja dictionary for HWP "
		  "to a\n format understood by Ami."), hanja_dic_filename);
	return ;
    }
    fstat(fd, &statbuf);
    filesize = statbuf.st_size;
    buf = g_new(guchar, filesize + 1);
    read(fd, buf, filesize);
    buf[filesize] = '\0';
    hanjadic = g_new(HanjaDic, 1);
    hanjadic->filesize = filesize;
    hanjadic->buf = buf;
    hanjadic->ref = 1;
    close(fd);
}

static void
hanja_dict_close(void)
{
    g_return_if_fail (hanjadic != NULL);
    hanjadic->ref--;
    if (hanjadic->ref != 0) return;
    g_free(hanjadic->buf);
    g_free(hanjadic);
    hanjadic = NULL;
}


/* The following functions are taken from gtkdic in gtkutils suit by NAM Sung-hyun */


static char *
go_next_line(char *s, char *last)
{
    while (s < last && *s != '\n')
	s++;
    if (s == last)
	return NULL;
    return s + 1;
}

/* Move to the beg. of a line that start with s. 
 * start is the beg. of this buffer and is the lower bound.
 */
static char *
go_start_of_line(char *s, char *start)
{
    if (s == start)
	return start;
    while (s > start && *s != '\n')
	s--;
    if (*s == '\n')
	return s + 1;
    return start;
}

static char *
hanja_find_word(char *word)
{
    char *e, *b, *s;
    int len, comp;
    static iconv_t icd_to_utf8=NULL;
    static iconv_t icd_to_euc=NULL;
    char *search;
    char *match;
    char *last;

    /* FIXME : eventually, we have to go the opposite direction.
     * that is, Hanja dict. should be in UTF-8 and in EUC-KR
     * locale, a search string has to be converted to UTF-8 before
     * dict. look-up. J. Shin */

    if ( ami_codeset==AMI_UTF8) {
        /* U+AC00 -> EAB080, U+D7A3 -> ED9EA3 */
	size_t ilen,olen;
	char *iptr=word;
	char *optr;
       
	ilen=olen=strlen(word)+1;
     	if (! IS_UTF8_3BYTE(word[0]) || (unsigned char)word[0] < 0xea
	      || (unsigned char)word[0] > 0xed )
	    return NULL;
    	if ( ! icd_to_euc || icd_to_euc == (iconv_t) -1 ) 
	    icd_to_euc=iconv_open(EUCKR_ICD,"UTF-8");
	search = g_new(gchar, strlen(word)+1);
	optr=search;
	if ( iconv(icd_to_euc,&iptr,&ilen,&optr,&olen) == -1) 
	    return NULL;
	if (ilen) *optr='\0';
    }
    else 
	search = word;

    if ((unsigned char)search[0] < 0xb0) return NULL;
    if ((unsigned char)search[0] > 0xc8) return NULL;

    len = strlen(search);
    e = hanjadic->buf + hanjadic->filesize;
    b = hanjadic->buf;
    if (strncmp(b, search, len) == 0) return b;

    /* bi-section-search for a line with a given pattern */
    while (1)
    {

	s = b + (e - b) / 2;
	if ((s = go_start_of_line(s, b)) == NULL)
	    return NULL;
	if (s == b)
	{
	    s = go_next_line(s, e);
	    if (s == NULL || s == e)
		return NULL;
	}
	comp = strncmp(s, search, len);
	if (comp == 0)
	    break;
	else if (comp > 0)
	    e = s;
	else
	    b = s;
    }

    last=strchr(s,'\n');
    if ( last!=NULL ) *last='\0';
    if ( ami_codeset==AMI_EUC) 
	match=g_strdup(s);
    else {
	size_t ilen=strlen(s)+1;
	size_t olen=ilen*2;
	char *iptr=s;
	char *optr;
	match=g_new(gchar,olen);
	optr=match;
    	if ( ! icd_to_utf8 || icd_to_utf8 == (iconv_t) -1 ) 
	    icd_to_utf8=iconv_open("UTF-8",EUCKR_ICD);
	if ( iconv(icd_to_utf8,&iptr,&ilen,&optr,&olen) == -1) {
	     g_warning("Something wrong with your dictionary.\n"
	         "Conversion from EUC-KR to UTF-8 failed.\n");
	     g_free(match);
	     match=NULL;
	}
	if (ilen) *optr='\0'; 
    }
    if ( last!=NULL ) *last='\n';
    return match;
}
