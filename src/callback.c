/* This file is a part of Ami software, (C) Hwang chi-deok 1999, 2001 */

#include "config.h"
#include "edit.h"
#include "ami.h"
#include "util.h"
#include "utf8.h"

void
cb_status_draw(IC *ic)
{
    IMStatusCBStruct data;
    XIMFeedback feedback[2]={0};
    XIMText text;
    char *txt;
    if (ic->composing_hangul) {
	txt = editing_mbstocts(hangul_mode_label);
    } else {
	txt = editing_mbstocts(english_mode_label);
    }
    data.major_code = XIM_STATUS_DRAW;
    data.todo.draw.data.text = &text;
    text.string.multi_byte = txt;
    text.length = strlen(txt);
    text.feedback = feedback;
    data.todo.draw.type = XIMTextType;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    IMCallCallback(xims, (gpointer)&data);
    if(debug) g_print("%s: %s\n", __FUNCTION__, txt);
}

int pop_mbc(unsigned char *d, char *stack)
{
    int len=MBC_LEN_B(stack-1); 
    strncpy(d,stack-len,len);
    d[len]='\0';
    return len;
}


void
cb_edit_clear_temp(IC *ic)
{
    IMPreeditCBStruct data;
    XIMText text;
    XIMFeedback feedback[2];
    char tmpbuf[4];
    char *cts;

    data.major_code = XIM_PREEDIT_DRAW;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    data.todo.draw.caret = util_get_mb_strlen(ic->han->buf, ic->han->pos);
    data.todo.draw.chg_first = data.todo.draw.caret - 1;
    data.todo.draw.chg_length = 1;

    data.todo.draw.text = &text;
    feedback[0] = XIMUnderline;
    feedback[1] = 0;
    text.feedback = feedback;

    pop_mbc((unsigned char*) tmpbuf, ic->han->buf+ic->han->pos); 

    cts = editing_mbstocts(tmpbuf);
    text.string.multi_byte = cts;
    text.length = strlen(cts);
    if (debug) g_print("%s\n",__FUNCTION__);
    IMCallCallback(xims, (gpointer)&data);
    XFree(cts);
}

void
cb_edit_correct_insert(IC *ic, char *buf)
{
    IMPreeditCBStruct data;
    XIMText text;
    XIMFeedback feedback[3];
    char *cts;
    int pos;

    if (debug) g_print("%s\n",__FUNCTION__);

    data.major_code = XIM_PREEDIT_DRAW;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    pos = util_get_mb_strlen(ic->han->buf, ic->han->pos);
    data.todo.draw.caret = pos + 1;
    data.todo.draw.chg_first = pos - 1;
    data.todo.draw.chg_length = 1;

    cts = editing_mbstocts(buf);
    data.todo.draw.text = &text;
    feedback[0] = XIMUnderline;
    feedback[1] = XIMReverse;
    feedback[2] = 0;
    text.feedback = feedback;
    text.string.multi_byte = cts;
    text.length = strlen(cts);
    if (debug) g_print("%s: text length=%d\n", __FUNCTION__, text.length);

    IMCallCallback(xims, (gpointer)&data);
    XFree(cts);
}

void
cb_edit_backward_delete(IC *ic)
{
    IMPreeditCBStruct data;
    XIMText text;
    XIMFeedback feedback = 0;
    int pos;
    if (debug) g_print("%s\n",__FUNCTION__);

    data.major_code = XIM_PREEDIT_DRAW;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    pos = util_get_mb_strlen(ic->han->buf, ic->han->pos);
    data.todo.draw.caret = pos - 1;
    data.todo.draw.chg_first = pos - 1;
    data.todo.draw.chg_length = 1;

    data.todo.draw.text = &text;
    text.feedback = &feedback;

    text.string.multi_byte = "";
    text.length = 0;
    IMCallCallback(xims, (gpointer)&data);
}

void
cb_edit_insert(IC *ic, char *buf)
{
    IMPreeditCBStruct data;
    XIMText text;
    char *cts;
    XIMFeedback feedback[3];
    int pos;
    int len = MBC_LEN(buf[0]);

    g_assert (buf[len] == '\0');

    if (debug) g_print("%s\n",__FUNCTION__);

    data.major_code = XIM_PREEDIT_DRAW;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    pos = util_get_mb_strlen(ic->han->buf, ic->han->pos);
    data.todo.draw.caret = pos + 1;
    data.todo.draw.text = &text;
    text.feedback = feedback;

    if (ic->han->pos >= 2) {
	unsigned char tmpbuf[9]; /* A char. in UTF-8 can take up up to 4 octets*/
	int l,l1;

	feedback[0] = XIMUnderline;
	feedback[1] = XIMReverse;
	feedback[2] = 0;
	l=pop_mbc(tmpbuf, ic->han->buf + ic->han->pos);
	l1=MBC_LEN(buf[0]);
	strncpy(tmpbuf+l,buf,l1);
	tmpbuf[l+l1]='\0';
	data.todo.draw.chg_length = 1;
	data.todo.draw.chg_first = pos - 1;
	cts = editing_mbstocts(tmpbuf);
	if (debug) g_print("tmpbuf=%s\n", tmpbuf);
    } else {
	feedback[0] = XIMReverse;
	feedback[1] = 0;
	data.todo.draw.chg_length = 0;
	data.todo.draw.chg_first = pos;
	if (debug) g_print("buf = %s\n", buf);
	cts = editing_mbstocts(buf);
    }
    text.string.multi_byte = cts;
    text.length = strlen(cts);
    if (debug) g_print("%s: len=%d\n",__FUNCTION__,text.length);

    IMCallCallback(xims, (gpointer)&data);
    XFree(cts);
}

void
cb_edit_correct(IC *ic, char *buf)
{
    IMPreeditCBStruct data;
    XIMText text;
    XIMFeedback feedback[2];
    char *cts;
    int pos;
    if (debug) g_print("%s\n",__FUNCTION__);

    data.major_code = XIM_PREEDIT_DRAW;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    pos = util_get_mb_strlen(ic->han->buf, ic->han->pos);
    data.todo.draw.caret = pos;
    data.todo.draw.chg_first = pos - 1;
    data.todo.draw.chg_length = 1;

    data.todo.draw.text = &text;
    feedback[0] = XIMReverse;
    feedback[1] = 0;
    text.feedback = feedback;

    cts = editing_mbstocts(buf);
    text.string.multi_byte = cts;
    text.length = strlen(cts);
    if (debug) g_print("%s: len=%d\n",__FUNCTION__,text.length);
    IMCallCallback(xims, (gpointer)&data);
    XFree(cts);
}

void
cb_edit_reset(IC *ic)
{
    IMPreeditCBStruct data;
    XIMText text;
    XIMFeedback feedback[1] = {0};
    if (debug) g_print("%s\n",__FUNCTION__);
    if (ic == NULL || ic->han == NULL) return;
    if (ic->han->buf[0] == 0) return;
    data.major_code = XIM_PREEDIT_DRAW;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    data.todo.draw.caret = 0;
    data.todo.draw.chg_first = 0;
    data.todo.draw.chg_length = util_get_mb_strlen(ic->han->buf, ic->han->len);
    data.todo.draw.text = &text;
    text.feedback = feedback;
    text.length = 0;
    text.string.multi_byte = "";
    IMCallCallback(xims, (gpointer)&data);
}

void
cb_edit_toggle(IC *ic)
{
    IMPreeditCBStruct data;
    cb_status_draw(ic);
    if (ic->han->composing_hangul) {
	data.major_code = XIM_PREEDIT_START;
    } else {
	data.major_code = XIM_PREEDIT_DONE;
    }
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    IMCallCallback(xims, (gpointer)&data);
}

void
cb_edit_move(IC *ic, int howmuch)
{
    IMPreeditCBStruct data;

    data.major_code = XIM_PREEDIT_CARET;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    data.todo.caret.position = util_get_mb_strlen(ic->han->buf, ic->han->pos);
    data.todo.caret.direction = XIMAbsolutePosition;
    data.todo.caret.style = XIMIsPrimary;
    IMCallCallback(xims, (gpointer)&data);
}

void
cb_edit_hanja_replace(IC *ic, const char *hangul, const char *hanja)
{
    IMPreeditCBStruct data;
    XIMText text;
    XIMFeedback *feedback;
    char *cts;
    char *buf;
    int chg_length;
    int hanja_length;
    int i, pos;
    if (debug) g_print("%s\n",__FUNCTION__);

    data.major_code = XIM_PREEDIT_DRAW;
    data.connect_id = ic->connect_id;
    data.icid = ic->id;
    buf = ic->han->buf;
    pos = util_get_mb_strlen(ic->han->buf, ic->han->pos);
    chg_length = util_get_mb_strlen(hangul, -1);
    data.todo.draw.chg_first = pos - chg_length;
    data.todo.draw.chg_length = chg_length;
    hanja_length = util_get_mb_strlen(hanja, -1);
    data.todo.draw.caret = pos - chg_length + hanja_length;
    feedback = g_new(XIMFeedback, hanja_length);
    for(i=0;i<hanja_length;i++) feedback[i] = XIMUnderline;
    if (debug) {
	g_print("chg_first = %d chg_length = %d\n", data.todo.draw.chg_first, data.todo.draw.chg_length);
	g_print("caret = %d\n", data.todo.draw.caret);
	g_print("text: %s\n", hanja);
    }

    cts = editing_mbstocts((char *)hanja);
    data.todo.draw.text = &text;
    text.feedback = feedback;
    text.string.multi_byte = cts;
    text.length = strlen(cts);
    if (debug) g_print("%s: text length=%d\n", __FUNCTION__, text.length);

    IMCallCallback(xims, (gpointer)&data);
    g_free(feedback);
    XFree(cts);
}
