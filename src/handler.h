#ifndef __HANDLER_H
#define __HANDLER_H
#include "amitype.h"

int handler_open(XIMS ims, IMOpenStruct *call_data);
int handler_close(XIMS ims, IMCloseStruct *call_data);
int handler_ic_get(XIMS ims, IMChangeICStruct *call_data);
int handler_ic_set(XIMS ims, IMChangeICStruct *call_data);
int handler_ic_create(XIMS ims, IMChangeICStruct *call_data);
int handler_ic_destroy(XIMS ims, IMChangeICStruct *call_data);
int IsMatchKeys(XIMS ims, IMForwardEventStruct *call_data, XIMTriggerKey *trigger);
int process_key(XIMS ims, IMForwardEventStruct *call_data);
int handler_forward_event(XIMS ims, IMForwardEventStruct *call_data);
int handler_trigger_notify(XIMS ims, IMTriggerNotifyStruct *call_data);
int handler_preedit_start_reply(XIMS ims, IMPreeditCBStruct *call_data);
int handler_preedit_caret_reply(XIMS ims, IMPreeditCBStruct *call_data);
int handler_str_conversion_reply(XIMS ims, IMStrConvCBStruct *call_data);
int handler_ic_focus_set(XIMS ims, IMChangeFocusStruct *call_data);
int handler_ic_focus_unset(XIMS ims, IMChangeFocusStruct *call_data);
int handler_ic_reset(XIMS ims, IMResetICStruct *call_data);

extern IC *current_focus_ic;

#endif
