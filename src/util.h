

GdkFont * util_fontset_load(char *name);
void util_font_unref(GdkFont *font);
GdkWindow * util_get_top_window (GdkWindow *win);
int util_get_mb_strlen(const char *s, int len);
GString* util_xlate_hanja_fmts();
