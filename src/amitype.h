#ifndef __AMITYPE_H
#define __AMITYPE_H

#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <Ximd/IMdkit.h>
#include <Ximd/Xi18n.h>
#include "comp.h"

typedef struct _IC IC;
typedef struct _HangulState HangulState;
typedef struct _DispWindow DispWindow;

#define HAN_STACK_SIZE 30
struct _HangulState {
    unsigned char	*buf;
    int			buf_size;
    int			pos;
    int			len;
    unsigned char	stack[HAN_STACK_SIZE];
    Composer		*composer;
    int			composing_hangul;
    GtkWidget	*aux_input;
};

struct _DispWindow {
    GdkWindow *win;
    GdkWindow *win2, *win3;
    GdkGC *gc, *reverse_gc;
    GdkFont *font;
    char *fontname;
    gchar *text;
    XRectangle area;
    XRectangle area_needed;
    Cursor	cursor;		/* cursor */
    Colormap	cmap;		/* colormap */
    Pixmap	bg_pixmap;	/* background pixmap */
    int x, y;
    int caret;  /* the cursor position in terms of 'Hangul' character
		   (syllable) relative to the beginning of hangul 
		   buffer. A negative value indicates 'the cursor' is in
		   the status area where input is meaningless. */
    int caret_pixel;
    int has_temp_hangul;
    int win_width;
    int linespace;
    int cn1, cn2, cn3;
    int xoffset;
    CARD32 fg, bg;
    IC *ic; /* owner */
};

struct _IC {
    CARD16	id;		/* ic id */
    CARD16	connect_id;	/* connect id */
    INT32	input_style;	/* input style */
    GdkWindow	*client_win;	/* client window */
    GdkWindow	*focus_win;	/* focus window */
    GdkWindow   *top_win;
    DispWindow	*edit_win;
    DispWindow	*status_win;
    HangulState	*han;
    int		destroyed;
    int		composing_hangul;
    IC	*next;
};

enum {
    HOTKEY_FORWARD,
    HOTKEY_DONE,
    HOTKEY_CONTINUE
};

enum {
    AMI_NORMAL_RUN,  /* run as a standalone program */
    AMI_WM_RUN,      /* run as a WindowMaker Dock applet */
    AMI_GNOME_RUN    /* run as a GNOME panle applet */
} AMI_RUN_MODE;

enum { WM_E, WM_KDE1, WM_KDE2, WM_W, WM_DETECT, WM_UNKNOWN};

enum {
    AMI_EDITING_WORD_MODE=1,
    AMI_EDITING_SIMPLE_WORD_MODE=2,
    AMI_EDITING_CHAR_MODE=3
} AMI_EDITING_MODE;

enum {
    AMI_PIXMAP_ENGLISH,
    AMI_PIXMAP_HANGUL,
    AMI_PIXMAP_NONE
} AMI_DRAWING_PIXMAP;

enum {
		AMI_EUC,
		AMI_UTF8
} AMI_CODESET;

#endif /* __AMITYPE_H */
