#include <gdk/gdkkeysyms.h>
#include <stdlib.h>
#include <X11/Xlib.h>

#include "Ximd/IMdkit.h"

int make_trigger_keys(XIMTriggerKey **key, gchar *str);
char * trigger_keys_to_str(XIMTriggerKey *key);
