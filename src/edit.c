/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "config.h"
#include "edit.h"
#include "ic.h"
#include "ami.h"
#include "dw.h"
#include "cb.h"
#include "util.h"
#include "hinput.h"
#include "comp.h"
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include "utf8.h"

char *
editing_mbstocts(char *s)
{
    XTextProperty tp;

    XmbTextListToTextProperty(gdk_display, &s, 1,
			      XCompoundTextStyle, &tp);
    if (debug) {
	int i;
	fprintf(stderr,"%s: mbs= ", __FUNCTION__);
	for (i=0; i< strlen(s); i++) 
	    fprintf(stderr,"%02x%c", (unsigned char) s[i], i!=strlen(s)-1 ? ' ' : '\n');
	fprintf(stderr,"%s: cts= ", __FUNCTION__);
	for (i=0; i< strlen(tp.value); i++) 
	    fprintf(stderr,"%02x%c", tp.value[i], 
		i!=strlen(tp.value)-1 ? ' ' : '\n');
    }
    return tp.value;
}

int
editing_commit(IC *ic, char *buf)
{
    char *cts;
    IMCommitStruct cs;

    g_return_val_if_fail (ic != NULL, FALSE);
    g_return_val_if_fail (buf != NULL, FALSE);

    cts = editing_mbstocts(buf);
    cs.connect_id = ic->connect_id;
    cs.icid = ic->id;
    cs.flag = XimLookupChars;
    cs.commit_string = cts;
    IMCommitString(xims, (gpointer)&cs);
    XFree(cts);
    if (debug) g_print("ic=%d: commit string=%s\n", ic->id,buf);
    return 1;
}

int
editing_commit_both(IC *ic, KeySym keysym, char *buf)
{
    char *cts;
    IMCommitStruct cs;

    g_return_val_if_fail (ic != NULL, FALSE);
    g_return_val_if_fail (buf != NULL, FALSE);

    cs.connect_id = ic->connect_id;
    cs.icid = ic->id;
    if (*buf) {
	cs.flag = XimLookupBoth;
	cts = editing_mbstocts(buf);
    } else {
	cs.flag = XimLookupKeySym;
	cts = "";
    }
    cs.commit_string = cts;
    IMCommitString(xims, (gpointer)&cs);
    if (*cts) XFree(cts);
    if (debug) g_print("ic=%d: commit-both string=%s\n", ic->id,buf);
    return 1;
}

void
editing_toggle(IC *ic)
{
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);

    ic->han->composing_hangul = !ic->han->composing_hangul;
    ic->composing_hangul = ic->han->composing_hangul;

    editing_flush(ic);
    composer_reset(ic->han->composer);
    if (ic->edit_win) {
	if (ic->han->composing_hangul) {
	    dw_show(ic->edit_win);
	} else {
	    dw_hide(ic->edit_win);
	}
    }
    if (ic->input_style & XIMPreeditCallbacks) {
	cb_edit_toggle(ic);
    }
    editing_status_win_update(ic);
}


/* FIXME */
/* make more sane function name */
int
editing_flush(IC *ic)
{
    char *s;
    g_return_val_if_fail (ic != NULL, 0);
    g_return_val_if_fail (ic->han != NULL, 0);
    s = g_strdup(ic->han->buf);
    if (ic->han->len <= 0) return 0;
    editing_commit(ic, ic->han->buf);
    editing_send_caret_position(ic);
    editing_reset(ic);
    g_free(s);
    return 1;
}

int
editing_flush_without_reset(IC *ic)
{
    g_return_val_if_fail (ic != NULL, 0);
    g_return_val_if_fail (ic->han != NULL, 0);
    if (ic->han->len <= 0) return 0;
    editing_commit(ic, ic->han->buf);
    editing_send_caret_position(ic);

    if (ic->input_style & XIMPreeditCallbacks) {
	cb_edit_reset(ic);
    }
    ic->han->buf[0] = '\0';
    ic->han->pos = ic->han->len = 0;
    if (ic->edit_win) {
	dw_set_text(ic->edit_win, "", 0, 0);
    }
    return 1;
}

int
editing_flush_with_str(IC *ic, char *s)
{
    char * buf;
    int pos;
    int add_len = strlen(s);

    g_return_val_if_fail (ic != NULL, 0);
    g_return_val_if_fail (ic->han != NULL, 0);
    if (ic->han->len == 0) {
	editing_commit(ic, s);
	return 1;
    }

    buf = ic->han->buf;
    pos = ic->han->pos;
    memmove(buf + pos + add_len, buf + pos, ic->han->len - pos + 1);
    memcpy(buf + pos, s, add_len);
    editing_commit(ic, buf);
    ic->han->pos++;
    editing_send_caret_position(ic);
    ic->han->pos--;
    editing_reset(ic);
    return 1;
}

int
editing_reset(IC *ic)
{
    g_return_val_if_fail (ic != NULL, 0);
    g_return_val_if_fail (ic->han != NULL, 0);
    if (ic->han->len <= 0) return 0;
    if (ic->input_style & XIMPreeditCallbacks) {
	cb_edit_reset(ic);
    }
    ic->han->buf[0] = '\0';
    ic->han->pos = ic->han->len = 0;
    composer_reset(ic->han->composer);
    if (ic->edit_win) {
	dw_set_text(ic->edit_win, "", 0, 0);
    }
    return 1;
}

void
editing_correct_insert(IC *ic, guchar *buf)
{

    int pos;
    guchar *hbuf;
    int len1=2;  /* the length of a replacement char. */
    int len2=2;  /* the length of a char. to insert */
    int len0=2;  /* the length of a char. to be replaced */
    int lenchg;  /* net change in the length of ic->han->buf */
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);

    pos = ic->han->pos;

    /* By inspecting the first byte, we can tell how many octets
     * are taken by a UTF-8 character. If it's in [0xe0,0xef],
     * it should be 3-octet-long */
    if ( ami_codeset==AMI_UTF8 && IS_UTF8_3BYTE(buf[0])) 
      len1=3;
    if ( ami_codeset==AMI_UTF8 && IS_UTF8_3BYTE(buf[len1]))
      len2=3;

    buf[len1+len2] = '\0';

    if (ic->input_style & XIMPreeditCallbacks) {
	cb_edit_correct_insert(ic, buf);
    }

    hbuf = ic->han->buf;
    /* if the second byte from the end is still in the range [0x80,0xbf],
     * it should be at least 3-octet-long. It could be 4 octets long,
     * but at the moment we don't care about characters beyond BMP. */
    if ( ami_codeset==AMI_UTF8 && IS_UTF8_TRAIL(hbuf[pos-2]) )
      len0=3;

    lenchg = len2+len1-len0;
    ami_hangul_check_bufsize(ic->han, lenchg);


    if (ic->han->len > pos)
        memmove(hbuf + pos + lenchg, hbuf + pos, ic->han->len - pos);
    memcpy(hbuf + pos - len0, buf, len1 + len2);
    ic->han->pos += lenchg;
    ic->han->len += lenchg;
    hbuf[ic->han->len] = '\0';
    if (ic->edit_win) {
	editing_edit_win_update(ic);
    }

}

int
editing_forward_delete(IC *ic)
{
    int pos;
    guchar *hbuf;
    int howmuch;

    g_return_val_if_fail (ic != NULL, 0);
    g_return_val_if_fail (ic->han != NULL, 0);

    pos = ic->han->pos;
    hbuf = ic->han->buf;
    howmuch = MBC_LEN(hbuf[pos]);

    g_assert(howmuch);

    if (pos + howmuch > ic->han->len) howmuch = ic->han->len - pos;
    if (howmuch <= 0) return FALSE;
    memmove (hbuf + pos, hbuf + pos + howmuch, ic->han->len - pos - howmuch);
    ic->han->len -= howmuch;
    hbuf[ic->han->len] = '\0';
    if (ic->edit_win) {
	editing_edit_win_update(ic);
    } else if (ic->input_style & XIMPreeditCallbacks) {
    }
    return TRUE;
}

int
editing_backward_delete(IC *ic)
{
    int pos;
    guchar *hbuf;
    int howmuch;

    g_return_val_if_fail (ic != NULL, 0);
    g_return_val_if_fail (ic->han != NULL, 0);

    pos = ic->han->pos;
    hbuf = ic->han->buf;

    if (pos <= 0) return FALSE;
    if (ic->input_style & XIMPreeditCallbacks) {
	cb_edit_backward_delete(ic);
    }

    howmuch = MBC_LEN_B(hbuf+pos-1); 
#if 0
    if ( ami_codeset == AMI_EUC) 
        howmuch = hbuf[pos-1] & 0x80 ? 2:1;
    else
				howmuch = ! (hbuf[pos-1] & 0x80) ? 1 : 
	          IS_UTF8_2BYTE(hbuf[pos-2])  ? 2  : 
	          IS_UTF8_3BYTE(hbuf[pos-3])  ? 3  : 0;
#endif
    if (debug) 
      fprintf(stderr,"%s: *(hbuf+pos-3) = %02x %02x %02x\n"
      "pos=%d howmuch=%d ic->han->len=%d\n",
  	  __FUNCTION__,hbuf[pos-3],hbuf[pos-2],hbuf[pos-1],
   	  pos,howmuch,ic->han->len);

    g_assert(howmuch);
    if (pos - howmuch < 0) howmuch = pos;
    memmove(hbuf + pos - howmuch, hbuf + pos, ic->han->len - pos);
    ic->han->len -= howmuch;
    ic->han->pos -= howmuch;
    hbuf[ic->han->len] = '\0';
    if (ic->edit_win) {
	editing_edit_win_update(ic);
    }
    return TRUE;
}

void
editing_insert(IC *ic, guchar *buf, int size)
{
    int pos;
    guchar *hbuf;
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);

    pos = ic->han->pos;

    buf[size] = '\0';
    if (ic->input_style & XIMPreeditCallbacks) {
	/*
	Is this necessary really?
	char buf1[3];
	buf1[0] = buf[0]; buf1[1] = buf[1]; buf1[2] = '\0';
	cb_edit_correct(ic, buf1);
	*/
	cb_edit_insert(ic, buf);
    }

    ami_hangul_check_bufsize(ic->han, size);
    hbuf = ic->han->buf;
    if (ic->han->len > pos)
    	memmove(hbuf + pos + size, hbuf + pos, ic->han->len - pos);
    ic->han->len += size;
    hbuf[ic->han->len] = '\0';
    memcpy(hbuf + pos, buf, size);
    ic->han->pos = pos + size;
    if (ic->edit_win) {
	editing_edit_win_update(ic);
    }
}

void
editing_correct(IC *ic, guchar *buf)
{
    int pos;
    int len=2;

    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);

    pos = ic->han->pos;
    if ( ami_codeset==AMI_UTF8 && IS_UTF8_3BYTE(buf[0]) )
	len=3;

    buf[len] = '\0';

    if (ic->input_style & XIMPreeditCallbacks) {
    	cb_edit_correct(ic, buf);
    }
    memcpy(ic->han->buf+(pos-len),buf,len);
    if (ic->edit_win) {
	editing_edit_win_update(ic);
    }
}

void
editing_hanja_replace(IC *ic, guchar *hanja)
{
    int len = strlen(hanja);
    char *buf = ic->han->buf + ic->han->pos - len;
    char *hangul = g_strndup(buf, len);
    char **hanja_formats = g_strsplit(ami_hanja_subst_format, ":", -1);
    GString *str = g_string_new(NULL);
    char *s = hanja_formats[ami_hanja_subst_mode-1];
    while (*s) {
	if (strncmp(s, "�ѱ�", 4) == 0) {
	    s += 4;
	    g_string_append(str, hangul);
	} else if (strncmp(s, "����", 4) == 0) {
	    s += 4;
	    g_string_append(str, hanja);
	} else {
	    g_string_append_c(str, *s++);
	}
    }
    g_strfreev(hanja_formats);
    if (ic->input_style & XIMPreeditCallbacks) {
	cb_edit_hanja_replace(ic, hangul, str->str);
    }
    ami_hangul_check_bufsize(ic->han, str->len - len);
    buf = ic->han->buf + ic->han->pos - len;
    memmove(buf + str->len, buf + len, strlen(buf + len)+1);
    memmove(buf, str->str, str->len);
    ic->han->len += str->len - len;
    ic->han->pos += str->len - len;
    if (ic->edit_win) {
	editing_edit_win_update(ic);
    }
    g_free(hangul);
    g_string_free(str, TRUE);
    if (debug) g_print("%s\n", buf);
}

/* return TRUE if successful in moving. otherwise, return FALSE */
int
editing_move(IC *ic, int howmuch)
{
    int pos;
    char *buf;
    int len;
    g_return_val_if_fail (ic != NULL, 0);
    g_return_val_if_fail (ic->han != NULL, 0);
    buf = ic->han->buf;
    len = ic->han->len;
    pos = ic->han->pos;
    if (howmuch > 0) {
	while (howmuch--) {
	    if (buf[pos] & 0x80) pos++;
            if ( ami_codeset==AMI_UTF8 && ! ((0xe0 ^ buf[pos]) & 0x10) )
		pos++;
	    if (++pos >= len) {
		pos = len;
		break;
	    }
	}
    } else {
	while(howmuch++) {
	    if (--pos <= 0) {
		pos = 0;
		break;
	    }
	    if (buf[pos] & 0x80) pos--;
	    if ( ami_codeset==AMI_UTF8 && !((0x80 ^ buf[pos]) & 0x40) )
	      pos--;
	}
    }
    if (ic->han->pos == pos) return FALSE;
    ic->han->pos = pos;
    if (ic->edit_win) {
	editing_edit_win_update(ic);
    } else if (ic->input_style & XIMPreeditCallbacks) {
	cb_edit_move(ic);
    }
    return TRUE;
}

void
editing_edit_win_update(IC *ic)
{
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);
    if (!ic->edit_win) return;
    dw_set_text(ic->edit_win, ic->han->buf, ic->han->pos, composer_has_temp_hangul(ic->han->composer));
}

void
editing_status_win_update(IC *ic)
{
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);
    if (!ic->status_win) return;
    g_return_if_fail (ic->han != NULL);
    if (ic->han->composing_hangul) {
	dw_set_text(ic->status_win, hangul_mode_label, -1, 0);
    } else {
	dw_set_text(ic->status_win, english_mode_label, -1, 0);
    }
}

void
editing_clear_temp(IC *ic)
{
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);
    if (!composer_has_temp_hangul(ic->han->composer)) return;
    if (ic->input_style & XIMPreeditCallbacks) cb_edit_clear_temp(ic);
    composer_reset(ic->han->composer);
}

void
editing_send_caret_position(IC *ic)
{
#if 1
    IMCommitStruct cs;
    int howmuch;
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);
    /* XXX: assert(len >= pos) */
    if (ic->han->len <= ic->han->pos) return;
    howmuch = util_get_mb_strlen(ic->han->buf + ic->han->pos, -1);
    if (debug) g_print("%s: %d\n",__FUNCTION__, howmuch);

    cs.connect_id = ic->connect_id;
    cs.icid = ic->id;
    cs.flag = XimLookupKeySym;
    cs.commit_string = "";
    cs.keysym = GDK_Left;
    while (howmuch-- > 0) {
	IMCommitString(xims, (gpointer)&cs);
    }
#else
    IMForwardEventStruct cs;
    int howmuch;
    XKeyEvent *ev;
    g_return_if_fail (ic != NULL);
    g_return_if_fail (ic->han != NULL);
    if (ic->han->len == ic->han->pos) return;
    howmuch = count_char_num(ic->han->buf + ic->han->pos);
    cs.connect_id = ic->connect_id;
    cs.icid = ic->id;
    cs.event.xany.serial = 0;
    ev = (XKeyEvent *)&cs.event;
    ev->type = KeyPress;
    ev->state = 0;
    ev->time = 0;
    ev->keycode = XKeysymToKeycode(gdk_display, GDK_Left);
    while (howmuch-- > 0) {
	IMForwardEvent(xims, (gpointer)&cs);
    }
#endif
}
