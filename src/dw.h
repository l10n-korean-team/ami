#ifndef __DW_H
#define __DW_H
#include "amitype.h"

DispWindow * dw_new(IC *ic);
void dw_destroy(DispWindow *dw);
void dw_set_area(DispWindow *dw, XRectangle *area);
void dw_get_area(DispWindow *dw, XRectangle *area);
void dw_set_area_needed(DispWindow *dw, XRectangle *area);
void dw_get_area_needed(DispWindow *dw, XRectangle *area);
void dw_set_text(DispWindow *dw, char *text, int caret, int composing);
void dw_set_pos(DispWindow *dw, XPoint *p);
void dw_get_pos(DispWindow *dw, XPoint *p);
void dw_set_cursor_pos(DispWindow *dw, int pos);
void dw_draw(DispWindow *dw);
void dw_update_width(DispWindow *dw);
void dw_set_bg(DispWindow *dw, CARD32 bg);
void dw_set_fg(DispWindow *dw, CARD32 fg);
void dw_set_font(DispWindow *dw, char *font);
void dw_set_parent(DispWindow *dw, GdkWindow *parent);
void dw_hide(DispWindow *dw);
void dw_show(DispWindow *dw);
void dw_set_focus(DispWindow *dw);
void dw_unset_focus(DispWindow *dw);
void dw_set_cmap(DispWindow *dw, Colormap *cmap);
void dw_set_cursor(DispWindow *dw, Cursor *cmap);
void dw_set_bg_pixmap(DispWindow *dw, Pixmap *pixmap);
#endif
