/* this file is a part of Ami software, (C) Hwang chi-deok 1999,2000,2001 */
/* This file includes a couple of arrays initialized with 
 * character strings in EUC-KR. If you don't have an editor that can
 * deal with EUC-KR, you have to make sure that your editor does not
 * haphazardly trashes the content.
 */

#include "config.h"

#include <stdio.h>
#include <ctype.h>
#include <sys/stat.h>
#include <unistd.h>
#include "ami.h"
#include "conf.h"
#include "keyparse.h"

#ifndef HANGUL_KEYBOARD
#define HANGUL_KEYBOARD "2bul"
#endif

static char * config_readline(FILE *fp);
static gboolean config_add_item(guchar *desc, guchar *key, guchar *val_string);
static int save_default_config_file(void);
static ConfigItem *ami_config = NULL;


static ConfigDesc ami_config_type[] =
{
    {"자판", STRING, &keyboard_name},
    {"입력 방식", INT, &ami_editing_mode},
    {"기본 글꼴", STRING, &default_fontname},
    {"한영 전환키", KEYSYM, &trigger_keys},
    {"한자 변환키", KEYSYM, &hanja_keys},
    {"특수문자 입력키", KEYSYM, &special_char_keys},
    {"한글상태 라벨", STRING, &hangul_mode_label},
    {"영문상태 라벨", STRING, &english_mode_label},
    {"단일 한영상태", BOOLEAN, &unique_han},
    {"flush key", KEYSYM, &flush_keys},
    {"last line scroll", BOOLEAN, &ami_line_wraping_mode},
    {"escape hangul toggle", BOOLEAN, &esc_han_toggle},
    {"상태창 만들기", BOOLEAN, &support_status},
    {"한자 치환 형태", STRING, &ami_hanja_subst_format},
    {"한자 치환 방식", INT, &ami_hanja_def_subst_mode},
    {"밑줄을 사용", BOOLEAN, &ami_use_underline},
    {"한자사전", STRING, &hanja_dic_filename},
    {"벨소리 크기", INT, &bell_intensity},
    {"그림 파일 위치", STRING, &pix_dir_name},
    {"초/중성의 입력 순서 뒤바뀜 허용", BOOLEAN, &allow_shuffle},
    {"글꼴 수(UTF-8 locale 글꼴 모음)",INT,&num_fonts_in_utf8_fs},
    {NULL, 0, NULL}
};

gboolean
ami_config_init(void)
{
    gboolean ret;
    int i;
    char *conf_file = g_strconcat(g_get_home_dir(), "/.ami/ami.conf", NULL);
    if (access(conf_file, R_OK) != 0) {
	if (!save_default_config_file())
	    g_error(_("Cannot write to %s\n"), conf_file);
    }
    ret = ami_config_load_file(conf_file, ami_config_type);
    on_keys = g_new(XIMTriggerKeys, 1);
    if (trigger_keys == NULL) {
	g_error(_("No key is specified for Korean-English KBD toggle."));
    }
    for(i=0;trigger_keys[i].keysym;i++);
    on_keys->count_keys = i;
    on_keys->keylist = trigger_keys;
    g_free(conf_file);
    ami_hanja_subst_mode = ami_hanja_def_subst_mode;
    if (!hangul_mode_label)
	hangul_mode_label = g_strdup(_(" [KO] "));
    if (!english_mode_label)
	english_mode_label = g_strdup(_(" [EN] "));
    if (!ami_hanja_subst_format)
	ami_hanja_subst_format = g_strdup("漢字:漢字(한글):한글(漢字)");
    if (!hanja_dic_filename) hanja_dic_filename = g_strdup(HANJA_DIC);
    if (!keyboard_name) keyboard_name = g_strdup(HANGUL_KEYBOARD);
    composer_set_shuffle_active((int) allow_shuffle);
    return ret;
}

static guchar *
line_skip_space(guchar *s)
{
    while(isspace(*s)) s++;
    return s;
}

static guchar *
line_rskip_space(guchar *s)
{
    while(isspace(*s)) s--;
    return s;
}

static gboolean
line_is_empty(guchar *s)
{
    while(*s) {
	if (!isspace(*s)) return FALSE;
	s++;
    }
    return TRUE;
}

static int
save_default_config_file(void)
{
    char *conf_dir = g_strdup_printf("%s/%s", g_get_home_dir(), ".ami");
    char *conf;
    extern const char *default_config;
    struct stat statbuf;
    FILE *fp;
    if (stat(conf_dir, &statbuf) != 0) {
	mkdir(conf_dir,0750);
    }
    conf = g_strdup_printf("%s/%s", conf_dir, "ami.conf");
    g_free(conf_dir);
    fp = fopen(conf, "w");
    if (fp == NULL) {
	g_error(_("Cannot save Ami configuration options to %s."), conf);
    }
    g_free(conf);
    fputs(default_config, fp);
    fclose(fp);
    return TRUE;
}

static char *
config_readline(FILE *fp)
{
    char buf[1024];
    if (fgets(buf, sizeof(buf), fp)) {
	return g_strdup(buf);
    }
    return NULL;
}

static ConfigItem *
config_find_config_item(guchar *key)
{
    ConfigItem *config = ami_config;
    while (config->key) {
	if (strcmp(config->key, key) == 0) return config;
	config++;
    }
    return NULL;
}

static gboolean
config_add_item(guchar *desc, guchar *key, guchar *val_string)
{
    ConfigItem *config_item;
    config_item = config_find_config_item(key);
    if (config_item == NULL) return FALSE;
    config_item->desc = g_strdup(desc);
    switch(config_item->type) {
	case INT:
	    *config_item->val.integer = atoi(val_string);
	    break;
	case BOOLEAN:
	    if (*val_string == 't'
	    	|| strcmp(val_string, "on") == 0
		|| *val_string == '0') *config_item->val.boolean = TRUE;
	    else if(*val_string == 'f'
	    	|| strcmp(val_string, "off") == 0
		|| *val_string == '1') *config_item->val.boolean = FALSE;
	    else {
		g_warning("%s: valid value is true or false", key);
	    }
	    break;
	case KEYSYM:
	    make_trigger_keys(config_item->val.keys, val_string);
	    break;
	case STRING:
	    if (*config_item->val.string) g_free(*config_item->val.string);
	    if (val_string[0] == '"' &&
	        val_string[strlen(val_string)-1] == '"') {
		val_string[strlen(val_string)-1] = 0;
		val_string++;
	    }
	    *config_item->val.string = g_strdup(val_string);
	    break;
    }
    return TRUE;
}

gboolean
ami_config_load_file(char *file, ConfigDesc *ami_config_type)
{
    int config_size;
    int i;
    GSList *list, *comment = NULL;
    FILE *fp;
    char *line;

    for(i=0;ami_config_type[i].key;i++);
    config_size = i;
    ami_config = g_new0(ConfigItem, config_size + 1);
    for(i=0;i<config_size;i++) {
	ami_config[i].key = ami_config_type[i].key;
	ami_config[i].type = ami_config_type[i].type;
	switch(ami_config[i].type) {
	    case STRING:
		ami_config[i].val.string = ami_config_type[i].val_pointer;
		break;
	    case INT:
		ami_config[i].val.integer = ami_config_type[i].val_pointer;
		break;
	    case KEYSYM:
		ami_config[i].val.keys = ami_config_type[i].val_pointer;
		break;
	    case BOOLEAN:
		ami_config[i].val.boolean = ami_config_type[i].val_pointer;
		break;
	}
    }
    fp = fopen(file, "r");
    if (fp == NULL) return FALSE;
    while ((line = config_readline(fp))) {
	if (line[0] == '#' || line_is_empty(line)) {
	    comment = g_slist_append(comment, line);
	} else {
	    char *sep = strchr(line, '=');
	    guchar * start, *end, *key, *val;
	    GString *desc_string;
	    if (sep == NULL) {
		g_warning(_("invalid config line: %s"), line);
		continue;
	    }
	    *sep = 0;
	    start = line_skip_space(line);
	    end = line_rskip_space(sep - 1);
	    if (isspace(*end)) *end = 0;
	    else *(end+1) = 0;
	    key = start;
	    sep += 1;
	    start = line_skip_space(sep);
	    end = line_rskip_space(sep + strlen(sep) - 1);
	    if (isspace(*end)) *end = 0;
	    else *(end+1) = 0;
	    val = start;
	    desc_string = g_string_new("");
	    list = comment;
	    while(list) {
		g_string_append(desc_string , list->data);
		g_free(list->data);
		list = list->next;
	    }
	    g_slist_free(comment);
	    comment = NULL;
	    if (!config_add_item(desc_string->str, key, val)) {
		g_warning(_("%s is not a valid key"), key);
	    };
	    g_string_free(desc_string, TRUE);
	    g_free(line);
	}
    }
    fclose(fp);
    return TRUE;
}

gboolean
ami_config_save_file(char *file)
{
    FILE *fp;
    ConfigItem *config;
    char *key_desc;
    if (file) {
	fp = fopen(file, "w");
    } else {
	file =g_strconcat(g_get_home_dir(), "/.ami/ami.conf", NULL);
	fp = fopen(file, "w");
	g_free(file);
    }
    if (fp == NULL) return FALSE;
    config = ami_config;
    while(config->key) {
	if (config->desc) fprintf (fp, "%s", config->desc);
	else fprintf (fp, "# %s\n", config->key);
	switch(config->type) {
	    case INT:
		fprintf (fp, "%s=%d\n", config->key, *config->val.integer);
		break;
	    case BOOLEAN:
		if(*config->val.boolean)
		    fprintf (fp, "%s=true\n", config->key);
		else
		    fprintf (fp, "%s=false\n", config->key);
		break;
	    case STRING:
		{
		    char *s = *config->val.string;
		    if (!s || !s[0]) s = "";
		    fprintf (fp, "%s=\"%s\"\n", config->key, s);
		}
		break;
	    case KEYSYM:
		key_desc = trigger_keys_to_str(*config->val.keys);
		fprintf (fp, "%s=%s\n", config->key, key_desc);
		g_free(key_desc);
		break;
	}
	config++;
    }
    fprintf(fp, "\n");
    fclose(fp);
    return TRUE;
}

/*
   '\n\' has to be used to make xgettext not complain about unterminated
  string literals. J. Shin
*/
const char *default_config = 
"\n\
# Caution: This file MUST be in EUC-KR encoding. If you use UTF-8 locale,\n\
# convert this file to UTF-8 before editing and convert back to EUC-KR\n\
# after you're done with editing. Otherwise, Ami wouldn't be able to\n\
# parse the file to get the values of configuration options.\n\
# 아미의 설정 파일\n\
# 주의: 설정 키를 절대 바꾸지 마십시오. 즉 \"입력 방식\"이라고 되어있으면\n\
# 그 띄어쓰기까지를 포함해서 정확한 이름이어야 합니다.\n\
\n\
# 자판\n\
# 2bul, 3bul_390, 3bul_final, dvorak_2bul, dvorak_3bul_390,\n\
# dvorak_3bul_final이 Ami 배포판에 들어 있는 자판 종류입니다.\n\
# 추가로 다른 자판을 정의해서 쓸 수도 있습니다.\n\
자판=" HANGUL_KEYBOARD "\n\
\n\
# 입력시 글을 모아서 보내는 방식을 정해줍니다.\n\
# 1=단어 단위로 편집하며 입력 도중 화살표 키를 이용한 이동을 Ami가 직접\n\
#   처리합니다.\n\
# 2=단어 단위로 편집하며 화살표 키를 이용한 이동은 클라이언트 프로그램으로 \n\
#   하여금  처리를 하도록 합니다. 권장하는 모드입니다.\n\
# 3=글자 단위로 편집합니다.\n\
\n\
입력 방식 = 2\n\
\n\
# 모든 클라이언트 프로그램이 동일한 한영 상태를 유지할 것인지 결정\n\
# false: 한영 상태를 창 별로 유지합니다. XIM 표준에 가장 부합하는 방식입니다.\n\
#  htt와 같은 입력기와는 달리 클라이언트에서 주는 정보에만 의존하지 않고\n\
#  직접 창을 조사해서 gtk+와 같은 프로그램에서도 동일한 동작을 하도록\n\
#  합니다.\n\
# true: 모든 클라이언트 프로그램이 같은 한영 상태를 같습니다. 상태창을 \n\
# 만들지 않습니다.  사람에 따라 다르지만 저는 이 방식이 더 편합니다.\n\
\n\
단일 한영상태 = false\n\
\n\
\n\
# 한영 전환을 위해 사용되는 키를 정해줍니다\n\
\n\
한영 전환키 = Shift<Key>space, <Key>Hangul\n\
\n\
# 한자 입력 키\n\
\n\
한자 변환키 = <Key>F9, <Key>Hangul_Hanja\n\
\n\
# 특수 문자 입력키 \n\
\n\
특수문자 입력키 = <Key>F3\n\
\n\
# 현재 입력 중인 입력창의 내용을 프로그램으로 곧장 보내는 키\n\
\n\
flush key = Ctrl<Key>Return\n\
\n\
# 편집창이 화면 맨 아랫 부분에 나타나는 경우(또는 gtk entry와 같은 한 줄짜리 \n\
# 입력창)에 입력창의 맨 오른쪽까지 한글 입력이 다 되었을때 그 다음 입력이\n\
# 화면의 맨 윗 부분에서 이루어질 것인지 아니면 현재 입력되고 있는 것을 \n\
# 오른쪽으로 스크롤 시킬 것인지  정합니다.\n\
# true이면 그냥 스크롤 시킵니다.\n\
\n\
last line scroll = true\n\
\n\
# 기본 글꼴 \n\
# 아미에 접속하는 프로그램에서 글꼴을 지정해 주지 않거나 잘못된 이름인 경우에\n\
# 쓰일 글꼴을 적어주시면 됩니다. 형식은 영문 글꼴,한글 글꼴\n\
# Ami를 UTF-8 locale에서 돌린다면 아래의 값을 쓰는 것이 좋은 생각입니다.\n\
# 그렇지 않고 EUC-KR locale에서 돌린다면  아래의 값을 써도 상관 없고, \n\
# \"*,*\"을 지정해 주어도 좋습니다.\n\
\n\
기본 글꼴 = -*-*-medium-r-normal-ko-16-*-*-*-*-*-iso10646-1,-*-*-medium-r-normal--16-*-*-*-*-*-*-*\n\
\n\
# 한글을 모으는 도중일 때 상태창에 표시되는 문자열을 지정해줍니다.\n\
\n\
한글상태 라벨 = \" [KO] \"\n\
\n\
# 영문 입력 상태일 때 상태창에 표시되는 문자열을 지정해 줍니다.\n\
\n\
영문상태 라벨 = \" [EN] \"\n\
\n\
# 한글 입력 상태에서 Escape가 눌려졌을 때 영문 상태로 자동적으로 갈 것인지\n\
# 정해줍니다. true/false\n\
\n\
escape hangul toggle = true\n\
\n\
# 상태창을 만들 것인지 결정해 줍니다.\n\
# 창 아랫쪽에 보통 생기는 상태창이 거추장스러운 분들은 이 옵션을 \n\
# false로 하십시요. \n\
# 단일 한글 상태를 선택하시면 이 옵션과 상관 없이 상태창을 만들지 않습니다.\n\
\n\
상태창 만들기 = true\n\
\n\
# 한자를 치환하는 형태를 정해줍니다.\n\
# \"漢字\"라고 되어있는 부분은 실제 선택한 한자로 치환되고 \"한글\"이라고\n\
# 쓰인 부분은 그 한자에 해당하는 한글이 넣어지게 됩니다.\n\
# 형태들 사이의 구분은 \":\"로 합니다.\n\
\n\
한자 치환 형태 = 漢字:漢字(한글):한글(漢字)\n\
\n\
# 한자 치환 형태에서 몇 번째 방식을 기본으로 할 것인지\n\
# 선택합니다.\n\
# 1부터 시작\n\
\n\
한자 치환 방식 = 1\n\
\n\
# 현재 입력창에서 모으고 있는 한글을 표시할 때 아랫쪽에 밑줄을 긋을 것인지\n\
# 아니면 색을 기존 창과 조금 다르게 쓸 것인지 결정\n\
\n\
밑줄을 사용 = true\n\
\n\
# 한자 사전의 위치를 설정\n\
\n\
한자사전 = " HANJA_DIC "\n\
\n\
# 삑 소리의 크기를 정해줍니다.\n\
# 유효한 범위는 -100에서 100까지이고 값이 클수록 소리가 커집니다.\n\
벨소리 크기 = 0\n\
\n\
# 한영 표시를 위한 그림 파일이 위치하는 디렉토리를 지정합니다.\n\
# \"\"로 되어있을 경우는 아미에 내장된 그림을 사용합니다.\n\
# 디렉토리이름은 '/'로 시작하는 디렉토리 이름을 전부 써 주셔야 합니다.\n\
그림 파일 위치 = \"\"\n\
\n\
# 초성 없이 중성 입력 후 초성이 들어오는 경우 타이핑의 순서가 바뀐 것으로 \n\
# 간주하고 초성+중성의 순의 입력인 것으로 간주\n\
초/중성의 입력 순서 뒤바뀜 허용 = false\n\
\n\
# UTF-8 locale을 쓸 경우 글꼴 모음(fontset)에서 필요로 하는\n\
# 글꼴 수를 지정합니다. $XROOT/locale/ko_KR.UTF-8/XLC_LOCALE에\n\
# 지정된 글꼴 수를 여기에 쓰면 됩니다. XFree86 4.x에 있는\n\
# en_US.UTF-8/XLC_LOCALE에는 7개의 글꼴이 지정되어 있습니다.\n\
# 반면에 X11R6.6나 Solaris 8 혹은 9의 ko_KR.UTF-8/XLC_LOCALE에는 \n\
#  18개의 글꼴이 나열되어 있습니다. \n\
\n\
글꼴 수(UTF-8 locale 글꼴 모음)= 7\n\
\n\
";
