/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "ami.h"
#include "config.h"
#include "dw.h"
#include "ami.h"
#include "ic.h"
#include "util.h"
#include "cli.h"
#include "utf8.h"

/* If a client does not inform us of the window size, we need to 
 * figure it out on our own. Sometimes the first attempt gives
 * us an inappropriate value (smaller than MIN_WIDTH) when it's made
 * before the window of a client is fully formed. In that case,
 * we have to give it another try after some delay. Netscape (4.x)
 * makes a 5 x 5 window at first and then expands(?) it to an
 * appropriate size. So does gtk with the  initial size of 1 x 1. */

#define MIN_WIDTH 10

static GdkFilterReturn dw_event_filter(XEvent *xev, GdkEvent *ev, DispWindow *dw);
static GdkFilterReturn dw_event_filter_extra(XEvent *xev, GdkEvent *ev, DispWindow *dw);
static int dw_idle_win_show(DispWindow *dw);
static void dw_extra_destroy(DispWindow *dw);
static void dw_extra_update_position(DispWindow *dw);
static void dw_draw_extra(DispWindow *dw);
static GdkWindow * dw_extra_win_new(DispWindow *dw, int y, int w);

CARD32 forced_bg = 0;

DispWindow *
dw_new(IC *ic)
{
    DispWindow *dw;

    dw = g_new0(DispWindow, 1);

    dw->fg = BlackPixel(gdk_display, gdk_screen);
    dw->bg = WhitePixel(gdk_display, gdk_screen);

    dw->font = default_xim_font;
    dw->fontname = g_strdup("");
    gdk_font_ref(default_xim_font);
    dw->linespace = -1;
    dw->text = g_strdup("");
    dw->area.x = -1;
    dw->ic = ic;
    if ((unique_han || ic->input_style == (XIMStatusNone|XIMPreeditNone))&& ic->top_win == NULL) ic->top_win = GDK_ROOT_PARENT();
    return dw;
}

int
dw_status_timeout_show(gpointer data)
{
    IC *ic;
    IC *status_ic;
    CARD16 id;
    id = GPOINTER_TO_INT(data);
    ic = ic_find_with_id(id);
    if (!ic) return 0;
    if (!ic->status_win) return 0;
    status_ic = cli_get_status_ic(ic);
    if (status_ic == NULL || status_ic == ic) dw_show(ic->status_win);

    return 0;
}

static void
report_parent(GdkWindow *win)
{
    gint x, y, width, height, depth;
    gdk_window_get_geometry (win,&x,&y,&width,&height,&depth);
}

void
dw_set_parent(DispWindow *dw, GdkWindow *parent)
{
    int mask = GDK_WA_X | GDK_WA_Y;
    GdkWindowAttr attr;
    g_return_if_fail (dw != NULL);
    g_return_if_fail (parent != NULL);

#if 0
    g_print("ic=%d parent info: ", dw->ic->id);
    report_parent(parent);
#endif
    if ((dw->ic->input_style & XIMStatusArea) && dw->ic->top_win == NULL) {
	dw->ic->top_win = dw->ic->client_win;
    }

    if (dw->win) {
	XWindowAttributes xwa;
	XSetWindowAttributes set_xwa;
	Window xwin;
	if (gdk_window_get_parent(dw->win) == parent) {
	    return;
	}
	if (!dw->ic->top_win && dw->ic->focus_win &&
	    dw->ic->focus_win != dw->ic->client_win) {
	    dw->ic->top_win = dw->ic->client_win;
	}
	gdk_window_reparent(dw->win, parent, dw->x, dw->y - dw->font->ascent);
	/* if area was calculated before, reevaluate it ???? */
	if (dw->area.x < 0) dw->area.width = 1;
	dw_update_width(dw);
	return;
    }
    attr.event_mask = GDK_EXPOSURE_MASK;
    attr.wclass = GDK_INPUT_OUTPUT;
    attr.window_type = GDK_WINDOW_CHILD;
    attr.width = 1;
    attr.height = 1;
    if (dw->caret >= 0) {
	if (dw->x < dw->area.x) dw->x = dw->area.x;
	if (dw->y - dw->font->ascent < dw->area.y) dw->y = dw->area.y + dw->font->ascent;
	attr.x = dw->x;
	attr.y = dw->y - dw->font->ascent;
    } else {
	attr.x = 0;
	attr.y = 0;
    }
    dw->win = gdk_window_new(parent, &attr, mask);
    g_return_if_fail (dw->win != NULL);
    gdk_window_add_filter (dw->win, (GdkFilterFunc)dw_event_filter, dw);

    dw->gc = gdk_gc_new(dw->win);
    if (dw->caret >= 0) dw->reverse_gc = gdk_gc_new(dw->win);

    gdk_gc_set_stipple(dw->gc, dim_mask);
    if(dw->reverse_gc) gdk_gc_set_stipple(dw->reverse_gc, dim_mask);
    gdk_gc_set_fill(dw->gc, GDK_STIPPLED);
    if(dw->reverse_gc) gdk_gc_set_fill(dw->reverse_gc, GDK_STIPPLED);

    dw_set_fg(dw, dw->fg);
    dw_set_bg(dw, dw->bg);
    dw_update_width(dw);
    if (!dw->ic->top_win && dw->ic->focus_win &&
    	dw->ic->focus_win != dw->ic->client_win) {
	dw->ic->top_win = dw->ic->client_win;
    }
    if (dw->caret >= 0 && dw->ic->top_win && !cli_get_edit_ic(dw->ic)) {
	cli_set_edit_ic(dw->ic);
	dw_hide(dw);
	if (dw->ic->status_win) {
	    cli_set_status_ic(dw->ic);
	    dw_unset_focus(dw->ic->status_win);
	    gtk_timeout_add(1000, dw_status_timeout_show, GINT_TO_POINTER((int)dw->ic->id));
	}
    } else if (dw->caret < 0 && !cli_get_status_ic(dw->ic)) {
	if (dw->ic->han) {
	    cli_set_status_ic(dw->ic);
	    dw_unset_focus(dw);
	}
    }
#if 0
    if (!dw->idle) dw->idle = gtk_idle_add((GtkFunction)dw_idle_win_show, dw);
#endif
}

static int
string_width(DispWindow *dw, char *str)
{
    return gdk_string_width(dw->font, str);
}

static int
dw_seg_width(DispWindow *dw, int start, int num_char)
{
    return gdk_text_width(dw->font, dw->text + start, num_char);
}


static int
text_width(DispWindow *dw, char *str, int len)
{
    return gdk_text_width(dw->font, str, len);
}

static int
get_fit_cnum(DispWindow *dw, char *text, int pwidth)
{
    int count = 0;
    int mul=MBC_LEN(*text);

    while (*text) {
	pwidth -= gdk_text_width(dw->font, text, mul);
	if (pwidth < 0) break;
	text += mul;
	count += mul;
    }
    return count;
}

static int
dw_get_status_needed_width(DispWindow *dw)
{
    int w1 = string_width(dw, hangul_mode_label);
    int w2 = string_width(dw, english_mode_label);
    return w1>w2?w1:w2;
}

int
dw_guess_area(DispWindow *dw)
{
    XWindowAttributes attrs;
    GdkWindow *parent = gdk_window_get_parent (dw->win);
    gdk_error_trap_push ();
    XGetWindowAttributes (gdk_display, GDK_WINDOW_XWINDOW(parent), &attrs);
    if (gdk_error_trap_pop ()) {
	return 0;
    }
    dw->area.x = -1;
    if (dw->caret < 0) {
	dw->area.width = dw_get_status_needed_width(dw);
	dw->area.height = dw->font->ascent + dw->font->descent;
	dw->area.y = attrs.height - dw->font->ascent - dw->font->descent;
    } else {
	dw->area.y = 0;
	dw->area.width = attrs.width;
	dw->area.height = attrs.height;
    }
    return 1;
}

#if 0
GdkFilterReturn
wait_parent_map(XEvent *xev, GdkEvent *ev, DispWindow *dw)
{
    if (xev->xany.type == ConfigureNotify && dw->area.x < 0) {
	dw_guess_area(dw);
	dw_update_width(dw);
	if (dw->caret >= 0 && !cli_get_edit_ic(dw->ic)) {
	    cli_set_edit_ic(dw->ic);
	} else if (dw->caret < 0 && !cli_get_status_ic(dw->ic)) {
	    cli_set_status_ic(dw->ic);
	}
	//gdk_window_remove_filter(parent, (GdkFilterFunc)wait_parent_map, dw);
    }
    gdk_window_unref(dw->win);
    return GDK_FILTER_REMOVE;
}

static int
dw_idle_win_show(DispWindow *dw)
{
    GdkWindow *parent;

    parent = gdk_window_get_parent(dw->win);
    dw->idle = 0;
    if (dw->area.x < 0) dw_guess_area(dw);
    if (dw->area.width >= 5 && dw->area.height > 3) {
	if (dw->caret >= 0 && !cli_get_edit_ic(dw->ic)) {
	    cli_set_edit_ic(dw->ic);
	    dw_hide(dw);
	} else if (dw->caret < 0 && !cli_get_status_ic(dw->ic)) {
	    gdk_window_move_resize(dw->win, dw->area.x<0?0:dw->area.x, dw->area.y, dw->area.width, dw->area.height);
	    cli_set_status_ic(dw->ic);
	    dw_unset_focus(dw);
	}
	return 0;
    }
    gdk_window_set_events(parent, 0);
    gdk_window_add_filter(parent, (GdkFilterFunc)wait_parent_map, dw);
    return 0;
}
#endif

static GdkFilterReturn
dw_event_filter(XEvent *xev, GdkEvent *ev, DispWindow *dw)
{
    if (xev->xany.type == Expose || xev->xany.type == MapNotify) {
	dw_draw(dw);
	return GDK_FILTER_REMOVE;
    }
    if (xev->xany.type == DestroyNotify) {
	ic_destroy(dw->ic);
	return GDK_FILTER_REMOVE;
    }
    return GDK_FILTER_CONTINUE;
}

static GdkFilterReturn
dw_event_filter_extra(XEvent *xev, GdkEvent *ev, DispWindow *dw)
{
    if (xev->xany.type == Expose || xev->xany.type == MapNotify) {
	dw_draw(dw);
	return GDK_FILTER_REMOVE;
    }
    if (xev->xany.type == DestroyNotify) {
	return GDK_FILTER_REMOVE;
    }
    return GDK_FILTER_REMOVE;
}


void
dw_set_area(DispWindow *dw, XRectangle *area)
{
    g_return_if_fail (dw != NULL);
    if (debug) g_print("%s: ic=%d x=%d, y=%d, w=%d, h=%d\n",__FUNCTION__,dw->ic->id, area->x,area->y,area->width,area->height);
    dw->area = *area;
    if (dw->win == NULL) return;
    dw_update_width(dw);
    if (dw->caret >= 0) {
	if (dw->ic->input_style & XIMPreeditArea) {
	    gdk_window_move_resize(dw->win, dw->area.x, dw->area.y, dw->area.width, dw->area.height);
	} else {
	    //gdk_window_move(dw->win, dw->x, dw->y - dw->font->ascent);
	}
    } else {
	//gdk_window_move_resize(dw->win, area->x, area->y, area->width, area->height);
    }
}

void
dw_set_area_needed(DispWindow *dw, XRectangle *area)
{
    dw->area_needed = *area;
}

void
dw_get_area_needed(DispWindow *dw, XRectangle *area)
{
    if (dw->area_needed.width == 0) {
	if (dw->caret < 0) {
	    dw->area_needed.width = dw_get_status_needed_width(dw);
	} else {
	    dw->area_needed.width = string_width(dw, 
		ami_codeset==AMI_EUC ? "\xb0\xb1" : "\xea\xb0\x80" );
	}
    }
    if (dw->area_needed.height == 0) {
	dw->area_needed.height = dw->font->ascent + dw->font->descent;
    }
    *area = dw->area_needed;
}

void
dw_get_area(DispWindow *dw, XRectangle *rect)
{
    g_return_if_fail (dw != NULL);
    *rect = dw->area;
}

void
dw_set_text(DispWindow *dw, char *text, int caret, int has_temp_hangul)
{
    int len;
    g_return_if_fail (dw != NULL);
    g_free(dw->text);
    dw->text = g_strdup(text);
    len = strlen(text);
    dw->caret = caret;
    if (caret > len) {
	dw->caret = len;
    }
    dw->has_temp_hangul = has_temp_hangul;
    dw_update_width(dw);
}

static int
dont_move_to_next_line (DispWindow *dw)
{
    int y;

    if (ami_line_wraping_mode) {
	y = dw->y;
	if (dw->linespace > 0) y += dw->linespace;
	else y += dw->font->ascent + dw->font->descent;
	if (y > dw->area.height - dw->font->descent)
	    return 1;
	else return 0;
    }
    else return 0;
}

void
dw_set_pos(DispWindow *dw, XPoint *p)
{
    g_return_if_fail (dw != NULL);
    g_return_if_fail (p != NULL);
    if (dw->x == p->x && dw->y == p->y) return;
    dw->x = p->x;
    dw->y = p->y;
    if (debug>2)
	g_print("%s: ic=%d x=%d y=%d\n",__FUNCTION__,dw->ic->id, p->x,p->y);
    if (!dw->win) return;
    /* 0xb0 0xa1 : 'GA' in EUC-KR 0xea 0x b0 0x80 : 'GA' in UTF-8 */
    if (dw->x + string_width(dw, ami_codeset==AMI_EUC ? "\xb0\xa1" : 
	  "\xea\xb0\x80") > dw->area.width) {
	if (!dont_move_to_next_line(dw)) {
	    dw->x = 0;
	    if (dw->linespace > 0) dw->y += dw->linespace;
	    else dw->y += dw->font->ascent + dw->font->descent;
	    if (dw->y > dw->area.height - dw->font->descent) {
		    /* y is too low; jump up */
		    dw->y  = dw->font->ascent;

	    }
	}
    }
    gdk_window_move(dw->win, dw->x, dw->y - dw->font->ascent);
    dw_update_width(dw);
}

void
dw_get_pos(DispWindow *dw, XPoint *p)
{
    p->x = dw->x;
    p->y = dw->y;
}

void
dw_destroy(DispWindow *dw)
{
    g_return_if_fail (dw != NULL);
    if (dw->win) {
	gdk_window_remove_filter (dw->win, (GdkFilterFunc)dw_event_filter, dw);
	gdk_error_trap_push ();
	gdk_window_destroy(dw->win);
	gdk_flush();
	gdk_error_trap_pop ();
#if 0
	if (dw->idle) gtk_idle_remove(dw->idle);
#endif
	if (dw->win2) {
	    gdk_window_remove_filter (dw->win2, (GdkFilterFunc)dw_event_filter_extra, dw);
	    gdk_window_destroy(dw->win2);
	}
	if (dw->win3) {
	    gdk_window_remove_filter (dw->win3, (GdkFilterFunc)dw_event_filter_extra, dw);
	    gdk_window_destroy(dw->win3);
	}
    }
    if (dw->gc) gdk_gc_destroy(dw->gc);
    if (dw->reverse_gc) gdk_gc_destroy(dw->reverse_gc);
    if (dw->font) util_font_unref(dw->font);
    if (dw->text) g_free(dw->text);
    if (dw->fontname) g_free(dw->fontname);
    g_free(dw);
}

void
dw_set_font(DispWindow *dw, char *font)
{
    XPoint p;
    g_return_if_fail (dw != NULL);
    g_return_if_fail (font != NULL);

    /* Qt 2.?.x keeps sending the font info. whenever it gets focused. Silly !*/
    if (strcmp(dw->fontname, font) == 0) return;

    util_font_unref(dw->font);
    dw->font = util_fontset_load(font);
    g_free(dw->fontname);
    dw->fontname = g_strdup(font);
    if(debug) g_print("%s: fontname = %s ic=%d\n", __FUNCTION__, font, dw->ic->id);
    /* If the size was determined by us, obtain it again for a new font */
    if (dw->area.x < 0) dw->area.width = 0;
    p.x = dw->x;
    p.y = dw->y;
    dw->x = -1;
    dw_set_pos(dw, &p);
}

char *
dw_get_font(DispWindow *dw)
{
    return dw->fontname;
}

static void
dw_real_set_bg(DispWindow *dw)
{
    unsigned long bg;

    if (!dw->win) return;

    if (ami_use_underline || dw->caret < 0) {
	bg = dw->bg;
    } else {
	/* Mix fg color and bg color with the ratio of 2 : 8 */
	XColor cfg;
	XColor cbg;
	GdkColor newbg;
	GdkColormap *cmap;
	cmap = gdk_window_get_colormap(dw->win);
	cfg.pixel = dw->fg;
	cbg.pixel = dw->bg;
	XQueryColor(gdk_display, GDK_COLORMAP_XCOLORMAP(cmap), &cfg);
	XQueryColor(gdk_display, GDK_COLORMAP_XCOLORMAP(cmap), &cbg);
	newbg.red = cfg.red*0.2 + cbg.red * 0.8;
	newbg.green = cfg.green*0.2 + cbg.green * 0.8;
	newbg.blue = cfg.blue*0.2 + cbg.blue * 0.8;
	gdk_color_alloc(cmap, &newbg);
	bg = newbg.pixel;
    }
    XSetWindowBackground(gdk_display, GDK_WINDOW_XWINDOW(dw->win), bg);
    if (dw->reverse_gc) XSetForeground(gdk_display, GDK_GC_XGC(dw->reverse_gc), dw->bg);
    if (dw->win2) {
    	XSetWindowBackground(gdk_display, GDK_WINDOW_XWINDOW(dw->win2), bg);
    }
    if (dw->win3) {
    	XSetWindowBackground(gdk_display, GDK_WINDOW_XWINDOW(dw->win3), bg);
    }
}

void
dw_set_bg(DispWindow *dw, CARD32 bg)
{
    g_return_if_fail (dw != NULL);

    dw->bg = bg;
    dw_real_set_bg(dw);
}

void
dw_set_fg(DispWindow *dw, CARD32 fg)
{
    g_return_if_fail (dw != NULL);

    dw->fg = fg;
    if (dw->gc) {
        XSetForeground(gdk_display, GDK_GC_XGC(dw->gc), fg);
	if (dw->reverse_gc) XSetBackground(gdk_display, GDK_GC_XGC(dw->reverse_gc), fg);
    }
    if (!ami_use_underline) {
	dw_real_set_bg(dw);
    }
}

void
dw_set_cursor_pos(DispWindow *dw, int pos)
{
    g_return_if_fail (dw != NULL);
    /* FIXME */
    dw->caret = pos;
    dw->has_temp_hangul = 0;
    dw_update_width(dw);
}

void
dw_hide(DispWindow *dw)
{
    g_return_if_fail(dw != NULL);
    if (dw->win == NULL) return;
    if (gdk_window_is_visible(dw->win)) {
	gdk_window_hide(dw->win);
	if (dw->win2) gdk_window_hide(dw->win2);
	if (dw->win3) gdk_window_hide(dw->win3);
    }
}

void
dw_show(DispWindow *dw)
{
    g_return_if_fail(dw != NULL);
    if (dw->win == NULL) return;
    if (!gdk_window_is_visible(dw->win)) {
	gdk_window_show(dw->win);
	if (dw->win2) gdk_window_show(dw->win2);
	if (dw->win3) gdk_window_show(dw->win3);
    } else {
        gdk_window_raise(dw->win);
	if (dw->win2) gdk_window_raise(dw->win2);
	if (dw->win3) gdk_window_raise(dw->win3);
    }
}

static void
dw_draw_simple(DispWindow *dw)
{
    int font_height;
    g_return_if_fail (dw != NULL);

    gdk_window_clear(dw->win);
    if (dw->area.width < MIN_WIDTH) return;

    font_height = dw->font->ascent + dw->font->descent;
    if (dw->text[0] != '\0') {
	gdk_draw_string(dw->win, dw->font, dw->gc, -dw->xoffset, dw->font->ascent,
	    dw->text);
	if (dw->caret >= 0 && ami_use_underline)
	   gdk_draw_line(dw->win, dw->gc, 0, font_height-1, dw->win_width, font_height-1);
    }
    if (dw->has_temp_hangul) {
	gdk_draw_rectangle(dw->win, dw->gc, 1,
		-dw->xoffset + dw->caret_pixel, 0,
		dw_seg_width(dw, dw->caret - 2, 2),
		font_height);
	gdk_draw_text(dw->win, dw->font, dw->reverse_gc,
		-dw->xoffset + dw->caret_pixel, dw->font->ascent, dw->text + dw->caret - 2, 2);
    } else if (dw->caret >= 0){
	int x = dw->caret_pixel - dw->xoffset;
	gdk_draw_line(dw->win, dw->gc, x, 0, x, font_height);
	gdk_draw_line(dw->win, dw->gc, x - 1, 1, x+1, 1);
	gdk_draw_line(dw->win, dw->gc, x - 1, font_height-1, x+1, font_height);
    }
}

static void
dw_draw_caret(DispWindow *dw)
{
    int x;
    GdkWindow *win;
    int font_height;

    font_height = dw->font->ascent + dw->font->descent;

    if (dw->has_temp_hangul) {
	/*In the middle of a Hangul syllable formation, draw it in
	 * inverse mode instead of the caret. */
	int len = MBC_LEN_B(dw->text+dw->caret-1);
	if (debug > 3) g_print("%s: MBC_LEN_B()=%d\n",__FUNCTION__,len);
	/* FIXME : We may cheat here just using 3 for UTF8 and 2 for 
	   EUC-KR. J. Shin */
	if (dw->caret <= dw->cn1) {
	    /* caret is in win1 */
	    win = dw->win;
	    x = dw_seg_width(dw, 0, dw->caret - len);
	} else if (dw->caret <= dw->cn1 + dw->cn2) {
	    /* caret is in win2 */
	    win = dw->win2;
	    x = dw_seg_width(dw, dw->cn1, dw->caret - len - dw->cn1);
	} else {
	    /* caret is in win3 */
	    win = dw->win3;
	    x = dw_seg_width(dw, dw->cn1+dw->cn2,
	    	             dw->caret - len - dw->cn1 - dw->cn2);
	}
	g_assert (win != NULL);
	gdk_draw_rectangle(win, dw->gc, 1, x, 0,
		dw_seg_width(dw, dw->caret - len, len), font_height);
	gdk_draw_text(win, dw->font, dw->reverse_gc,
		x, dw->font->ascent, dw->text + dw->caret - len, len);
    } else {
	if (dw->caret <= dw->cn1) {
	    win = dw->win;
	    x = dw_seg_width(dw, 0, dw->caret);
	} else if (dw->caret <= dw->cn1 + dw->cn2) {
	    win = dw->win2;
	    x = dw_seg_width(dw, dw->cn1, dw->caret - dw->cn1);
	} else {
	    win = dw->win3;
	    x = dw_seg_width(dw, dw->cn1+dw->cn2,
	    		     dw->caret - dw->cn1 - dw->cn2);
	}
	g_assert (win != NULL);
	gdk_draw_line(win, dw->gc, x, 0, x, font_height);
	gdk_draw_line(win, dw->gc, x - 1, 1, x+1, 1);
	gdk_draw_line(win, dw->gc, x - 1, font_height-1, x+1, font_height);
    }
}

static void
dw_draw_simple2(DispWindow *dw)
{
    int height;
    int need_width;

    height = dw->font->ascent + dw->font->descent;
    need_width = dw_seg_width(dw, 0, dw->cn1);
    if (dw->caret == dw->cn1 && !dw->has_temp_hangul) need_width += 2; /* for caret display */
    if (dw->x + need_width> dw->area.width) {
	int x;
	gdk_window_get_position(dw->win, &x, NULL);
	if (x != dw->area.width - need_width)
	    gdk_window_move (dw->win, dw->area.width - need_width,
			 dw->y - dw->font->ascent);
	gdk_window_get_position(dw->win, &x, NULL);
    }
    gdk_window_clear(dw->win);
    gdk_draw_text(dw->win, dw->font, dw->gc, 0, dw->font->ascent, dw->text, dw->cn1);
    if (ami_use_underline)
	gdk_draw_line(dw->win, dw->gc, 0, height-1,
		      dw_seg_width(dw, 0, dw->cn1), height-1);
    if (dw->win2) {
	gdk_window_clear(dw->win2);
	gdk_draw_text(dw->win2, dw->font, dw->gc, 0, dw->font->ascent, dw->text + dw->cn1, dw->cn2);
	if (ami_use_underline)
		gdk_draw_line(dw->win2, dw->gc, 0, height-1,
			      dw_seg_width(dw, dw->cn1, dw->cn2),
			      height-1);
    }
    if (dw->win3) {
	gdk_window_clear(dw->win3);
	gdk_draw_text(dw->win3, dw->font, dw->gc, 0, dw->font->ascent, dw->text + (dw->cn1+dw->cn2), dw->cn3);
	if (ami_use_underline)
		gdk_draw_line(dw->win3, dw->gc, 0, height-1,
			      dw_seg_width(dw, dw->cn1+dw->cn2, dw->cn3),
			      height-1);
    }
    dw_draw_caret(dw);

}

static void
dw_draw_extra(DispWindow *dw)
{
    int height;

    height = dw->font->ascent + dw->font->descent;

    gdk_window_clear(dw->win);
    if (dw->cn1)
    	gdk_draw_text(dw->win, dw->font, dw->gc, 0, dw->font->ascent, dw->text, dw->cn1);
    if (ami_use_underline)
	    gdk_draw_line(dw->win, dw->gc, 0, height-1,
			  dw_seg_width(dw, 0, dw->cn1), height-1);
    if (dw->win2) {
	gdk_window_clear(dw->win2);
	gdk_draw_text(dw->win2, dw->font, dw->gc, 0, dw->font->ascent, dw->text + dw->cn1, dw->cn2);
	if (ami_use_underline)
		gdk_draw_line(dw->win2, dw->gc, 0, height-1,
			      dw_seg_width(dw, dw->cn1, dw->cn2),
			      height-1);
    }
    if (dw->win3) {
	gdk_window_clear(dw->win3);
	gdk_draw_text(dw->win3, dw->font, dw->gc, 0, dw->font->ascent, dw->text + (dw->cn1+dw->cn2), dw->cn3);
	if (ami_use_underline)
		gdk_draw_line(dw->win3, dw->gc, 0, height-1,
			      dw_seg_width(dw, dw->cn1+dw->cn2, dw->cn3),
			      height-1);
    }
    dw_draw_caret(dw);
}


void
dw_draw(DispWindow *dw)
{
    g_return_if_fail (dw != NULL);
    if (dw->win == NULL) return;
    if (!gdk_window_is_visible(dw->win)) return;

    if (dw->caret < 0 || dw->ic->input_style & XIMPreeditArea)
    	dw_draw_simple(dw);
    else if (dont_move_to_next_line(dw))
        dw_draw_simple2(dw);
    else
        dw_draw_extra(dw);
}

void
dw_update_width(DispWindow *dw)
{
    int tw;
    int width;
    int max_x;
    int font_height;
    int text_len;
    int w1, w2, w3;
    int w, h;
    int y2, y3;
    int cn1, cn2, cn3;
    int max_character;

    g_return_if_fail (dw != NULL);
    if (!dw->win) return;


    font_height = dw->font->ascent + dw->font->descent;

    if (dw->area.width <= MIN_WIDTH) {
	dw_guess_area(dw);
    }

    if (dw->caret < 0) {
	int x;
	if (dw->area.x < 0) x = 0;
	else x = dw->area.x;
	gdk_window_move_resize(dw->win, x, dw->area.height + dw->area.y - font_height, dw->area.width, font_height);
	dw_draw(dw);
	return;
    }

    tw = string_width(dw, dw->text);
    text_len = strlen(dw->text);

    if (dw->ic->input_style & XIMPreeditArea) {
	int caret_pixel = dw_seg_width(dw, 0, dw->caret);
	if (caret_pixel > dw->area.width) {
	    dw->xoffset = dw->x + caret_pixel - dw->area.width;
	} else {
	    dw->xoffset = 0;
	}
	if (dw->has_temp_hangul) {
		  int len= MBC_LEN_B(dw->text + dw->caret-1);
	    dw->caret_pixel = (caret_pixel
			       - dw_seg_width(dw, dw->caret - len, len));
	} else {
	    dw->caret_pixel = caret_pixel;
	}
	dw_draw(dw);
	return;
    }

    /* get_cn1 */
    max_x = dw->area.x < 0?dw->area.width:(dw->area.x+dw->area.width);
    if (dw->x + tw <= max_x) {
	cn1 = text_len;
	w1 = tw;
	if (cn1 == dw->caret && !dw->has_temp_hangul) w1 += 2;
	cn2 = 0;
	if (dw->win2) {
	    gdk_window_remove_filter (dw->win2,
	    	(GdkFilterFunc)dw_event_filter_extra, dw);
	    gdk_window_destroy(dw->win2);
	    dw->win2 = NULL;
	}
	cn3 = 0;
	if (dw->win3) {
	    gdk_window_remove_filter (dw->win3,
	    	(GdkFilterFunc)dw_event_filter_extra, dw);
	    gdk_window_destroy(dw->win3);
	    dw->win3 = NULL;
	}
    } else if (dont_move_to_next_line(dw)) {
	cn1 = text_len;
	w1 = tw;
	if (cn1 == dw->caret && !dw->has_temp_hangul) w1 += 2;
	cn2 = 0;
	if (dw->win2) {
	    gdk_window_remove_filter (dw->win2,
	    	(GdkFilterFunc)dw_event_filter_extra, dw);
	    gdk_window_destroy(dw->win2);
	    dw->win2 = NULL;
	}
	cn3 = 0;
	if (dw->win3) {
	    gdk_window_remove_filter (dw->win3,
	    	(GdkFilterFunc)dw_event_filter_extra, dw);
	    gdk_window_destroy(dw->win3);
	    dw->win3 = NULL;
	}
    } else {
	w1 = max_x - dw->x;
	cn1 = get_fit_cnum(dw, dw->text, w1);
	max_character = get_fit_cnum(dw, dw->text + cn1, dw->area.width);
	if (text_len - cn1 > max_character) {
	    cn2 = max_character;
	    w2 = dw->area.width;
	    y2 = dw->y + ((dw->linespace > 0)?dw->linespace:font_height);
	    if (y2 + dw->font->descent > dw->area.height) {
		y2 = dw->font->ascent;
	    }
	    if (!dw->win2) {
		dw->win2 = dw_extra_win_new(dw, y2 - dw->font->ascent, w2);
	    } else {
		gdk_window_move_resize(dw->win2, 0, y2-dw->font->ascent, w2, font_height);
	    }
	    cn3 = text_len - cn1 - cn2;
	    if (dw->caret == text_len && !dw->has_temp_hangul)
	    	w3 = string_width(dw, dw->text + (cn1+cn2)) + 2;
	    else
	    	w3 = string_width(dw, dw->text + (cn1+cn2));
	    y3 = y2 + ((dw->linespace > 0)?dw->linespace:font_height);
	    if (y3 + dw->font->descent > dw->area.height) {
		y3 = dw->font->ascent;
	    }
	    if (!dw->win3) {
		dw->win3 = dw_extra_win_new(dw, y3 - dw->font->ascent, w3);
	    } else {
		gdk_window_move_resize(dw->win3, 0, y3-dw->font->ascent, w3, font_height);
	    }
	} else {
	    cn2 = text_len - cn1;
	    if (dw->caret == text_len && !dw->has_temp_hangul)
	    	w2 = string_width(dw, dw->text + cn1) + 2;
	    else
	    	w2 = string_width(dw, dw->text + cn1);
	    y2 = dw->y + ((dw->linespace>0)?dw->linespace:font_height);
	    if (y2 + dw->font->descent > dw->area.height) {
		y2 = dw->font->ascent;
	    }
	    if (!dw->win2) {
		dw->win2 = dw_extra_win_new(dw, y2 - dw->font->ascent, w2);
	    } else {
		gdk_window_move_resize(dw->win2, 0, y2-dw->font->ascent, w2, font_height);
	    }
	    cn3 = 0;
	    if (dw->win3) {
		gdk_window_remove_filter (dw->win3,
		    (GdkFilterFunc)dw_event_filter_extra, dw);
		gdk_window_destroy(dw->win3);
		dw->win3 = NULL;
	    }
	}

    }
    dw->cn1 = cn1; dw->cn2 = cn2; dw->cn3 = cn3;
    gdk_window_get_size(dw->win, &w, &h);
    if (w != w1 || h != font_height) {
	gdk_window_resize(dw->win, w1, font_height);
    }
    dw_draw(dw);
}

void
dw_set_cmap(DispWindow *dw, Colormap *cmap)
{
    dw->cmap = *cmap;
}

void
dw_set_cursor(DispWindow *dw, Cursor *cursor)
{
    dw->cursor = *cursor;
}

void dw_set_bg_pixmap(DispWindow *dw, Pixmap *pixmap)
{
    dw->bg_pixmap = *pixmap;
}

void
dw_unset_focus(DispWindow *dw)
{
    g_return_if_fail(dw != NULL);
    if (dw->gc == NULL) return;
    gdk_gc_set_fill(dw->gc, GDK_STIPPLED);
    if (dw->reverse_gc) gdk_gc_set_fill(dw->reverse_gc, GDK_STIPPLED);
    dw_draw(dw);
}

void
dw_set_focus(DispWindow *dw)
{
    g_return_if_fail(dw != NULL);
    if (debug) g_print("%s: ic=%d\n", __FUNCTION__, dw->ic->id);
    if (dw->gc == NULL) return;
    if (debug) g_print("2 %s: ic=%d\n", __FUNCTION__, dw->ic->id);
    gdk_gc_set_fill(dw->gc, GDK_SOLID);
    if (dw->reverse_gc) gdk_gc_set_fill(dw->reverse_gc, GDK_SOLID);
    gdk_window_raise(dw->win);
    dw_draw(dw);
}

static GdkWindow *
dw_extra_win_new(DispWindow *dw, int y, int w)
{
    GdkWindow *win;
    int mask = GDK_WA_X | GDK_WA_Y;
    GdkWindowAttr attr;
    attr.event_mask = GDK_EXPOSURE_MASK;
    attr.wclass = GDK_INPUT_OUTPUT;
    attr.window_type = GDK_WINDOW_CHILD;
    attr.x = 0;
    attr.y = y;
    attr.width = w;
    attr.height = dw->font->ascent + dw->font->descent;
    win = gdk_window_new(gdk_window_get_parent(dw->win), &attr, mask);
    gdk_window_add_filter (win, (GdkFilterFunc)dw_event_filter_extra, dw);
    XSetWindowBackground(gdk_display, GDK_WINDOW_XWINDOW(win), dw->bg);
    gdk_window_show(win);
    return win;
}
