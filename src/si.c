
/* this file is a part of ami software, (C) Hwang chi-deok 1997-1999      */

#include "config.h"

#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "amitype.h"
#include "edit.h"
#include "ami.h"
#include "chartable.h"
#include "utf8.h"


static gint current_x = 0;
static gint current_y = 0;
static gint current_page_num = 0;

static iconv_t icd_to_utf8=NULL;

static void char_frame(GtkWidget * notebook, char *title, char first,
		       char second, int row, int col);

static void
si_destroyed(GtkWidget *win)
{
    IC *ic;
    HangulState *han;

    ic = (IC *)gtk_object_get_data((GtkObject *)win, "ic");
    if (!ic || !ic->han) return;
    han = (HangulState *)gtk_object_get_data((GtkObject *)win, "han");
    if (ic->han != han) return;
    ic->han->aux_input = NULL;
}

static int
key_press_cb(GtkWidget *w, GdkEventKey *ev)
{
    if (ev->keyval == GDK_Tab) {
	GtkNotebook *notebook = gtk_object_get_data((GtkObject *)w, "notebook");
	GtkWidget *ctable;
	if (current_page_num == 7) /* last page; */
	    gtk_notebook_set_page(notebook, 0);
	else
	    gtk_notebook_next_page(notebook);
	ctable = GTK_BIN(gtk_notebook_get_nth_page (notebook,gtk_notebook_get_current_page(notebook)))->child;
	gtk_widget_grab_focus(ctable);
	gtk_signal_emit_stop_by_name(GTK_OBJECT(w), "key_press_event");
	return TRUE;
    } else if (ev->keyval == GDK_Escape) {
	gtk_widget_destroy(w);
	return TRUE;
    }
    return FALSE;
}

static void
notebook_switch(GtkWidget *w, GtkNotebookPage *page, int page_num)
{
    CharTable *table;
    if (!GTK_WIDGET_REALIZED(w)) return;
    table = CHAR_TABLE(GTK_BIN(page->child)->child);
    current_page_num = page_num;
    current_x = table->current_x;
    current_y = table->current_y;
    gtk_widget_grab_focus((GtkWidget *)table);
}

GtkWidget *
special_char_input(void)
{
    GtkWidget *notebook;
    GtkWidget *win;
    GtkWidget *vbox;
    GtkWidget *current_table;
    int x, y, num;

    win = gtk_window_new(GTK_WINDOW_DIALOG);
    gtk_window_set_position(GTK_WINDOW(win), GTK_WIN_POS_CENTER);
    gtk_signal_connect(GTK_OBJECT(win), "destroy",
		       GTK_SIGNAL_FUNC(si_destroyed), NULL);
    gtk_window_set_title(GTK_WINDOW(win), _("Ami: Special/Symbol Character Input"));
    gtk_container_set_border_width(GTK_CONTAINER(win), 10);
    gtk_signal_connect((GtkObject *)win, "key_press_event", (GtkSignalFunc)key_press_cb, NULL);
    vbox = gtk_vbox_new(FALSE, 5);
    gtk_container_add(GTK_CONTAINER(win), vbox);
    gtk_widget_show(vbox);

    notebook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(notebook), TRUE);
    gtk_signal_connect((GtkObject *)notebook, "switch_page", (GtkSignalFunc)notebook_switch, NULL);
    gtk_object_set_data((GtkObject *)win, "notebook", notebook);
    gtk_notebook_set_tab_pos(GTK_NOTEBOOK(notebook), GTK_POS_TOP);
    gtk_box_pack_start(GTK_BOX(vbox), notebook, TRUE, TRUE, 0);

    /* FIXME : codeset names are not standardized and OS-dependent. */
    if (ami_codeset==AMI_UTF8 && (! icd_to_utf8 || icd_to_utf8 == (iconv_t) -1)) {
        icd_to_utf8 = iconv_open("UTF-8", EUCKR_ICD);
        g_assert(icd_to_utf8 != (iconv_t) (-1));
    }

    char_frame(notebook, _("Math etc"), 0xa1, 0xa0, 6, 16);
    char_frame(notebook, _("Var. symbols"), 0xa2, 0xa0, 6, 12);
    char_frame(notebook, _("Fullwidth ASCII"), 0xa3, 0xa0, 6, 16);
    char_frame(notebook, _("Hangul Jamo"), 0xa4, 0xa0, 6, 16);
    char_frame(notebook, _("Greek"), 0xa5, 0xa0, 6, 16);
    char_frame(notebook, _("Box drawings"), 0xa6, 0xa0, 5, 14);
    char_frame(notebook, _("Meas. units"), 0xa7, 0xa0, 5, 16);
    char_frame(notebook, _("Circled"), 0xa8, 0xa0, 6, 16);
    char_frame(notebook, _("Parenthesized"), 0xa9, 0xa0, 6, 16);
    char_frame(notebook, _("Hiragana"), 0xaa, 0xa0, 6, 16);
    char_frame(notebook, _("Katakana"), 0xab, 0xa0, 6, 16);
    char_frame(notebook, _("Cyrillic"), 0xac, 0xa0, 6, 16);
    x = current_x; y = current_y;
    num = current_page_num;
    gtk_notebook_set_page((GtkNotebook *)notebook, num);
    current_table = GTK_BIN(gtk_notebook_get_nth_page((GtkNotebook *)notebook, num))->child;
    char_table_set_prelight((CharTable *)current_table, x, y);
    gtk_widget_grab_focus(current_table);
    gtk_widget_show_all(win);
    return win;
}

static void
char_selected(GtkWidget *widget, char *s)
{
    GtkWidget *top = gtk_widget_get_toplevel(widget);
    GtkWidget *notebook;
    IC *ic;
    ic = gtk_object_get_data((GtkObject *)top, "ic");
    if (ic == NULL) {
	g_warning("%s: IC attached to special chars dialog is null",__FUNCTION__);
    } else {
	if (ic->han == NULL) {
	    g_warning("%s: Hangul Buffer attached to special chars dialog is null",__FUNCTION__);
	} else {
	    if (ic->han == gtk_object_get_data((GtkObject *)top, "han"))
		editing_insert(ic, s, strlen(s));
	    else g_warning("%s: han is different from original.",__FUNCTION__);
	}
    }
    gtk_widget_destroy(top);
}

static void
char_moved(GtkWidget *w, gint x, gint y)
{
    current_x = x;
    current_y = y;
}

static void
char_frame(GtkWidget * notebook, char *title,
           char first, char second, int row, int col)
{
    GtkWidget *frame, *label;
    GtkWidget *table;
    int i, k;
    char **buf;
    int len= ami_codeset==AMI_UTF8 ? 3 : 2 ;  /* Even in UTF-8, some special 
						 chars take only two bytes, but
						 we can be lazy here. */ 
    buf = g_new(char *, col*row);
    for (i = 0; i < col*row; i++)
	buf[i] = g_new(char, len+1);
    k = second;
    if ( ami_codeset==AMI_EUC) 
    for (i = 0; i < row*col; i++) {
	buf[i][0] = first;
	buf[i][1] = k++;
	buf[i][2] = '\0';
	}
    else {
        char tbuf[3];
	char *iptr,*optr;
	size_t ilen,olen;
	for (i = 0; i < row*col; i++) {
	    tbuf[0] = first;
	    tbuf[1] = k++;
	    tbuf[2] = '\0';
	    iptr=tbuf;
	    optr=buf[i];
	    ilen=3;
	    olen=4; 
	    if ( iconv(icd_to_utf8,&iptr,&ilen,&optr,&olen) == (size_t) -1) 
		strcpy(buf[i],"\xef\xbf\xbd");  /*U+FFFD in UTF-8 */
	    else 
	      if (olen) *optr='\0';
	}
    }

    frame = gtk_frame_new(NULL);
    gtk_container_set_border_width(GTK_CONTAINER(frame), 10);
    gtk_widget_show(frame);

    table = char_table_new(buf, col, col*row);
    gtk_container_add(GTK_CONTAINER(frame), table);
    gtk_signal_connect(GTK_OBJECT(table), "selected",
    	GTK_SIGNAL_FUNC(char_selected), NULL);
    gtk_signal_connect(GTK_OBJECT(table), "moved",
    	GTK_SIGNAL_FUNC(char_moved), NULL);
    gtk_widget_show(table);
    label = gtk_label_new(title);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), frame, label);
}
