#include "amitype.h"

GdkWindow *cli_win_new(Window win, IC *ic);
void cli_win_remove_filter(GdkWindow *win);
void cli_win_is_dead(IC *ic, gpointer data);
void cli_win_register(GdkWindow *win);
GdkFilterReturn cli_win_monitor(XEvent *xev, GdkEvent *ev, gpointer data);
void cli_set_status_ic(IC *ic);
void cli_set_edit_ic(IC *ic);
void cli_unset_edit_ic(IC *ic);
void cli_unset_status_ic(IC *ic);
IC * cli_get_status_ic(IC *ic);
IC * cli_get_edit_ic(IC *ic);
