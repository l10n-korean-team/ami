#ifndef __AMI_UTF8_H
#define __AMI_UTF8_H

#include <iconv.h>

/*  macros for UTF-8 */
#define IS_UTF8_TRAIL(x)  ( !((0x80 ^ (x)) & 0xc0) )
#define IS_UTF8_2BYTE(x)  ( !((0xc0 ^ (x)) & 0xe0) )
#define IS_UTF8_3BYTE(x)  ( !((0xe0 ^ (x)) & 0xf0) )
#define IS_UTF8_4BYTE(x)  ( !((0xf0 ^ (x)) & 0xf8) )

/* x must be char */
#define MBC_LEN(x) \
    ( ami_codeset == AMI_EUC ? ((x) & 0x80 ? 2 : 1) : \
		     IS_UTF8_3BYTE(x) ? 3 :           \
		     IS_UTF8_2BYTE(x) ? 2 :           \
		     IS_UTF8_4BYTE(x) ? 4 :           \
                     (x) & 0x80 ? 0 : 1)          

/* x must be *char !! */
/* FIXME : This is not very robust !!! */
#define MBC_LEN_B(x) \
    ( ami_codeset == AMI_EUC ? ((*(x)) & 0x80 ? 2 : 1) : \
		     ! ((*(x)) & 0x80) ? 1             : \
		     IS_UTF8_2BYTE(*((x)-1)) ? 2       : \
		     IS_UTF8_3BYTE(*((x)-2)) ? 3       : \
		     IS_UTF8_4BYTE(*((x)-3)) ? 4 : 0) 

#endif

