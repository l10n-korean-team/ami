#ifndef __IC_H
#define __IC_H

#include "amitype.h"

void ic_destroy(IC *ic);
IC * ic_find_with_id(CARD16 id);
IC *ic_find(gpointer data);
void ic_get(IMChangeICStruct *call_data);
void ic_set(IMChangeICStruct *call_data);
void ic_create(IMChangeICStruct *call_data);
int ami_editing_flush(IC *ic);
int ami_commit_string(IC *ic, char *buf);
void ic_list_foreach(void (*func)(IC *ic, gpointer data), gpointer data);
int ic_is_valid(IC *ic);
int ic_is_valid_id(CARD16 id);
HangulState * ic_han_new(void);
void ic_han_free(HangulState *han);
#endif /* __IC_H */
