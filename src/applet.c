/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

/* Author: Han Ji-Ho <hanjiho@penta.co.kr>  */
/* With code from wmxmms, gnomex11amp and battery_meter */

#include "config.h"


#include <stdio.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <X11/Xatom.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "cp.h"
#include "ami.h"
#include "hinput.h"
#include "handler.h"

#include "ami-h.xpm"
#include "ami-e.xpm"
#include "ami-d.xpm"


/* Common variables */
static GtkWidget *d_area;
static GdkPixbuf *pixbuf_h=NULL, *pixbuf_e=NULL, *pixbuf_d=NULL;
static GdkFilterReturn e_event_translate(XEvent *xev, GdkEvent *ev, gpointer data);
static gboolean e_is_running(void);
static gboolean kde_is_running(void);
static gboolean kde2_is_running(void);
static gboolean wmaker_is_running(void);
static int x_offset = 0, y_offset = 0;
static gint wm_type;
static gint icon_size = -1;

static void wm_change_shape_mask(GtkWidget *win, GdkPixbuf *pixbuf);
static void expose_cb(GtkWidget *w, GdkEventExpose *event, gpointer data);


/* Common functions */
void 
draw_applet_image(gint hangul_mode)
{
  GdkPixbuf *pixbuf;
  if(ami_run_mode == AMI_NORMAL_RUN) {
      static int old_mode = -3;
      if ((!unique_han && support_status) || old_mode == hangul_mode) return;
      if (hangul_mode == AMI_PIXMAP_ENGLISH) {
	 gdk_window_set_icon_name(cp_win->window, _("ENG: Ami"));
      } else if (hangul_mode == AMI_PIXMAP_HANGUL) {
	 gdk_window_set_icon_name(cp_win->window, _("KOR: Ami"));
      } else {
	 gdk_window_set_icon_name(cp_win->window, _("Ami Preference"));
      }
      old_mode = hangul_mode;
      return;
  }
  switch(hangul_mode) {
      case AMI_PIXMAP_ENGLISH: 
	  pixbuf = pixbuf_e; break;
      case AMI_PIXMAP_HANGUL: 
	  pixbuf = pixbuf_h; break;
      default :
	  pixbuf = pixbuf_d; break;
  }
  if (!pixbuf) return;
  if (ami_run_mode == AMI_WM_RUN) wm_change_shape_mask(d_area, pixbuf);
  else gdk_window_clear(d_area->window);
  gdk_pixbuf_render_to_drawable_alpha(pixbuf, d_area->window, 
  	0, 0, x_offset, y_offset, 
	gdk_pixbuf_get_width(pixbuf),
	gdk_pixbuf_get_height(pixbuf),
	GDK_PIXBUF_ALPHA_BILEVEL, 128, GDK_RGB_DITHER_NORMAL,
	0,0);
}

static void 
expose_cb(GtkWidget *w, GdkEventExpose *event, gpointer data)
{
  if(current_focus_ic==NULL) 
    draw_applet_image(AMI_PIXMAP_NONE);
  else if((current_focus_ic->composing_hangul))
    draw_applet_image(AMI_PIXMAP_HANGUL);
  else
    draw_applet_image(AMI_PIXMAP_ENGLISH);
}

static gint 
cp_delete_cb( GtkWidget *widget, void *data )
{
    gtk_widget_hide(widget); /* hide cp_win */
    return TRUE;
}

static GdkPixbuf*
get_pixbuf(int type)
{
    const char **xpm_d;
    GdkPixbuf *pixbuf;
    if (pix_dir_name && pix_dir_name[0] && strcmp(pix_dir_name, "(null)")) {
	char *file;
	switch (type) {
	    case AMI_PIXMAP_ENGLISH: 
		file = g_strconcat(pix_dir_name, "/ami-e.xpm", NULL);
		break;
	    case AMI_PIXMAP_NONE: 
		file = g_strconcat(pix_dir_name, "/ami-d.xpm", NULL);
		break;
	    default:
		file = g_strconcat(pix_dir_name, "/ami-h.xpm", NULL);
		break;
	}
	pixbuf = gdk_pixbuf_new_from_file(file);
	if (pixbuf) {
	    g_free(file);
	    return pixbuf;
	}

	g_warning(_("Cannot create pixbuf using '%s'. Use internal pixmap"),
		  file);
	g_free(file);
    } 

    switch(type) {
	case AMI_PIXMAP_NONE:
	    xpm_d = ami_d_xpm;
	    break;
	case AMI_PIXMAP_ENGLISH:
	    xpm_d = ami_e_xpm;
	    break;
	default:
	    xpm_d = ami_h_xpm;
	    break;
    }
    pixbuf = gdk_pixbuf_new_from_xpm_data(xpm_d);
    return pixbuf;
}

static GdkPixbuf *
fit_size(GdkPixbuf *pixbuf, int size)
{
    if (size <= 0 || pixbuf == NULL) return pixbuf;
    if (size != gdk_pixbuf_get_width(pixbuf)
       || size != gdk_pixbuf_get_height(pixbuf)) {
    	GdkPixbuf *new_pixbuf = gdk_pixbuf_scale_simple(pixbuf, size, size, 
	    GDK_INTERP_HYPER);
	gdk_pixbuf_unref(pixbuf);
	return new_pixbuf;
    }
    return pixbuf;
}

static void
applet_change_pixel_size(GtkWidget *w, int size)
{
    GdkPixbuf *pixbuf;
    static int panel_size = -1;
    if (size == 0) size = panel_size;
    if (pixbuf_h) {
	gdk_pixbuf_unref(pixbuf_h);
	gdk_pixbuf_unref(pixbuf_e);
	gdk_pixbuf_unref(pixbuf_d);
    }
    if (w) gtk_widget_set_usize(w, size, size);
    panel_size = size;
    pixbuf_h = fit_size(get_pixbuf(AMI_PIXMAP_HANGUL), size);
    pixbuf_e = fit_size(get_pixbuf(AMI_PIXMAP_ENGLISH), size);
    pixbuf_d = fit_size(get_pixbuf(AMI_PIXMAP_NONE), size);
}

void
applet_change_pixmap_dir(char *dir_name)
{
    if (pix_dir_name) g_free(pix_dir_name);
    pix_dir_name = g_strdup(dir_name);
    applet_change_pixel_size(NULL, 0);
    draw_applet_image(AMI_PIXMAP_NONE);
}

#ifdef GNOME_APPLET
#include <gnome.h>
#include <applet-widget.h>

static void about (AppletWidget *applet, gpointer data)
{
    static const char *authors[] = { N_("HWANG ChiDeok"), NULL};
    GtkWidget *about_box;

    about_box = gnome_about_new(_("Ami Applet"), VERSION,
			    _("Copyright (C) HWANG ChiDeok 1999,2000,2001"),
			    authors,
			    _("Gnome applet for Korean Input Server Ami"),
			    NULL);

    gtk_widget_show(about_box);
}


static gint 
prop_destroy_cb( GtkWidget *widget, void *data )
{
    cp_win = NULL;
    return FALSE;
}

static void 
properties (AppletWidget *applet, gpointer data)
{
   GtkWidget *box, *vbox, *label, *frame;
  
   if( cp_win ){
       gtk_widget_show(cp_win);
       gdk_window_raise(cp_win->window);
       return;
   }

  cp_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(cp_win), _("Ami Preference"));
  gtk_container_add(GTK_CONTAINER(cp_win), cp);

  gtk_signal_connect( GTK_OBJECT(cp_win),
		      "delete_event", GTK_SIGNAL_FUNC(cp_delete_cb), NULL );

  gtk_widget_show_all(cp_win);
}

int 
ami_gnome_main(int argc, char *argv[], char* rc_file)
{
  GtkWidget *applet, *frame;
  GdkBitmap *mask;
  gint i, j;
  
  /* get rid of arguments not recognized by applet_widget_init */
  for (i = 1; i < argc; i++) {
      if (!strcmp(argv[i], "-debug") || !strcmp(argv[i], "-gnome") || 
          !strcmp(argv[i], "-wm")) {
	  argc--;
	  for(j = i;j < argc;j++) argv[j] = argv[j+1];
	  if(argc <= 1) break;
	  i--;
	  continue;
      }
  }

  applet_widget_init("ami_applet", VERSION, argc, argv,
	  NULL, 0, NULL);
  gdk_rgb_init();
  gtk_widget_push_visual(gdk_rgb_get_visual());
  gtk_widget_push_colormap(gdk_rgb_get_cmap());
  gtk_rc_parse(rc_file);
  applet = applet_widget_new("ami_applet");
  gtk_widget_realize(applet);

  applet_widget_register_stock_callback (APPLET_WIDGET (applet),
	  "about", GNOME_STOCK_MENU_ABOUT,
	  _("About..."), about, NULL);
  applet_widget_register_stock_callback (APPLET_WIDGET (applet),
	  "properties", GNOME_STOCK_MENU_PROP,
	  _("Properties..."), properties, NULL);
  d_area = gtk_drawing_area_new();
  gtk_widget_set_app_paintable(d_area,TRUE);
  gtk_widget_set_usize(d_area, 10, 10);
  gtk_signal_connect_object(GTK_OBJECT(applet), "change-pixel-size",
  	GTK_SIGNAL_FUNC(applet_change_pixel_size), GTK_OBJECT(d_area));

  gtk_widget_set_events(d_area, GDK_EXPOSURE_MASK );
  gtk_signal_connect(GTK_OBJECT(d_area),"expose_event",
	  GTK_SIGNAL_FUNC(expose_cb),NULL);

  gdk_window_clear(applet->window);
  applet_widget_add(APPLET_WIDGET(applet),d_area);
  gtk_widget_show_all(applet);	


  ami_init_im(d_area);

  cp = create_cp();

  XBell(gdk_display, bell_intensity);
  applet_widget_gtk_main();
  IMCloseIM(xims);
  
  return 0;
}

#endif /* GNOME_APPLET */


/**********************************************************************/


static void 
wm_destroy_cb( GtkWidget *widget, void *data )
{
    IMCloseIM(xims);
    gtk_exit(0);
}

static void 
wm_button_press_cb(GtkWidget * w, GdkEventButton * event, gpointer data)
{
    if(event->type != GDK_2BUTTON_PRESS) return; /* only process double-click */
    if(cp_win) {
	gtk_widget_show(cp_win);
	gdk_window_raise(cp_win->window);
	return;
    }
    cp_win = gtk_window_new(GTK_WINDOW_DIALOG);
    gtk_window_set_title(GTK_WINDOW(cp_win), _("Ami Preference"));
    gtk_container_add(GTK_CONTAINER(cp_win), cp);
    gtk_signal_connect(GTK_OBJECT(cp_win), "delete_event", GTK_SIGNAL_FUNC(cp_delete_cb), NULL);
    gtk_widget_show_all(cp_win);
}

static void
wm_size_allocate_cb(GtkWidget *w, GtkAllocation * allocation, gpointer data)
{
    if (allocation->height >= 200) return;
    applet_change_pixel_size(NULL, allocation->height);
    icon_size = allocation->height;
    draw_applet_image(AMI_PIXMAP_NONE);
}

static void
wm_change_shape_mask(GtkWidget *win, GdkPixbuf *pixbuf)
{
    int w, h;
    GdkPixmap *mask;
    GdkGC *gc;
    GdkColor color;
    int size = 64;

    if (pixbuf == NULL) return;
    if (wm_type == WM_KDE1 || wm_type == WM_KDE2) {
	if (icon_size <= 0) return;
	size = icon_size;
    }
    w = gdk_pixbuf_get_width(pixbuf);
    h = gdk_pixbuf_get_height(pixbuf);
    if (w != size) {
	x_offset = (size - w)/2;
    }
    if (h != size) {
	y_offset = (size - h)/2;
    }
    mask = gdk_pixmap_new(NULL, size, size, 1);
    gc = gdk_gc_new (mask);
    color.pixel = 0;
    gdk_gc_set_foreground (gc, &color);
    gdk_draw_rectangle (mask, gc, TRUE, 0, 0, size, size);
    gdk_gc_unref (gc);

    gdk_pixbuf_render_threshold_alpha (pixbuf, mask,
        0, 0, x_offset, y_offset, w, h, 128);
    gdk_window_shape_combine_mask(d_area->window, mask, 0, 0);
    gdk_pixmap_unref(mask);
}

static int
check_wm(void)
{

    if (dock_style != WM_DETECT) return dock_style;

    if (debug) {
	if (wmaker_is_running()) printf("WindowMaker check return OK\n");
	else printf("WindowMaker check return No\n");
	if (e_is_running()) printf("Enlightenment check return OK\n");
	else printf("Enlightenment check return No\n");
	if (kde_is_running()) printf("KDE1 check return OK\n");
	else printf("KDE1 check return No\n");
	if (kde2_is_running()) printf("KDE2 check return OK\n");
	else printf("KDE2 check return No\n");
    }
    if (wmaker_is_running()) return WM_W;
    if (e_is_running()) return WM_E;
    if (kde2_is_running()) return WM_KDE2;
    if (kde_is_running()) return WM_KDE1;
    return WM_UNKNOWN;
}

int 
ami_wm_main(int argc, char *argv[], char *rc_file)
{
    GdkColor bg_color;
    GtkWidget *icon_win;
    GdkWindow *leader;
    GdkPixmap *pixmap;
    GdkGC *dock_gc;
    GdkGC *mask_gc;
    GdkBitmap *mask, *dock_mask;
    XWMHints hints;
    gint i, w, h;


    gtk_init(&argc, &argv);
    gdk_rgb_init();
    gtk_rc_parse(rc_file);

    wm_type = check_wm();
    if (wm_type == WM_UNKNOWN) {
	if (!wait_wm) wm_type = WM_W;
	else {
	    while (1) {
		sleep(1);
		wm_type = check_wm();
		if (wm_type != WM_UNKNOWN) break;
	    }
	}
    }

    //d_area = icon_win = gtk_window_new(GTK_WINDOW_DIALOG);
    d_area = icon_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_widget_set_app_paintable(icon_win, TRUE);
    if (wm_type != WM_KDE1 && wm_type != WM_KDE2) gtk_widget_set_usize(icon_win, 64, 64);

    gtk_widget_set_events(icon_win, GDK_BUTTON_PRESS_MASK | GDK_EXPOSURE_MASK);
    gtk_signal_connect(GTK_OBJECT(icon_win), "expose_event", 
    	GTK_SIGNAL_FUNC(expose_cb), NULL);
    gtk_signal_connect(GTK_OBJECT(icon_win), "button_press_event", 
    	GTK_SIGNAL_FUNC(wm_button_press_cb), NULL);
    gtk_signal_connect(GTK_OBJECT(icon_win), "destroy", 
    	GTK_SIGNAL_FUNC(wm_destroy_cb), NULL);
    if (wm_type == WM_KDE1 || wm_type == WM_KDE2) 
        gtk_signal_connect(GTK_OBJECT(icon_win), "size_allocate",
	    GTK_SIGNAL_FUNC(wm_size_allocate_cb), NULL);

    gtk_widget_realize(icon_win);

    /* make 3 pixmaps */
    pixbuf_d = get_pixbuf(AMI_PIXMAP_NONE);
    pixbuf_h = get_pixbuf(AMI_PIXMAP_HANGUL);
    pixbuf_e = get_pixbuf(AMI_PIXMAP_ENGLISH);
    wm_change_shape_mask(icon_win, pixbuf_d);

    if (wm_type == WM_E) {
	/* setting up dock for E  which employs a different dock mechanism
	   from that of Windowmaker */
	gdk_window_add_filter(NULL, (GdkFilterFunc)e_event_translate, NULL); 
	hints.initial_state = WithdrawnState;
	hints.window_group = GDK_WINDOW_XWINDOW(icon_win->window);
	hints.icon_x = 0;
	hints.icon_y = 0;
	hints.flags = StateHint | WindowGroupHint | IconPositionHint;
	XSetWMHints(GDK_DISPLAY(), GDK_WINDOW_XWINDOW(icon_win->window), &hints);
	XMapRaised(gdk_display, GDK_WINDOW_XWINDOW(icon_win->window));
    } else if (wm_type == WM_KDE1) {
	GdkAtom a = gdk_atom_intern("KWM_DOCKWINDOW", FALSE);
	long data = 1;
	XChangeProperty(GDK_DISPLAY(), GDK_WINDOW_XWINDOW(d_area->window), 
		a, a, 32, PropModeReplace, (unsigned char *)&data, 1);
	gtk_widget_show_all(d_area);
    } else if (wm_type == WM_KDE2) {
#if 1
	GdkAtom a = gdk_atom_intern("_KDE_NET_WM_SYSTEM_TRAY_WINDOW_FOR", FALSE);
	Window data = GDK_WINDOW_XWINDOW(d_area->window);
	XChangeProperty(GDK_DISPLAY(), data, a,
		XA_WINDOW, 32, PropModeReplace, (unsigned char *)&data, 1);
#else
	GdkAtom a = gdk_atom_intern("_NET_KDE_SYSTEM_TRAY_WINDOWS", FALSE);
	Window data = GDK_WINDOW_XWINDOW(d_area->window);
	XChangeProperty(GDK_DISPLAY(), GDK_ROOT_WINDOW(),
		a, XA_WINDOW, 32, PropModeAppend, (unsigned char *)&data, 1);
#endif
	gtk_widget_show_all(d_area);
    } else {
	hints.initial_state = WithdrawnState;
	hints.icon_window = GDK_WINDOW_XWINDOW(icon_win->window);
	hints.window_group = gdk_leader_window;
	hints.flags = WindowGroupHint | StateHint | IconWindowHint;
	XSetWMHints(GDK_DISPLAY(), gdk_leader_window, &hints);
	XMapRaised(gdk_display, gdk_leader_window);
    }

    ami_init_im(icon_win);
    cp = create_cp();

    XBell(gdk_display, bell_intensity);
    gtk_main();

    return 0;
}

static GdkFilterReturn 
e_event_translate(XEvent *xev, GdkEvent *ev, gpointer data)
{
    if (xev->xany.send_event && xev->type == ButtonPress && 
    	gdk_window_lookup(xev->xbutton.window) == NULL) {
	/* To get a 'button press' signal, E creates a window on top of 
	 * the icon window.  After extracting information they need, 
	 * it informs us of the 'event' with |SendEvent|, but 
	 * 'window info' of xbutton sent by E is not updated and  gtk+
	 * doesn't work as intended because the event comes up 
	 * to a window not created by gtk+. To work around this problem,
	 * we manually reset |window| and send the event back to X buffer.
	 * GDK_FILTER_TRANSLATE cannot be used as it doesn't support
	 * window translation. Neither can gdk_event_put because double
	 * click event is not supported. In case you wonder why wmtime
	 * works fine under E, it does because it doesn't inspect
	 * |window| but directly invokes callback.
	 */
        xev->xbutton.window = GDK_WINDOW_XWINDOW(d_area->window);
        XPutBackEvent(gdk_display, xev);
	return GDK_FILTER_REMOVE;
    }
    return GDK_FILTER_CONTINUE;
}

static gboolean
kde_is_running(void)
{
    GdkAtom en = gdk_atom_intern("KWM_RUNNING", TRUE);
    GdkAtom type;
    gint format, length;
    guchar *root_data = NULL;
    guchar *win_data = NULL;
    long eid;
    gboolean ret;
    unsigned long nitems, after;

    if (en == None) return FALSE;
    if (XGetWindowProperty(gdk_display, gdk_root_window, en, 0, 256, False, en,
    	&type, &format, &nitems, &after, &root_data) != Success) return FALSE;
    if (!root_data) return FALSE;
    if (*(GdkAtom *) root_data) {
	XFree(root_data);
	return TRUE;
    } else {
	XFree(root_data);
	return FALSE;
    }
}

static gboolean
kde2_is_running(void)
{
    GdkAtom en = gdk_atom_intern("KWIN_RUNNING", TRUE);
    GdkAtom type;
    gint format, length;
    guchar *root_data = NULL;
    guchar *win_data = NULL;
    long eid;
    gboolean ret;
    unsigned long nitems, after;

    if (en == None) return FALSE;
    if (XGetWindowProperty(gdk_display, gdk_root_window, en, 0, 256, False, en,
    	&type, &format, &nitems, &after, &root_data) != Success) return FALSE;
    if (!root_data) return FALSE;
    if (*(GdkAtom *) root_data) {
	XFree(root_data);
	return TRUE;
    } else {
	XFree(root_data);
	return FALSE;
    }
}

static gboolean
e_is_running(void)
{
    GdkAtom en = gdk_atom_intern("ENLIGHTENMENT_COMMS", TRUE);
    GdkAtom type;
    gint format, length;
    guchar *root_data = NULL;
    guchar *win_data = NULL;
    long eid;
    gboolean ret;
    unsigned long nitems, after;
    GdkAtom string_atom;

    if (en == None) return FALSE;
    string_atom = gdk_atom_intern("STRING", FALSE);
    if (XGetWindowProperty(gdk_display, gdk_root_window, en, 0, 256, False, string_atom,
    	&type, &format, &nitems, &after, &root_data) != Success) return FALSE;
    if (!root_data) return FALSE;
    if (sscanf(root_data, "WINID %lx", &eid) != 1) return FALSE;
    gdk_error_trap_push ();
    if (XGetWindowProperty(gdk_display, eid, en, 0, 256, False, string_atom,
    	&type, &format, &nitems, &after, &win_data) != Success) {
	XFree(root_data);
	return FALSE;
    }
    if (gdk_error_trap_pop () || !win_data) {
	XFree(root_data);
	return FALSE;
    }
    ret = !strcmp(root_data, win_data);
    XFree(root_data); XFree(win_data);
    return ret;
}

static gboolean
wmaker_is_running(void)
{
    GdkAtom en = gdk_atom_intern("_WINDOWMAKER_NOTICEBOARD", TRUE);
    GdkAtom type;
    gint format, length;
    guchar *root_data = NULL;
    guchar *win_data = NULL;
    long eid;
    gboolean ret;
    unsigned long nitems, after;
    GdkAtom string_atom;

    if (en == None) return FALSE;
    string_atom = gdk_atom_intern("WINDOW", FALSE);
    if (XGetWindowProperty(gdk_display, gdk_root_window, en, 0, 256, False, string_atom,
    	&type, &format, &nitems, &after, &root_data) != Success) return FALSE;
    if (!root_data) return FALSE;
    eid = *(Window*)root_data;
    gdk_error_trap_push ();
    if (XGetWindowProperty(gdk_display, eid, en, 0, 256, False, string_atom,
    	&type, &format, &nitems, &after, &win_data) != Success) {
	XFree(root_data);
	return FALSE;
    }
    if (gdk_error_trap_pop () || !win_data) {
	XFree(root_data);
	return FALSE;
    }
    ret = eid == (*(Window *)win_data);
    XFree(root_data); XFree(win_data);
    return ret;
}
