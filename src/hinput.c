/* this file is a part of Ami software, (C) Hwang chi-deok 1999,2000,2001 */

/* this file was based on hanterm by Song, JaeKeung and then
   modified for using in hgtk by Hwang, chi-deok */

#include "config.h"
#include <X11/Xlib.h>
#include <string.h>
#include <gdk/gdkkeysyms.h>
#include <X11/keysym.h>
#include <gdk/gdk.h>
#include <gdk/gdkprivate.h>
#include "ami.h"
#include "hinput.h"
#include "ic.h"
#include "comp.h"


#if 0
int 
ami_hangul_input_process(char *buf, char *c)
{
    int ret = composer_process(composer, c);
    if (hangul_keyboard_type == 2)
    	return hangul_automata2(buf, c);
    else
    	return hangul_automata3(buf, c);
}

int
ami_hangul_is_first_han(char *buf, int x)
{
    int xx;
    if (x < 0) return 0;
    xx = x;
    if(*(buf + x) & 0x80) {
	do { 
	    x--;
	} while(x >= 0 && *(buf + x) & 0x80);
	if((xx - x)%2 == 1) return 1;
    } 
    return 0;
}

int
ami_hangul_is_second_han(char *buf, int x)
{
    int xx;
    if (x < 1) return 0;
    xx = x;
    if(*(buf + x) & 0x80) {
	do { 
	    x--;
	} while(x >= 0 && *(buf + x) & 0x80);
	if((xx - x)%2 == 0) return 1;
    } 
    return 0;
}

int
ami_hangul_in_ks(int f, int m, int t)
{
    char buf[8];

    return convert_3_to_ks(f, m, t, buf) == 2;
}

gint
ami_hangul_need_clear(GdkEventKey *event)
{
    if((event->keyval >= 0x20 && event->keyval <= 0xff) 
		 && event->state & (GDK_CONTROL_MASK|GDK_MOD1_MASK)) {
		return TRUE;
    }

    if(event->keyval == GDK_Delete || event->keyval == GDK_Home ||
       event->keyval == GDK_Clear || event->keyval == GDK_Left ||
       event->keyval == GDK_Right || event->keyval == GDK_Insert ||
       event->keyval == GDK_Up    || event->keyval == GDK_Down ||
       event->keyval == GDK_Page_Up || event->keyval == GDK_Page_Down) {
	    return TRUE;
    }
    return FALSE;
}

void
ami_hangul_state_init(HangulState *han)
{
    g_return_if_fail (han != NULL);
    han->f = F_NULL;
    han->m = M_NULL;
    han->l = L_NULL;
    han->sp = 0;
    han->last_l = -1;
    han->last_ll = -1;
}

void
ami_hangul_ic_change(IC *ic)
{
    static IC *current_ic = NULL;
    g_return_if_fail (ic != NULL);
    if (current_ic == ic) return;
    ami_hangul_state_put(ic->han);
    current_ic = ic;
}

void
ami_hangul_state_put(HangulState *han)
{
    g_return_if_fail (han != NULL);
    stack = han->stack;
    f = han->f;
    m = han->m;
    l = han->l;
    sp = han->sp;
    last_l = han->last_l;
    last_ll = han->last_ll;
}

void
ami_hangul_state_get(HangulState *han)
{
    g_return_if_fail (han != NULL);
    han->f = f;
    han->m = m;
    han->l = l;
    han->sp = sp;
    han->last_l = last_l;
    han->last_ll = last_ll;
}
#endif

void
ami_hangul_check_bufsize(HangulState *han, int inc)
{
    /* hack for edit_flush_with_c */
    inc++;
    if (han->buf_size > (han->len + inc)) return;
    while (han->buf_size <= han->len + inc) han->buf_size *= 2;
    han->buf = g_realloc(han->buf, han->buf_size);
}
