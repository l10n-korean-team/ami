#ifndef __CB_H
#define __CB_H
#include "amitype.h"

void cb_status_draw(IC *ic);
void cb_edit_insert(IC *ic, char *buf);
void cb_edit_backward_delete(IC *ic);
void cb_edit_correct_insert(IC *ic, char *buf);
void cb_edit_correct(IC *ic, char *buf);
void cb_edit_reset(IC *ic);
void cb_edit_toggle(IC *ic);
void cb_edit_move(IC *ic);
void cb_edit_clear_temp(IC *ic);
void cb_edit_hanja_replace(IC *ic, const char *hangul, const char *hanja);

#endif
