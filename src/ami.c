/* This file is a part of Ami software, (C) Hwang chi-deok 1999, 2001 */

#include "config.h"

#include <stdio.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include <gdk/gdkkeysyms.h>
#include <Ximd/IMdkit.h>
#include <Ximd/Xi18n.h>
#include <sys/stat.h>
#include <unistd.h>
#include <langinfo.h>

#include "ami.h"
#include "applet.h"
#include "cp.h"
#include "handler.h"
#include "util.h"
#include "keyparse.h"
#include "conf.h"
#include "comp.h"

static void print_help(void);


/* flags for debugging */
gboolean use_trigger = TRUE;	/* Dynamic Event Flow is default */
gboolean use_offkey = FALSE;	/* Register OFF Key for Dynamic Event Flow */
int debug = 0;
long filter_mask = KeyPressMask;
GdkFont	*default_xim_font = NULL;
GdkBitmap *dim_mask;
int dock_style = WM_DETECT;

XIMS xims;
char *hangul_mode_label = NULL;
char *english_mode_label = NULL;

int ami_codeset;

#define DEFAULT_IMNAME "Ami"
#define DEFAULT_LOCALE "ko"
#define DEFAULT_FONT1_EUC      "lucidasans-14,-*-*-medium-r-normal--*-*-*-*-c-160-ksc5601.1987-0"
#define DEFAULT_FONT2_EUC      "*,-*-*-medium-r-normal--16-*-*-*-c-*-*"
#define DEFAULT_FONT1_UTF8     "-*-*-medium-r-normal-ko-16-*-*-*-c-*-iso10646-1,-*-*-medium-r-normal--16-*-*-*-c-*-*-*"
#define DEFAULT_FONT2_UTF8     "-*-*-medium-r-normal-ko-18-*-*-*-c-*-iso10646-1,-*-*-medium-r-normal--18-*-*-*-*-*-iso10646-1" 


char *imname = DEFAULT_IMNAME;
char *default_fontname = NULL;
char *hanja_dic_filename = NULL;

GtkWidget *cp_win = NULL, *cp = NULL;

gint ami_run_mode = AMI_NORMAL_RUN;
int ami_editing_mode = AMI_EDITING_WORD_MODE;
int bell_intensity = 0;

int num_fonts_in_utf8_fs = 7;

gboolean ami_line_wraping_mode = FALSE;
gboolean ami_use_underline = TRUE;
gboolean esc_han_toggle = FALSE;
gboolean unique_han = FALSE;
gboolean support_status = TRUE;
gboolean support_cb = TRUE;
gboolean wait_wm = FALSE;
gboolean allow_shuffle = FALSE;

int ami_hanja_def_subst_mode = 1;
int ami_hanja_subst_mode = 0;
char * ami_hanja_subst_format = NULL;

char *pix_dir_name = NULL;
char *keyboard_name = NULL;


/* Supported Inputstyles */
static XIMStyle Styles[] = {
    XIMPreeditCallbacks|XIMStatusCallbacks,
    XIMPreeditCallbacks|XIMStatusArea,
    XIMPreeditCallbacks|XIMStatusNothing,
    XIMPreeditCallbacks|XIMStatusNone,
    XIMPreeditPosition|XIMStatusCallbacks,
    XIMPreeditPosition|XIMStatusArea,
    XIMPreeditPosition|XIMStatusNothing,
    XIMPreeditPosition|XIMStatusNone,
    XIMPreeditArea|XIMStatusArea,
    XIMPreeditArea|XIMStatusCallbacks,
    XIMPreeditArea|XIMStatusNothing,
    XIMPreeditArea|XIMStatusNone,
    XIMPreeditNothing|XIMStatusNothing,
    XIMPreeditNone|XIMStatusNone,
    0
};

/* Supported Inputstyles */
static XIMStyle Styles_nocb[] = {
    XIMPreeditPosition|XIMStatusArea,
    XIMPreeditPosition|XIMStatusNothing,
    XIMPreeditPosition|XIMStatusNone,
    XIMPreeditArea|XIMStatusArea,
    XIMPreeditArea|XIMStatusNothing,
    XIMPreeditArea|XIMStatusNone,
    XIMPreeditNothing|XIMStatusNothing,
    XIMPreeditNone|XIMStatusNone,
    0
};

static XIMStyle Styles_nostatus[] = {
    XIMPreeditCallbacks|XIMStatusNone,
    XIMPreeditCallbacks|XIMStatusNothing,
    XIMPreeditPosition|XIMStatusNothing,
    XIMPreeditPosition|XIMStatusNone,
    XIMPreeditArea|XIMStatusNone,
    XIMPreeditArea|XIMStatusNothing,
    XIMPreeditNothing|XIMStatusNothing,
    XIMPreeditNone|XIMStatusNone,
    0
};

/* struct for trigger keys used to open IM */
XIMTriggerKeys *on_keys = NULL;

/* structs mainly used in hanlder_match_keys[2] functions */
XIMTriggerKey *trigger_keys = NULL;
XIMTriggerKey *hanja_keys = NULL;
XIMTriggerKey *special_char_keys = NULL;
XIMTriggerKey *flush_keys = NULL;

/* Supported Korean Encodings */
static XIMEncoding koEncodings[] = {
    "COMPOUND_TEXT",
    NULL
};

static char *locale=NULL;

int
proto_handler(XIMS ims, IMProtocol *call_data)
{
    switch (call_data->major_code) {
      case XIM_OPEN:
	return handler_open(ims, (IMOpenStruct *)call_data);
	break;
      case XIM_CLOSE:
	return handler_close(ims, (IMCloseStruct *)call_data);
	break;
      case XIM_CREATE_IC:
	return handler_ic_create(ims, (IMChangeICStruct *)call_data);
	break;
      case XIM_DESTROY_IC:
	return handler_ic_destroy(ims, (IMChangeICStruct *)call_data);
	break;
      case XIM_SET_IC_VALUES:
	return handler_ic_set(ims, (IMChangeICStruct *)call_data);
	break;
      case XIM_GET_IC_VALUES:
	return handler_ic_get(ims, (IMChangeICStruct *)call_data);
	break;
      case XIM_FORWARD_EVENT:
	return handler_forward_event(ims, (IMForwardEventStruct *)call_data);
	break;
      case XIM_SET_IC_FOCUS:
	return handler_ic_focus_set(ims, (IMChangeFocusStruct *)call_data);
	break;
      case XIM_UNSET_IC_FOCUS:
	return handler_ic_focus_unset(ims, (IMChangeFocusStruct *)call_data);
	break;
      case XIM_RESET_IC:
	return handler_ic_reset(ims, (IMResetICStruct *)call_data);
	break;
      case XIM_TRIGGER_NOTIFY:
	return handler_trigger_notify(ims, (IMTriggerNotifyStruct *)call_data);
	break;
      case XIM_PREEDIT_START_REPLY:
	return handler_preedit_start_reply(ims, (IMPreeditCBStruct *)call_data);
      case XIM_PREEDIT_CARET_REPLY:
	return handler_preedit_caret_reply(ims, (IMPreeditCBStruct *)call_data);
      case XIM_STR_CONVERSION_REPLY:
	return handler_str_conversion_reply(ims, (IMStrConvCBStruct *)call_data);
      default:
	if (debug) g_print("%d un-processed\n", call_data->major_code);
    }
    return FALSE;
}

int
ami_init_im(GtkWidget* win)
{
    XIMStyles *input_styles, *styles2;
    XIMTriggerKeys *trigger2;
    XIMEncodings *encodings, *encoding2;
    int i;


    if ((input_styles = (XIMStyles *)malloc(sizeof(XIMStyles))) == NULL) {
	fprintf(stderr, "Can't allocate\n");
	exit(1);
    }
    if (!support_cb) {
	if (debug) printf("callback is disabled\n");
	input_styles->count_styles = sizeof(Styles_nocb)/sizeof(Styles_nocb[0]) - 1;
	input_styles->supported_styles = Styles_nocb;
    } else if (unique_han || !support_status) {
	if (debug) printf("no status area\n");
	input_styles->count_styles = sizeof(Styles_nostatus)/sizeof(Styles_nostatus[0]) - 1;
	input_styles->supported_styles = Styles_nostatus;
    } else {
	input_styles->count_styles = sizeof(Styles)/sizeof(Styles[0]) - 1;
	input_styles->supported_styles = Styles;
    }

    if ((encodings = (XIMEncodings *)malloc(sizeof(XIMEncodings))) == NULL) {
	fprintf(stderr, "Can't allocate\n");
	exit(1);
    }
    encodings->count_encodings = sizeof(koEncodings)/sizeof(XIMEncoding) - 1;
    encodings->supported_encodings = koEncodings;


    xims = IMOpenIM(GDK_DISPLAY(),
		   IMModifiers, "Xi18n",
		   IMServerWindow, GDK_WINDOW_XWINDOW(win->window),
		   IMServerName, imname,
		   IMLocale, DEFAULT_LOCALE,
		   IMServerTransport, "X/",
		   IMInputStyles, input_styles,
		   NULL);
    if (xims == (XIMS)NULL) {
	g_warning(_("Can't Open Input Method Service:\n"
	         "\tInput Method Name :%s\n") , imname);
	g_print(_("***Please check whether another ami is running\n\n"));
	exit(1);
    }
    if (use_trigger) {
	if (use_offkey)
	  IMSetIMValues(xims,
			IMOnKeysList, on_keys,
			IMOffKeysList, on_keys,
			NULL);
	else
	  IMSetIMValues(xims,
			IMOnKeysList, on_keys,
			NULL);
    }
    IMSetIMValues(xims,
		  IMEncodingList, encodings,
		  IMProtocolHandler, proto_handler,
		  IMFilterEventMask, filter_mask,
		  NULL);
    IMGetIMValues(xims,
		  IMInputStyles, &styles2,
		  IMOnKeysList, &trigger2,
		  IMOffKeysList, &trigger2,
		  IMEncodingList, &encoding2,
		  NULL);
    {
	char dim_data[] = {0x02, 0x01};
	dim_mask = gdk_bitmap_create_from_data(NULL, dim_data, 2, 2);
    }
    if (default_fontname && *default_fontname)
	default_xim_font = util_fontset_load(default_fontname);
    if (ami_codeset == AMI_EUC ) 
    {
  	if (!default_xim_font)
	    default_xim_font = util_fontset_load(DEFAULT_FONT1_EUC);
    	if (!default_xim_font)
	    default_xim_font = util_fontset_load(DEFAULT_FONT2_EUC);
    }
    else
    {
    	if (!default_xim_font)
	    default_xim_font = util_fontset_load(DEFAULT_FONT1_UTF8);
    	if (!default_xim_font)
	    default_xim_font = util_fontset_load(DEFAULT_FONT2_UTF8);
    }

    if (!default_xim_font)
	g_error("Cannot set XIM default font\n");

    return 0;
}

int
ami_main(int argc, char *argv[], char *rc_file)
{
    XWMHints *wm_hints;

    gtk_init(&argc, &argv);
    gtk_rc_parse(rc_file);

    cp_win = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(cp_win), _("Ami Preference"));

    gtk_widget_realize(cp_win);
    wm_hints = XAllocWMHints();
    wm_hints->flags = StateHint;
    wm_hints->initial_state = IconicState;
    XSetWMHints(gdk_display, GDK_WINDOW_XWINDOW(cp_win->window), wm_hints);
    XFree(wm_hints);

    gtk_container_set_border_width(GTK_CONTAINER(cp_win), 6);
    gtk_signal_connect(GTK_OBJECT(cp_win), "destroy", (GtkSignalFunc)cp_quit, NULL);

    cp = create_cp();
    gtk_container_add(GTK_CONTAINER(cp_win), cp);

    gtk_widget_show_all(cp_win);

    ami_init_im(cp_win);

    XBell(gdk_display, bell_intensity);
    gtk_main();

    return 0;
}

static void
ami_exit_func(void)
{
    if (xims) IMCloseIM(xims);
}

#ifdef DEBUG_WITH_GDB
#include <signal.h>
static void
caughtSignal(int sig)
{
#define ____NAME "ami"
#define ____PATH "/usr/bin/ami"
#define ____TEMP "/tmp/.pcgdb.XXXXXX"

    fprintf(stderr, "%s: caught signal %d\n", ____NAME, sig);

    if (1)
    {
	char tmpFileName[] = ____TEMP;
	char cmd[256];
	FILE *fp;

	mkstemp(tmpFileName);
	if ((fp = fopen(tmpFileName, "w")) != NULL)
	{
	    fprintf(fp,
		    "file %s\n"
		    "attach %d\n"
		    "set height 1000\n"
		    "bt full\n"
		    , ____PATH, getpid());
	    fclose(fp);
	    snprintf(cmd, sizeof(cmd),
		     "hanterm -sb -sl 1024 -e gdb -x %s", tmpFileName);
	    system(cmd);
	    unlink(tmpFileName);
	}
    }

    exit(1);
}
#endif

int
main(int argc, char *argv[])
{
    char *rc_file;
    gint i;
    char *codeset;

#ifdef HAVE_GETTEXT
    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE, LOCALEDIR);
    textdomain(PACKAGE);
#endif

#ifdef DEBUG_WITH_GDB
    signal(SIGSEGV, caughtSignal);
#endif

    ami_config_init();

    if (strstr(argv[0], "wmami")) ami_run_mode = AMI_WM_RUN;
#ifdef GNOME_APPLET
    if (strstr(argv[0], "ami_applet")) ami_run_mode = AMI_GNOME_RUN;
#endif
    for (i = 1; i < argc; i++) {
	if (!strcmp(argv[i], "-wm")) {
	    ami_run_mode = AMI_WM_RUN;
	} else if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
	    print_help();
	} else if (!strcmp(argv[i], "-debug")) {
	    debug = TRUE;
	} else if (!strcmp(argv[i], "-nocb")) {
	    support_cb = FALSE;
	} else if (!strcmp(argv[i], "-wait")) {
	    wait_wm = TRUE;
	} else if (!strcmp(argv[i], "-force") && i < argc - 1) {
	    i++;
	    if (!strcmp(argv[i], "kde1")) {
		dock_style = WM_KDE1;
	    } else if (!strcmp(argv[i], "kde2")) {
		dock_style = WM_KDE2;
	    } else if (!strcmp(argv[i], "wmaker")) {
		dock_style = WM_W;
	    } else if (!strcmp(argv[i], "e")) {
		dock_style = WM_E;
	    }
	}
#ifdef GNOME_APPLET
	else if (!strcmp(argv[i], "-gnome")) {
	    ami_run_mode = AMI_GNOME_RUN;
	}
#endif
    }

    rc_file = g_strconcat(g_get_home_dir(), "/.ami/gtkrc", NULL);
    if (access(rc_file, R_OK) != 0) {
	/* user rc file does not exist. So copy one from system defaults*/
	int orig = open(SYS_RCFILE, O_RDONLY);
	if (orig >= 0) {
	    char buf[256];
	    int size;
	    int out = open(rc_file, O_WRONLY);
	    while((size = read(orig, buf, 256)) > 0) {
		write(out, buf, size);
	    }
	    close(orig);
	    close(out);
	}
    }

    locale = gtk_set_locale();
    if (!strcmp(locale, "C")) {
	char *lang = g_getenv("LC_ALL");
	if (!lang) lang = g_getenv("LC_CTYPE");
	if (!lang) lang = g_getenv("LANG");
	if (!lang) {
	    g_print(_("The environment variable LANG is not properly specified.\n"
		      "Set it to one of Korean locales such as ko_KR.eucKR and\n"
		      "ko_KR.UTF-8 and relaunch Ami\n"));
	} else if (strcmp(lang, "C") == 0 || strcmp(lang, "posix") == 0) {
	    g_print(_("The environment variable LANG is set to C.\n"
		      "Set it to one of Korean locales such as ko_KR.eucKR\n"
		      "and ko_KR.UTF-8 and relaunch Ami\n"));
	} else if (strstr(lang, "ko")) {
	    g_print(_("The current locale does not appear to  support Korean.\n"
		      "Check your system configuration.\n"));
	}
	exit(1);
    }

    codeset=nl_langinfo(CODESET);
    if ( ! strcasecmp(codeset,"EUC-KR") ) 
       ami_codeset=AMI_EUC;
    else if ( ! strcasecmp(codeset,"UTF-8") )
       ami_codeset=AMI_UTF8;
    else {
       g_print(_("The locale codeset is %s. It must be either "
               "EUC-KR or UTF-8.\n Check your locale.\n"),
               codeset);
       exit(1); 
    }

    g_atexit (ami_exit_func);
    switch(ami_run_mode) {
	case AMI_WM_RUN:
	    ami_wm_main(argc, argv, rc_file);
	    break;
#ifdef GNOME_APPLET
	case AMI_GNOME_RUN:
	    ami_gnome_main(argc, argv, rc_file);
	    break;
#endif
	case AMI_NORMAL_RUN:
	default:
	    ami_main(argc, argv, rc_file);
    }

    exit(0);
}

static void
print_help(void)
{
    g_print(_("Korean Input Server Ami %s\n"), VERSION);
    g_print(_("ami -wm or wmami: run Ami in the docking mode of Windowmaker or E.\n"));
#ifdef GNOME_APPLET
    g_print(_("ami -gnome or ami_applet: run Ami as a gnome panel applet.\n"));
#endif
    exit(0);
}

