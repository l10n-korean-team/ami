#ifndef __AMI_H
#define __AMI_H

#include "amitype.h"

#ifdef ENABLE_NLS
#include <libintl.h>
#include <locale.h>
#define _(String) gettext(String)
#else
#define _(String) String
#endif
#define N_(String) String

#include <libintl.h>

/* 
 * For maximum portability, consider using config.charset in configure script.
 * iconv(3) in Linux glibc2, libiconv and Solaris 7 or later use
 * (see http://docs.sun.com/db/doc/805-4123/6j3tmpc71?a=view)
 * EUC-KR for EUC-KR. Use "eucKR" for HP/UX and Compq Tru64
 * (ref. http://devrsrc1.external.hp.com/STK/partner/relnotes/10.20/chp7.html
 *       http://www.tru64unix.compaq.com/docs/base_doc/DOCUMENTATION/V51_HTML/SUPPDOCS/KOREADOC/KOREACH2.HTM)
 * "IBM-eucKR" for AIX
 * (ref. http://publib16.boulder.ibm.com/pseries/en_US/aixprggd/nlsgdrf/codeset_over.htm)
 */
#define EUCKR_ICD  "EUC-KR"

extern gboolean use_trigger, use_offkey, use_tcp, use_local;
extern XIMTriggerKey *trigger_keys;
extern XIMTriggerKeys *on_keys;
extern XIMTriggerKey *hanja_keys;
extern XIMTriggerKey *special_char_keys;
extern XIMTriggerKey *flush_keys;
extern XIMTriggerKey *forward_keys;
extern char *default_fontname;
extern char *hanja_dic_filename;


extern XIMS	xims;
extern char *hangul_mode_label;
extern char *english_mode_label;
extern long filter_mask;
extern GdkFont	*default_xim_font;
extern GtkWidget *cp, *cp_win;
extern GdkPixmap *dim_mask;
extern int debug;
extern int ami_hanja_def_subst_mode;
extern int ami_hanja_subst_mode;
extern char *ami_hanja_subst_format;
extern int num_fonts_in_utf8_fs;

extern int ami_codeset;

extern int ami_init_im(GtkWidget *widget);
extern int ami_run_mode;
extern int ami_editing_mode;
extern int bell_intensity;

extern gboolean ami_line_wraping_mode;
extern gboolean ami_use_underline;
extern gboolean esc_han_toggle;
extern gboolean unique_han;
extern gboolean support_status;
extern gboolean wait_wm;
extern gboolean allow_shuffle;
extern char *pix_dir_name;
extern char *keyboard_name;
extern int dock_style;

extern Composer *composer;
#endif
