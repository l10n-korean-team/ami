/* this file is a part of Ami software, (C) Hwang chi-deok 1999 */

#include "config.h"

#include "amitype.h"
#include "ami.h"
#include <gdk/gdkx.h>
#include "utf8.h"

static GHashTable *font_table = NULL;
static Window find_top_window (Window win);

GdkFont *
util_fontset_load(char *name)
{
    char **font_names;
    XFontStruct **font_structs;
    int num_fonts = 0;
    GdkFont *font, *f;
    GString *s;
    int i;
    char *xlocale;

    if (!font_table) {
	font_table = g_hash_table_new(g_str_hash, g_str_equal);
    }
    font = g_hash_table_lookup(font_table, name);
    if (font) {
	gdk_font_ref(font);
	return font;
    }
    font = gdk_fontset_load(name);
    if (font) {
#if 0
	XFontSet fs=GDK_FONT_XFONT(font);
	num_fonts = XFontsOfFontSet(fs, &font_structs, &font_names);
	xlocale = XLocaleOfFontSet(fs);
	if (debug) g_print("%s: X locale of fontset = %s\n",
	    __FUNCTION__,xlocale);
#endif
	num_fonts = XFontsOfFontSet(GDK_FONT_XFONT(font), &font_structs,
				    &font_names);
    }

/* 
   FIXME : This is really a kludge. The number of fonts
   in a given font set *cannot* be known in advance. That's certainly 
   the case for locales with the codeset UTF-8 although it can also 
   be the case for other locales. XCreateFontSet() can be used
   to get info. on missing character sets, but gdk_fontset_load()
   and GdkFont are too opaque and I don't know how to extract
   that info. To avoid hard-coding this number,
   I ended up making a configuration option to specify that. 
   See conf.c.  J. Shin

   Another problem arises when a client is running under a different
   locale than the locale under which Ami is running. For instance,
   when Ami is running in UTF-8 locale and a client is running under
   EUC-KR locale, the test below fails because ami_codeset
   has nothing to do with the codeset of the locale under which
   a client is running and dw_set_font() invokes this function
	 with a fontset for the locale under which a client is running.

    if (num_fonts != (ami_codeset==AMI_EUC ?  2 : num_fonts_in_utf8_fs) ) {


    if (num_fonts != ((strstr(xlocale,"UTF") || strstr(xlocale,"utf")) ?
        num_fonts_in_utf8_fs : 2 ) ) {
*/
    if (num_fonts != (ami_codeset==AMI_EUC ?  2 : num_fonts_in_utf8_fs) ) {
	g_warning("fontset(%s) loading failed(%d)", name, num_fonts);
	if (default_xim_font) gdk_font_ref(default_xim_font);
	return default_xim_font;
    }
    s = g_string_new(NULL);
    for (i=0;i<num_fonts;i++) {
	s = g_string_append(s, font_names[i]);
	if (i != num_fonts - 1) g_string_append_c(s, ',');
    }
    if (strcmp(name, s->str) == 0) {
	g_hash_table_insert(font_table, g_strdup(name), font);
	g_string_free(s, TRUE);
	return font;
    }
    f = g_hash_table_lookup(font_table, s->str);
    if (f) {
	gdk_font_unref(font);
	gdk_font_ref(f);
	g_hash_table_insert(font_table, g_strdup(name), f);
	g_string_free(s, FALSE);
	return f;
    }
    g_hash_table_insert(font_table, g_strdup(name), font);
    g_hash_table_insert(font_table, s->str, font);
    g_string_free(s, FALSE);
    return font;
}

static gboolean
remove_font(gpointer key, gpointer value, gpointer user_data)
{
    if (value == user_data) {
	g_free(key);
	return TRUE;
    } else
        return FALSE;
}

int
util_font_real_unref(GdkFont *font)
{
    GdkFontPrivate *private = (GdkFontPrivate *)font;
    if (private->ref_count == 1) {
	g_hash_table_foreach_remove(font_table, remove_font, font);
    }
    gdk_font_unref(font);
    return FALSE;
}

void
util_font_unref(GdkFont *font)
{
    GdkFontPrivate *private = (GdkFontPrivate *)font;
    if (private->ref_count == 1) {
	gtk_timeout_add(1000*60*2, (GtkFunction)util_font_real_unref, font);
    } else {
	gdk_font_unref(font);
    }
}

GdkWindow *
util_get_top_window (GdkWindow *win)
{
    Window xwin;
    GdkWindow *top;
    g_return_val_if_fail (win != NULL, NULL);
    if ((xwin = find_top_window(GDK_WINDOW_XWINDOW(win))) == None)
	return NULL;
    top = gdk_window_lookup(xwin);
    if (!top) top = gdk_window_foreign_new(xwin);
    else gdk_window_ref(top);
    return top;
}

GString 
* util_xlate_hanja_fmts()
{
    gchar *fmts_p = ami_hanja_subst_format;
    GString *fmts_xlat = g_string_new(NULL);

    while (*fmts_p) {
	if (strncmp(fmts_p, "�ѱ�", 4) == 0) {
	    fmts_p += 4;
	    g_string_append(fmts_xlat, _("HANGUL"));
	} else if (strncmp(fmts_p, "����", 4) == 0) {
	    fmts_p += 4;
	    g_string_append(fmts_xlat, _("HANJA"));
	} else {
	    g_string_append_c(fmts_xlat, *fmts_p++);
	}
    }

    return fmts_xlat;
}

static Window try_parent (Window win, Atom WM_STATE);

static Window
find_top_window (Window win)
{
    Atom WM_STATE;
    Atom type = None;
    int format;
    unsigned long nitems, after;
    unsigned char *data;
    Window inf;
    gboolean result;

    WM_STATE = gdk_atom_intern("WM_STATE", TRUE);
    if (!WM_STATE)
	return win;
    gdk_error_trap_push();
    result = XGetWindowProperty(GDK_DISPLAY(), win, WM_STATE, 0, 0, False,
				AnyPropertyType, &type, &format, &nitems,
				&after, &data);
    if (gdk_error_trap_pop() || !result)
	return None;
    if (type)
	return win;
    inf = try_parent(win, WM_STATE);
    if (!inf)
	inf = win;
    return inf;
}

static Window
try_parent (Window win, Atom WM_STATE)
{
    Window root, parent;
    Window *children = NULL;
    unsigned int nchildren;
    unsigned int i;
    Atom type = None;
    int format;
    unsigned long nitems, after;
    unsigned char *data;
    Window inf = 0;

    if (!XQueryTree(GDK_DISPLAY(), win, &root, &parent, &children, &nchildren))
	return 0;
    if (children) XFree((char *)children);

    if (root == parent) return win;
    XGetWindowProperty(GDK_DISPLAY(), parent, WM_STATE, 0, 0, False,
		       AnyPropertyType, &type, &format, &nitems,
		       &after, &data);
    if (type) return parent;
    return try_parent(parent, WM_STATE);
}

static GSList *gc_list = NULL;
static int
do_real_gc(void)
{
    GSList *list = gc_list;
    while(list) {
	free(list->data);
	list = list->next;
    }
    g_slist_free(gc_list);
    gc_list = NULL;
    return FALSE;
}

void
do_gc(char *buf)
{
    return;
    if (!gc_list) {
	gtk_timeout_add(1000, (GtkFunction)do_real_gc, NULL);
    }
    gc_list = g_slist_prepend(gc_list, buf);
}

/* count characters in mbstr as opposed to bytes */
int
util_get_mb_strlen(const char *s, int len)
{
    int i, mblen = 0;
    g_assert(s);
    if (len < 0) len = strlen(s);
    for (i=0;i<len;i++) {
	if (s[i] & 0x80) i+= MBC_LEN(s[i])-1;
	mblen++;
    }
    return mblen;
}
