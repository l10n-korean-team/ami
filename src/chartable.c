
/* this file is a part of gau software, (C) Hwang chi-deok 1997, 1998         */

#include "config.h"
#include <gtk/gtkwidget.h>
#include <gtk/gtksignal.h>
#include <gdk/gdkkeysyms.h>
#include <stdio.h>
#include <string.h>

#include "chartable.h"
#include "ami.h"
#include "util.h"

#define PAD 8

static void char_table_class_init    (CharTableClass *klass);
static void char_table_init          (CharTable      *darea);
static void char_table_realize       (GtkWidget           *widget);
static void char_table_finalize      (GtkObject *object);
static void char_table_size_allocate (GtkWidget           *widget,
					    GtkAllocation       *allocation);
static void char_table_size_request (GtkWidget           *widget,
				      GtkRequisition *requisition);
static gint char_table_expose          (GtkWidget      *widget,
				      GdkEventExpose *event);
static gint char_table_button_press          (GtkWidget      *widget,
				      GdkEventButton *event);
static gint char_table_key_press	(GtkWidget      *widget,
				      GdkEventKey *event);
static void char_table_item_select(CharTable *table);
static void char_table_item_unselect(CharTable *table);
static void char_table_item_draw(CharTable *table, int x, int y, gboolean selected);

static GtkWidgetClass *parent_class = NULL;

enum {
	SELECTED,
	MOVED,
	LAST_SIGNAL
};

static gint char_table_signals[LAST_SIGNAL] = { 0 };

GtkType
char_table_get_type ()
{
  static GtkType char_table_type = 0;

  if (!char_table_type)
    {
      GtkTypeInfo char_table_info =
      {
	"CharTable",
	sizeof (CharTable),
	sizeof (CharTableClass),
	(GtkClassInitFunc) char_table_class_init,
	(GtkObjectInitFunc) char_table_init,
	NULL, NULL, NULL,
      };

      char_table_type = gtk_type_unique (GTK_TYPE_WIDGET, &char_table_info);
    }

  return char_table_type;
}

static void
char_table_class_init (CharTableClass *class)
{
    GtkWidgetClass *widget_class;
    GtkObjectClass *object_class;

    object_class = (GtkObjectClass*) class;
    widget_class = (GtkWidgetClass*) class;

    char_table_signals[SELECTED] = gtk_signal_new("selected",
	    GTK_RUN_LAST,
	    object_class->type,
	    GTK_SIGNAL_OFFSET (CharTableClass, selected),
	    gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
	    GTK_TYPE_POINTER);
    char_table_signals[MOVED] = gtk_signal_new("moved",
	    GTK_RUN_LAST,
	    object_class->type,
	    GTK_SIGNAL_OFFSET (CharTableClass, moved),
	    gtk_marshal_NONE__INT_INT, GTK_TYPE_NONE, 2,
	    GTK_TYPE_INT, GTK_TYPE_INT);
    gtk_object_class_add_signals (object_class, char_table_signals, LAST_SIGNAL);
    parent_class = gtk_type_class (GTK_TYPE_WIDGET);
    object_class->finalize = char_table_finalize;
    widget_class->realize = char_table_realize;
    widget_class->size_allocate = char_table_size_allocate;
    widget_class->size_request = char_table_size_request;
    widget_class->expose_event = char_table_expose;
    widget_class->button_press_event = char_table_button_press;
    widget_class->key_press_event = char_table_key_press;
}

static void
char_table_init (CharTable *chartable)
{
    GTK_WIDGET_SET_FLAGS(chartable, GTK_CAN_FOCUS);
    chartable->current_x = 0;
    chartable->current_y = 0;
}


GtkWidget *
char_table_new(char **buf, int col, int total_len)
{
    CharTable *table;

    g_return_val_if_fail(buf != NULL, NULL);
    g_return_val_if_fail(col > 0, NULL);
    g_return_val_if_fail(total_len > 0, NULL);

    table = gtk_type_new(char_table_get_type());
    table->col = col;
    table->row = (total_len + col - 1) / col;
    table->total_len = total_len;
    table->buf = buf;
#if 0
    {
	int i;
	for(i=0;i<table->total_len;i++) {
	    if(table->buf[i]) if (debug) g_print("%d:%s\n", i, table->buf[i]);
	}
    }
#endif
    return GTK_WIDGET(table);
}

char *
char_table_get_char(CharTable *table)
{
    g_return_val_if_fail(table != NULL, NULL);
    g_return_val_if_fail(IS_CHAR_TABLE(table), NULL);

    if(table->current_x < 0) return NULL;
    return table->buf[table->current_x+table->col*table->current_y];
}

static void
char_table_realize (GtkWidget *widget)
{
    CharTable *table;
    GdkWindowAttr attributes;
    gint attributes_mask;
    GdkFont *font;
    g_return_if_fail (widget != NULL);

    table = CHAR_TABLE(widget);
    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x = widget->allocation.x;
    attributes.y = widget->allocation.y;
    attributes.width = widget->allocation.width;
    attributes.height = widget->allocation.height;
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes.visual = gtk_widget_get_visual (widget);
    attributes.colormap = gtk_widget_get_colormap (widget);
    attributes.event_mask = gtk_widget_get_events (widget);
    attributes.event_mask |= (GDK_EXPOSURE_MASK |
			      GDK_BUTTON_PRESS_MASK |
			      GDK_KEY_PRESS_MASK);

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

    widget->window = gdk_window_new (gtk_widget_get_parent_window(widget), &attributes, attributes_mask);
    gdk_window_set_user_data (widget->window, table);
    widget->style = gtk_style_attach (widget->style, widget->window);
    gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
    font = GTK_WIDGET(table)->style->font;
	if (ami_codeset==AMI_EUC)   /* Hangul syllable 'GA' in EUC-KR */
	    table->per_width = gdk_string_width(font, "\xb0\xa1") + PAD * 2;
	else           /* Hangul syllable 'GA' in UTF-8 */
	    table->per_width = gdk_string_width(font, "\xea\xb0\x80") + PAD * 2;
    table->per_height = font->ascent + font->descent + PAD * 2;
    gtk_widget_queue_resize(widget);
}

int
char_table_get_position1(CharTable *table, int x, int y, int *ret_x, int *ret_y)
{
    int i;
    i = y * table->col + x;
    if (table->buf[i] == NULL) {
	int len;
	while (table->buf[i] == NULL && i != 0) i--;
	if (table->buf[i] == NULL) { /* probably this should not happen but it does */
		if (ret_x) *ret_x = 0;
		if (ret_y) *ret_y = 0;
		return 0;
	}
	len = util_get_mb_strlen(table->buf[i],-1);
	if (len + i < y*table->col + x) return 0; /* invalid position */
	if (ret_x) *ret_x = i % table->col;
	if (ret_y) *ret_y = i / table->col;
    } else {
	if (ret_x) *ret_x = x;
	if (ret_y) *ret_y = y;
    }
    return 1;
}

int
char_table_get_position(CharTable *table, int x, int y, int *ret_x, int *ret_y)
{
    int col = x / table->per_width;
    int row = y / table->per_height;

    if (col < 0) col = 0;
    if (row < 0) row = 0;
    if (col > table->col) col = table->col;
    if (row > table->row) row = table->row;
    return char_table_get_position1(table, col, row, ret_x, ret_y);
}

static void
char_table_size_allocate (GtkWidget     *widget,
				GtkAllocation *allocation)
{
    g_return_if_fail (widget != NULL);
    widget->allocation = *allocation;

    if (GTK_WIDGET_REALIZED (widget)) {
	gdk_window_move_resize (widget->window,
				allocation->x, allocation->y,
				allocation->width, allocation->height);
    }
}

static gint
char_table_expose(GtkWidget *w, GdkEventExpose *ev)
{
    int x1, x2;
    int y1, y2;
    int i, j;
    CharTable *table;

    g_return_val_if_fail(w != NULL, FALSE);

    table = CHAR_TABLE(w);
    x1 = ev->area.x / table->per_width;
    x2 = (ev->area.x + ev->area.width) / table->per_width;
    y1 = ev->area.y / table->per_height;
    y2 = (ev->area.y + ev->area.height) / table->per_height;
    if(y2 >= table->row) y2 = table->row - 1;
    if(x2 >= table->col) x2 = table->col - 1;
    for(j=y1; j<= y2; j++) {
	i = x1;
	if (!char_table_get_position1(table, i, j, &i, NULL)) continue;
	while (i <= x2) {
	    if (table->buf[table->col*j + i] == NULL) {
		i++;
		continue;
	    }
	    if (table->col*j+i >= table->total_len)
		break;
	    if(i == table->current_x && j == table->current_y) {
		    char_table_item_select(table);
	    } else
		    char_table_item_draw(table, i, j, FALSE);
	    i += util_get_mb_strlen(table->buf[table->col*j + i],-1);
	}
    }
    return FALSE;
}

static gint
char_table_button_press(GtkWidget *w, GdkEventButton *ev)
{
    CharTable *table;
    int x, y;

    g_return_val_if_fail(w != NULL, FALSE);

    table = CHAR_TABLE(w);

    if (!GTK_WIDGET_HAS_FOCUS (w))
	gtk_widget_grab_focus (w);
    if (!char_table_get_position(table, ev->x, ev->y, &x, &y)) return FALSE;
    if(table->current_x != x || table->current_y != y) {
	char_table_item_unselect(table);
	table->current_x = x;
	table->current_y = y;
	char_table_item_select(table);
	if(ev->type == GDK_2BUTTON_PRESS || ev->button == 2) {
	    char_table_selected(table);
	}
    } else {
	char_table_selected(table);
    }
    return TRUE;
}

static gint
char_table_key_press(GtkWidget *w, GdkEventKey *ev)
{
    int x, y;
    int i;
    CharTable *table;
    g_return_val_if_fail( w != NULL, FALSE);

    table = CHAR_TABLE(w);
    x = table->current_x;
    y = table->current_y;
    i = x + y*table->col;
    if(ev->keyval == GDK_Left || ev->keyval == GDK_h) {
	i--;
	while (i > 0 && table->buf[i] == NULL) i--;
	if (i < 0) i = table->total_len - 1;
    } else if(ev->keyval == GDK_Right || ev->keyval == GDK_l) {
	i++;
	while (i < table->total_len && table->buf[i] == NULL) i++;
	if (i == table->total_len) i = 0;
    } else if(ev->keyval == GDK_Up || ev->keyval == GDK_k) {
	if(y == 0)
	    y = table->row -1;
	else y--;
	i = x + y*table->col;
	if (i >= table->total_len) i = table->total_len - 1;
    } else if(ev->keyval == GDK_Down || ev->keyval == GDK_j) {
	if(y == table->row -1) y = 0;
	else y++;
	i = x + y*table->col;
	if (i >= table->total_len) i = table->total_len - 1;
    } else if(ev->keyval == GDK_space || ev->keyval == GDK_Return) {
	char_table_selected(table);
    } else
	    return FALSE;
    x = i % table->col;
    y = i / table->col;
    if (char_table_get_position1(table, x, y, &x, &y) && (table->current_x != x || table->current_y != y)) {
	char_table_item_unselect(table);
	table->current_x = x;
	table->current_y = y;
	char_table_item_select(table);
    }
    return TRUE;
}

void
char_table_set_prelight(CharTable *table, gint x, gint y)
{
    if (!GTK_WIDGET_REALIZED(table)) {
	table->current_x = x;
	table->current_y = y;
	return;
    }
    char_table_item_unselect(table);
    table->current_x = x;
    table->current_y = y;
    char_table_item_select(table);
}

void
char_table_selected(CharTable *table)
{
    gtk_signal_emit(GTK_OBJECT(table), char_table_signals[SELECTED],
	table->buf[table->current_x+table->col*table->current_y]);
}

static void
char_table_finalize(GtkObject *object)
{
    int i;
    CharTable *table;

    g_return_if_fail(object != NULL);
    table = (CharTable *)object;
    for (i=0;i<table->total_len;i++) {
	if (table->buf[i]) g_free(table->buf[i]);
    }
    g_free(table->buf);

    if (GTK_OBJECT_CLASS (parent_class)->finalize)
	(* GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
char_table_size_request(GtkWidget *widget, GtkRequisition *requisition)
{
    CharTable *chartable;
    g_return_if_fail (widget != NULL);

    chartable = CHAR_TABLE(widget);
    requisition->width = chartable->col * chartable->per_width;
    requisition->height = chartable->row * chartable->per_height;
}

static void
char_table_item_select(CharTable *table)
{
    GtkWidget *w = GTK_WIDGET(table);
    int len_item;
    int x, y, width, height;
    GdkRectangle area;
    if(table->current_x < 0 || table->current_y < 0) return;
    if(table->current_x >= table->col || table->current_y >= table->row) return;
    char_table_item_draw(table, table->current_x, table->current_y, TRUE);
    gtk_signal_emit(GTK_OBJECT(table), char_table_signals[MOVED],
    	table->current_x, table->current_y);
}

static void
char_table_item_unselect(CharTable *table)
{
    int len_item;
    int x, y, width, height;
    GdkRectangle area;
    GtkWidget *w = GTK_WIDGET(table);
    if(table->current_x < 0 || table->current_y < 0) return;
    char_table_item_draw(table, table->current_x, table->current_y, FALSE);
}

static void
char_table_item_draw(CharTable *table, int x, int y, gboolean selected)
{
    int len_item, offset;
    GtkWidget *w = GTK_WIDGET(table);
    char *buf;
    int px, py, width, height;
    GdkRectangle area;
    GdkGC *gc;

    if (x+y*table->col >= table->total_len) return;

    if (!w->style) return;

    buf = table->buf[x+y*table->col];
    if (buf == NULL) return;
    len_item = util_get_mb_strlen(buf,-1);
    offset = PAD*(len_item-1);
    px = table->per_width * x;
    py = table->per_height * y;
    width = table->per_width * len_item;
    height = table->per_height;
    area.x = 0; area.y = 0;
    area.width = table->per_width * table->col;
    area.height = table->per_height * table->row;
    gdk_window_clear_area(w->window, px, py, width, height);

    if (selected) {
	gc = w->style->fg_gc[GTK_STATE_PRELIGHT];
	gtk_paint_box (w->style, w->window, GTK_STATE_PRELIGHT,
	    GTK_SHADOW_OUT, &area, w, "button", px, py, width, height);
    } else {
	gc = w->style->fg_gc[GTK_STATE_NORMAL];
	gtk_paint_box (w->style, w->window, GTK_STATE_NORMAL,
	    GTK_SHADOW_IN, &area, w, "buttondefault", px, py, width, height);
    }

    gdk_draw_text(w->window, w->style->font, gc,
	table->per_width * x + PAD + offset,
	table->per_height * y + PAD + w->style->font->ascent,
	buf, strlen(buf));
}
