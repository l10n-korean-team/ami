#ifndef __AUTOMATA_H__
#define __AUTOMATA_H__

#include "amitype.h"

void ami_hangul_input_clear(void);
int  ami_hangul_input_process(char *buf, char *c);
void ami_hangul_keyboard_set(int keyboard);
int  ami_hangul_keyboard_get(void);
int ami_hangul_is_first_han(char *buf, int x);
int ami_hangul_is_second_han(char *buf, int x);
void ami_hangul_state_put(HangulState *han);
void ami_hangul_state_get(HangulState *han);
void ami_hangul_check_bufsize(HangulState *han, int inc);
void ami_hangul_ic_change(IC *ic);
void ami_hangul_state_init(HangulState *han);

#endif /* __AUTOMATA_H__ */
