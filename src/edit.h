#ifndef __EDIT_H
#define __EDIT_H
#include "amitype.h"

char * editing_mbstocts(char *s);
int editing_commit(IC *ic, char *buf);
int editing_reset(IC *ic);
void editing_toggle(IC *ic);
int editing_move(IC *ic, int howmuch);
void editing_correct(IC *ic, guchar *buf);
void editing_insert(IC *ic, guchar *buf, int size);
int editing_backward_delete(IC *ic);
int editing_forward_delete(IC *ic);
void editing_correct_insert(IC *ic, guchar *buf);
void editing_hanja_replace(IC *ic, guchar *buf);
int editing_flush(IC *ic);
int editing_flush_without_reset(IC *ic);
int editing_flush_with_str(IC *ic, char *s);
void editing_clear_temp(IC *ic);
void editing_status_win_update(IC *ic);
void editing_edit_win_update(IC *ic);
void editing_send_caret_position(IC *ic);

#endif
