#ifndef __AMI_CONF_H
#define __AMI_CONF_H

typedef enum {STRING, INT, BOOLEAN, KEYSYM} ConfigType;

typedef struct _ConfigDesc {
    guchar *key;
    ConfigType type;
    gpointer val_pointer;
} ConfigDesc;

typedef struct _ConfigItem {
    guchar *key;
    guchar *desc;
    ConfigType type;
    union {
	guchar **string;
	gint *integer;
	gboolean *boolean;
	XIMTriggerKey **keys;
    } val;
} ConfigItem;

gboolean ami_config_init(void);
gboolean ami_config_save_file(char *file);
gboolean ami_config_load_file(char *file, ConfigDesc *ami_config_type);

#endif /* AMI_CONF_H */
