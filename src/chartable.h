#ifndef _CHAR_TABLE_H__
#define _CHAR_TABLE_H__

#include <gdk/gdk.h>
#include <gtk/gtkwidget.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define CHAR_TABLE(obj)	GTK_CHECK_CAST(obj, char_table_get_type(), CharTable)
#define CHAR_TABLE_CLASS(klass) GTK_CHECK_CLASS_CAST (klass, char_table_get_type(), CharTableClass)
#define IS_CHAR_TABLE(obj) GTK_CHECK_TYPE(obj, char_table_get_type())

typedef struct _CharTable	CharTable;
typedef struct _CharTableClass CharTableClass;

struct _CharTable {
	GtkWidget widget;
	gchar **buf;
	gint per_width;
	gint per_height;
	gint current_x;
	gint current_y;
	gint col;
	gint row;
	int total_len;
};

struct _CharTableClass {
	GtkWidgetClass parent_class;
	void (*selected) (CharTable *chartable,
		gchar *text);
	void (*moved) (CharTable *chartable,
		gint x, gint y);
};

guint char_table_get_type(void);
GtkWidget *char_table_new(char **buf, int col, int row);
char *char_table_get_char(CharTable *table);
void char_table_selected(CharTable *table);
void char_table_set_prelight(CharTable *table, gint x, gint y);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __CHAR_TABLE_H__ */


