#ifndef __APPLET_H
#define __APPLET_H

extern void draw_applet_image(gint hangul_mode);

extern int ami_wm_main(int argc, char *argv[], char *rc_file);
void applet_change_pixmap_dir(char *name);

#ifdef GNOME_APPLET
extern int ami_gnome_main(int argc, char *argv[], char* rc_file);
#endif

#endif /* __APPLET_H */
