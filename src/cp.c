/* this file is a part of Ami software, (C) Hwang chi-deok 1999, 2000, 2001, 2002 */

#include "config.h"
#include "cp.h"
#include "ami.h"
#include "hinput.h"
#include "conf.h"
#include "util.h"
#include "keyparse.h"
#include "applet.h"
#include "comp.h"
#include <dirent.h>

static GList *activate_list = NULL;

static void about_ami(void);
static void warn_no_effect(char *message);
static gint cp_delete( GtkWidget *widget, void *data );
static GtkTooltips *tooltip = NULL;
static guint update_id = 0;



static void
keyboard_toggle_cb(GtkWidget *w, gpointer data)
{
    if (GTK_TOGGLE_BUTTON(w)->active) {
	keyboard_name = data;
	composer_set_keymap(keyboard_name);
    }
}

static void
editing_mode_toggle_cb(GtkWidget *w, gpointer data)
{
    if (GTK_TOGGLE_BUTTON(w)->active) {
	ami_editing_mode = GPOINTER_TO_INT(data);
    } 
}

static void
hanja_subst_mode_toggle_cb(GtkWidget *w, gpointer data)
{
    if (GTK_TOGGLE_BUTTON(w)->active) {
	ami_hanja_subst_mode = ami_hanja_def_subst_mode = GPOINTER_TO_INT(data);
    }
}

static void
esc_mode_toggle_cb(GtkWidget *w, gpointer data)
{
    esc_han_toggle = GTK_TOGGLE_BUTTON(w)->active;
}

static void
shuffle_mode_toggle_cb(GtkWidget *w, gpointer data)
{
    allow_shuffle = GTK_TOGGLE_BUTTON(w)->active;
    composer_set_shuffle_active(allow_shuffle);
}

static void
unique_han_toggle_cb(GtkWidget *w, gpointer data)
{
    warn_no_effect(_("This change will get effective only after you relaunch Ami."));
    unique_han = GTK_TOGGLE_BUTTON(w)->active;
}

static void
entry_changed(GtkWidget *w)
{
    gtk_object_set_data(GTK_OBJECT(w), "changed", GINT_TO_POINTER(1));
}

static void
activate_cb(GtkWidget *w)
{
    gtk_signal_emit(GTK_OBJECT(w), update_id);
}

static void
key_setting_activate(GtkWidget *w, char *kind)
{
    char *key;
    if (!gtk_object_get_data(GTK_OBJECT(w), "changed")) return;
    gtk_object_set_data(GTK_OBJECT(w), "changed", NULL);
    key = gtk_entry_get_text(GTK_ENTRY(w));
    if (!strcmp(kind, "KorEng")) {
	make_trigger_keys(&trigger_keys, key);
	warn_no_effect(_("The toggle key for Korean and English kbd. will get effective only after relaunching Ami."));
    } else if (!strcmp(kind, "Hanja")) {
	make_trigger_keys(&hanja_keys, key);
    } else if (!strcmp(kind, "SpecialChar")) {
	make_trigger_keys(&special_char_keys, key);
    } else if (!strcmp(kind, "BufferClean")) {
	make_trigger_keys(&flush_keys, key);
    }
}

static void
label_setting_activate(GtkEntry *w, gchar **label)
{
    if (!gtk_object_get_data(GTK_OBJECT(w), "changed")) return;
    gtk_object_set_data(GTK_OBJECT(w), "changed", NULL);
    g_free(*label);
    *label = gtk_entry_get_text(w);
}

static void
support_status_toggled(GtkWidget *w)
{
    warn_no_effect(_("En/Disabling the status window gets effective only after relaunching Ami."));
    support_status = GTK_TOGGLE_BUTTON(w)->active;
}

static void
underline_mode_toggled(GtkWidget *w)
{
    ami_use_underline = GTK_TOGGLE_BUTTON(w)->active;
}

static void
bell_changed(GtkAdjustment *adj)
{
    bell_intensity = adj->value;
}

static void
save_conf(GtkWidget *w, gpointer data)
{
    GList *list = activate_list;
    while(list) {
	gtk_signal_emit(GTK_OBJECT(list->data), update_id);
	list = list->next;
    }
    ami_config_save_file(NULL);
}

static GtkWidget *
create_keyboard_setting(void)
{
    /* set up a keyboard */
    GtkWidget *vbox;
    GtkWidget *button;
    GSList *group;
    int keyboard;
    char **kbd_names;
    int num_kbd;
    int i;

    vbox = gtk_vbox_new(FALSE, 2);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);
    kbd_names = composer_get_keyboard_list(KEYBOARD_DIR, &num_kbd);
    composer_set_keymap(keyboard_name);
    group = NULL;
    for(i=0;i<num_kbd;i++) {
	button = gtk_radio_button_new_with_label(group, kbd_names[i]);
	gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 4);
	group = gtk_radio_button_group(GTK_RADIO_BUTTON(button));
	if (strcmp(keyboard_name, kbd_names[i]) == 0)
	    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button), TRUE);
	gtk_signal_connect(GTK_OBJECT(button), "toggled", 
	    (GtkSignalFunc)keyboard_toggle_cb, kbd_names[i]);
    }
    return vbox;
}

static GtkWidget *
create_input_setting(void)
{
    GtkWidget *vbox0, *vbox1, *frame, *hbox;
    GtkWidget *wbutton, *swbutton, *cbutton;
    GtkWidget *esc;
    GtkWidget *b;
    GSList *group;
    gchar **hanja_formats;
    GString *fmts_xlat;
    int i;

    vbox0 = gtk_vbox_new(FALSE, 2);
    gtk_container_set_border_width(GTK_CONTAINER(vbox0), 5);

    frame = gtk_frame_new(_("Input Method"));
    gtk_box_pack_start(GTK_BOX(vbox0), frame, FALSE, FALSE, 0);

    vbox1 = gtk_vbox_new(TRUE, 2);
    gtk_container_add(GTK_CONTAINER(frame), vbox1);

    wbutton = gtk_radio_button_new_with_label(NULL, _("Commit by words: Cursor movement processed by Ami"));
    gtk_container_add(GTK_CONTAINER(vbox1), wbutton);
    swbutton = gtk_radio_button_new_with_label(
	gtk_radio_button_group(GTK_RADIO_BUTTON(wbutton)), _("Commit by words: Cursor movement delegated to clients"));
    gtk_container_add(GTK_CONTAINER(vbox1), swbutton);
    cbutton = gtk_radio_button_new_with_label(
	gtk_radio_button_group(GTK_RADIO_BUTTON(swbutton)), _("Commit by characters"));
    gtk_container_add(GTK_CONTAINER(vbox1), cbutton);
    if (ami_editing_mode == AMI_EDITING_WORD_MODE) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(wbutton), TRUE);
    } else if (ami_editing_mode == AMI_EDITING_SIMPLE_WORD_MODE) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(swbutton), TRUE);
    } else {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(cbutton), TRUE);
    }

    gtk_signal_connect(GTK_OBJECT(wbutton), "toggled", (GtkSignalFunc)editing_mode_toggle_cb, GINT_TO_POINTER(AMI_EDITING_WORD_MODE));
    gtk_signal_connect(GTK_OBJECT(swbutton), "toggled", (GtkSignalFunc)editing_mode_toggle_cb, GINT_TO_POINTER(AMI_EDITING_SIMPLE_WORD_MODE));
    gtk_signal_connect(GTK_OBJECT(cbutton), "toggled", (GtkSignalFunc)editing_mode_toggle_cb, GINT_TO_POINTER(AMI_EDITING_CHAR_MODE));

    frame = gtk_frame_new(_("Hanja Conversion Mode"));
    gtk_box_pack_start(GTK_BOX(vbox0), frame, FALSE, FALSE, 0);
    hbox = gtk_hbox_new(FALSE, 0);
    gtk_container_add(GTK_CONTAINER(frame), hbox);

    fmts_xlat = util_xlate_hanja_fmts();
    hanja_formats = g_strsplit(fmts_xlat->str, ":", -1);
    group = NULL;
    for(i=0;hanja_formats[i] != NULL; i++) {
	b = gtk_radio_button_new_with_label(group, hanja_formats[i]);
	group = gtk_radio_button_group(GTK_RADIO_BUTTON(b));
	gtk_container_add(GTK_CONTAINER(hbox), b);
	if (ami_hanja_def_subst_mode == (i+1))
	    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(b), TRUE);
	gtk_signal_connect(GTK_OBJECT(b), "toggled", 
		(GtkSignalFunc)hanja_subst_mode_toggle_cb, 
		GINT_TO_POINTER(i+1));
    }
    g_strfreev(hanja_formats);
    g_string_free(fmts_xlat, TRUE);

    esc = gtk_check_button_new_with_label(_("On <ESC>, switch to English mode"));
    gtk_box_pack_start(GTK_BOX(vbox0), esc, FALSE, FALSE, 5);
    if (esc_han_toggle) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(esc), TRUE);
    }
    gtk_signal_connect(GTK_OBJECT(esc), "toggled", (GtkSignalFunc)esc_mode_toggle_cb, NULL);

    esc = gtk_check_button_new_with_label(_("Keep KOR/ENG mode across clients"));
    gtk_box_pack_start(GTK_BOX(vbox0), esc, FALSE, FALSE, 5);
    if (unique_han) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(esc), TRUE);
    }
    gtk_signal_connect(GTK_OBJECT(esc), "toggled", (GtkSignalFunc)unique_han_toggle_cb, NULL);
    gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltip), esc, _("To get this change effective, you have to relaunch Ami."), NULL);

    esc = gtk_check_button_new_with_label(_("Intelligent handling of misplaced Jamos"));
    gtk_box_pack_start(GTK_BOX(vbox0), esc, FALSE, FALSE, 5);
    if (allow_shuffle) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(esc), TRUE);
    }
    gtk_signal_connect(GTK_OBJECT(esc), "toggled", (GtkSignalFunc)shuffle_mode_toggle_cb, NULL);
    gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltip), esc, _("When a vowel is followed by a (leading) consonant, treat the pair as a consonant followed by a vowel."), NULL);

    return vbox0;
}

static void
pix_changed(GtkWidget *w)
{
    char * name = gtk_object_get_data(GTK_OBJECT(w), "directory");
    applet_change_pixmap_dir(name);
}

static gboolean
is_valid_pixdir(char *dir)
{
    char *s;
    s = g_strconcat(dir, "/ami-h.xpm", NULL);
    if (access(s, R_OK) != 0) goto error;
    g_free(s);
    s = g_strconcat(dir, "/ami-e.xpm", NULL);
    if (access(s, R_OK) != 0) goto error;
    g_free(s);
    s = g_strconcat(dir, "/ami-d.xpm", NULL);
    if (access(s, R_OK) != 0) goto error;
    g_free(s);
    return TRUE;
    error:
    g_free(s);
    return FALSE;
}

static GList *
get_pixmap_dir_list_sub(char *parent_dir, GList *list)
{
    DIR *pix_dir;
    struct dirent *entry;
    pix_dir = opendir(parent_dir);
    if (!pix_dir) return list;
    while((entry=readdir(pix_dir))) {
	char *s;
        if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
	    continue;
        s = g_strconcat(parent_dir, "/", entry->d_name, NULL);
        if (!is_valid_pixdir(s)) {
	    g_free(s);
	    continue;
        }
        list = g_list_prepend(list, s);
    } 
    closedir(pix_dir);
    return list;
}

static char **
get_pixmap_dir_list(void)
{
    char * pix_dir_name = g_strconcat(g_get_home_dir(), "/.ami", NULL);
    GList *list = NULL;
    char **dir_list;
    int i, n;

    list = get_pixmap_dir_list_sub(pix_dir_name, list);
    g_free(pix_dir_name);
    pix_dir_name = g_strconcat(XPM_DIR, "/pix", NULL);
    list = get_pixmap_dir_list_sub(pix_dir_name, list);
    g_free(pix_dir_name);
    if (!list) return NULL;
    n = g_list_length(list);
    dir_list = g_new(char *, n+1);
    for(i=0;i<n;i++) dir_list[i] = g_list_nth(list, i)->data;
    dir_list[n] = NULL;
    g_list_free(list);
    return dir_list;
}


static GtkWidget *
create_appearance_setting(void)
{
    GtkWidget *vbox, *frame, *hbox, *status;
    GtkWidget *l, *entry, *under;
    GtkWidget *bell_box, *bell_label, *bell_scale;
    GtkObject *adj;
    char **pixmap_dirs;

    vbox = gtk_vbox_new(FALSE, 2);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

    status = gtk_check_button_new_with_label(_("Make status window"));
    gtk_box_pack_start(GTK_BOX(vbox), status, FALSE, FALSE, 5);
    if (support_status) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(status), TRUE);
    }
    gtk_signal_connect(GTK_OBJECT(status), "toggled", 
    	(GtkSignalFunc)support_status_toggled, NULL);
    gtk_tooltips_set_tip(GTK_TOOLTIPS(tooltip), status, _("To get this change effective, you have to relaunch Ami"), NULL);

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
    l = gtk_label_new(_("Korean Input Mode Label"));
    gtk_box_pack_start(GTK_BOX(hbox), l, FALSE, FALSE, 0);
    entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(entry), hangul_mode_label);
    gtk_widget_set_usize(entry, 100, -1);
    gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
    gtk_signal_connect(GTK_OBJECT(entry), "activate", 
    	(GtkSignalFunc)activate_cb, NULL);
    gtk_signal_connect(GTK_OBJECT(entry), "update", 
    	(GtkSignalFunc)label_setting_activate, &hangul_mode_label);
    gtk_signal_connect(GTK_OBJECT(entry), "changed",
    	(GtkSignalFunc)entry_changed, NULL);
    activate_list = g_list_prepend(activate_list, entry);

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
    l = gtk_label_new(_("English Input Mode Label"));
    gtk_box_pack_start(GTK_BOX(hbox), l, FALSE, FALSE, 0);
    entry = gtk_entry_new();
    gtk_entry_set_text(GTK_ENTRY(entry), english_mode_label);
    gtk_widget_set_usize(entry, 100, -1);
    gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
    gtk_signal_connect(GTK_OBJECT(entry), "update", 
    	(GtkSignalFunc)label_setting_activate, &english_mode_label);
    gtk_signal_connect(GTK_OBJECT(entry), "activate", 
    	(GtkSignalFunc)activate_cb, NULL);
    gtk_signal_connect(GTK_OBJECT(entry), "changed",
    	(GtkSignalFunc)entry_changed, NULL);
    activate_list = g_list_prepend(activate_list, entry);

    under = gtk_check_button_new_with_label(_("Use underline during preediting"));
    gtk_box_pack_start(GTK_BOX(vbox), under, FALSE, FALSE, 5);
    if (ami_use_underline) {
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(under), TRUE);
    }
    gtk_signal_connect(GTK_OBJECT(under), "toggled", (GtkSignalFunc)underline_mode_toggled, NULL);
    bell_box = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(vbox), bell_box, FALSE, FALSE, 5);
    bell_label = gtk_label_new(_("Bell volume"));
    adj = gtk_adjustment_new (0.0, -100, 110, 1, 10, 10);
    gtk_signal_connect(adj, "value-changed", (GtkSignalFunc)bell_changed, NULL);
    bell_scale = gtk_hscale_new((GtkAdjustment*)adj);
    gtk_range_set_update_policy (GTK_RANGE (bell_scale), GTK_UPDATE_DELAYED);
    gtk_scale_set_digits (GTK_SCALE (bell_scale), 0);
    gtk_scale_set_draw_value (GTK_SCALE (bell_scale), TRUE);
    gtk_box_pack_start(GTK_BOX(bell_box), bell_label, FALSE, FALSE, FALSE);
    gtk_container_add(GTK_CONTAINER(bell_box), bell_scale);

    if ( ami_run_mode != AMI_NORMAL_RUN && (pixmap_dirs=get_pixmap_dir_list())) {
	GtkWidget *omenu, *menu, *menu_item;
	GtkWidget *label, *hbox;
	GSList *group;
	int selected, i;
	if (!pix_dir_name) pix_dir_name = g_strdup("");
	selected = -1;
	i = 0;
	while (pixmap_dirs[i]) {
	    if (strcmp(pixmap_dirs[i], pix_dir_name) == 0) {
		selected = i;
		break;
	    }
	    i++;
	}
	omenu = gtk_option_menu_new();
	menu = gtk_menu_new();
	group = NULL;
	menu_item = gtk_radio_menu_item_new_with_label(group, _("Default"));
	gtk_signal_connect(GTK_OBJECT(menu_item), "activate", (GtkSignalFunc)pix_changed, NULL);
	gtk_menu_append(GTK_MENU(menu), menu_item);
	if (selected ==  -1)
	  gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_item), TRUE);
	gtk_widget_show(menu_item);
	i = 0;
	while(pixmap_dirs[i]) {
	    menu_item = gtk_radio_menu_item_new_with_label(group, pixmap_dirs[i]);
	    gtk_signal_connect(GTK_OBJECT(menu_item), "activate", (GtkSignalFunc)pix_changed, NULL);
	    gtk_menu_append(GTK_MENU(menu), menu_item);
	    if (selected ==  i)
		gtk_check_menu_item_set_active(GTK_CHECK_MENU_ITEM(menu_item), TRUE);
	    gtk_widget_show(menu_item);
	    gtk_object_set_data(GTK_OBJECT(menu_item), "directory", pixmap_dirs[i]);
	    i++;
	}
	gtk_option_menu_set_menu(GTK_OPTION_MENU(omenu), menu);
	gtk_option_menu_set_history(GTK_OPTION_MENU(omenu), selected + 1);
	hbox = gtk_hbox_new(FALSE, 2);
	label = gtk_label_new(_("Icon"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), omenu, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
    }

    return vbox;
}

static GtkWidget *
create_key_setting(void)
{
    GtkWidget *vbox, *frame, *hbox;
    GtkWidget *l, *entry;
    gchar *key_desc;

    vbox = gtk_vbox_new(FALSE, 2);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 5);

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
    l = gtk_label_new(_("Kor-Eng. mode Toggle"));
    gtk_box_pack_start(GTK_BOX(hbox), l, FALSE, FALSE, 0);
    entry = gtk_entry_new();
    key_desc = trigger_keys_to_str(trigger_keys);
    gtk_entry_set_text(GTK_ENTRY(entry), key_desc);
    gtk_widget_set_usize(entry, 150, -1);
    gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
    gtk_signal_connect(GTK_OBJECT(entry), "activate", 
    	(GtkSignalFunc)activate_cb, NULL);
    gtk_signal_connect(GTK_OBJECT(entry), "update", 
    	(GtkSignalFunc)key_setting_activate, "KorEng");
    gtk_signal_connect(GTK_OBJECT(entry), "changed",
    	(GtkSignalFunc)entry_changed, NULL);
    activate_list = g_list_prepend(activate_list, entry);

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
    l = gtk_label_new(_("Conversion to Hanja"));
    gtk_box_pack_start(GTK_BOX(hbox), l, FALSE, FALSE, 0);
    entry = gtk_entry_new();
    key_desc = trigger_keys_to_str(hanja_keys);
    gtk_entry_set_text(GTK_ENTRY(entry), key_desc);
    gtk_widget_set_usize(entry, 150, -1);
    gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
    gtk_signal_connect(GTK_OBJECT(entry), "activate", 
    	(GtkSignalFunc)activate_cb, NULL);
    gtk_signal_connect(GTK_OBJECT(entry), "update", 
    	(GtkSignalFunc)key_setting_activate, "Hanja");
    gtk_signal_connect(GTK_OBJECT(entry), "changed",
    	(GtkSignalFunc)entry_changed, NULL);
    activate_list = g_list_prepend(activate_list, entry);

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
    l = gtk_label_new(_("Symbol Input"));
    gtk_box_pack_start(GTK_BOX(hbox), l, FALSE, FALSE, 0);
    entry = gtk_entry_new();
    key_desc = trigger_keys_to_str(special_char_keys);
    gtk_entry_set_text(GTK_ENTRY(entry), key_desc);
    gtk_widget_set_usize(entry, 150, -1);
    gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
    gtk_signal_connect(GTK_OBJECT(entry), "activate", 
    	(GtkSignalFunc)activate_cb, NULL);
    gtk_signal_connect(GTK_OBJECT(entry), "update", 
    	(GtkSignalFunc)key_setting_activate, "SpecialChar");
    gtk_signal_connect(GTK_OBJECT(entry), "changed",
    	(GtkSignalFunc)entry_changed, NULL);
    activate_list = g_list_prepend(activate_list, entry);

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 2);
    l = gtk_label_new(_("Clear Buffer"));
    gtk_box_pack_start(GTK_BOX(hbox), l, FALSE, FALSE, 0);
    entry = gtk_entry_new();
    key_desc = trigger_keys_to_str(flush_keys);
    gtk_entry_set_text(GTK_ENTRY(entry), key_desc);
    gtk_widget_set_usize(entry, 150, -1);
    gtk_box_pack_end(GTK_BOX(hbox), entry, FALSE, FALSE, 0);
    gtk_signal_connect(GTK_OBJECT(entry), "activate", 
    	(GtkSignalFunc)activate_cb, NULL);
    gtk_signal_connect(GTK_OBJECT(entry), "update", 
    	(GtkSignalFunc)key_setting_activate, "BufferClean");
    gtk_signal_connect(GTK_OBJECT(entry), "changed",
    	(GtkSignalFunc)entry_changed, NULL);
    activate_list = g_list_prepend(activate_list, entry);

    return vbox;
}


GtkWidget *
create_cp(void)
{
    GtkWidget *notebook;
    GtkWidget *vbox, *hbox;
    GtkWidget *b;
    GtkWidget *sep;

    tooltip = gtk_tooltips_new();
    vbox = gtk_vbox_new(FALSE, 10);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 10);
    notebook = gtk_notebook_new();
    gtk_container_add(GTK_CONTAINER(vbox), notebook);
    update_id = gtk_object_class_user_signal_new(gtk_type_class (GTK_TYPE_ENTRY),
    	"update", 0, gtk_marshal_NONE__NONE,
	GTK_TYPE_NONE, 0);
    {
	/* set up a keyboard */
	GtkWidget *keyw, *keylabel;
	keylabel = gtk_label_new(_("KBD Type"));
	keyw = create_keyboard_setting();
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), keyw, keylabel);
    }
    {
	GtkWidget *inputw, *inputlabel;
	inputlabel = gtk_label_new(_(_("Input Options")));
	inputw = create_input_setting();
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), inputw, inputlabel);
    }
    {
	GtkWidget *keyw, *keylabel;
	keylabel = gtk_label_new(_("Hot Keys"));
	keyw = create_key_setting();
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), keyw, keylabel);
    }
    {
	GtkWidget *box, *label;
	label = gtk_label_new(_("Appearance"));
	box = create_appearance_setting();
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), box, label);
    }

    sep = gtk_hseparator_new();
    gtk_container_add(GTK_CONTAINER(vbox), sep);

    {
	GtkWidget *h, *dummy;
	h = gtk_hbox_new(FALSE, 0);
	gtk_container_add(GTK_CONTAINER(vbox), h);
	gtk_container_set_border_width(GTK_CONTAINER(h), 0);
	dummy = gtk_label_new("");
	gtk_box_pack_start(GTK_BOX(h), dummy, TRUE, TRUE, 0);

	hbox = gtk_hbox_new(TRUE, 3);
	gtk_box_pack_end(GTK_BOX(h), hbox, FALSE, FALSE, 0);
    }

    b = gtk_button_new_with_label(_("About.."));
    gtk_container_add(GTK_CONTAINER(hbox), b);
    gtk_signal_connect(GTK_OBJECT(b), "clicked", (GtkSignalFunc)about_ami, NULL);

    b = gtk_button_new_with_label(_("Save Changes"));
    gtk_container_add(GTK_CONTAINER(hbox), b);
    gtk_signal_connect(GTK_OBJECT(b), "clicked", (GtkSignalFunc)save_conf, NULL);

    if (ami_run_mode == AMI_NORMAL_RUN) {
	b = gtk_button_new_with_label(_("Exit"));
	gtk_container_add(GTK_CONTAINER(hbox), b);
	gtk_signal_connect(GTK_OBJECT(b), "clicked", (GtkSignalFunc)cp_quit, NULL);
    } else {
	b = gtk_button_new_with_label(_("Close"));
	gtk_container_add(GTK_CONTAINER(hbox), b);
	gtk_signal_connect(GTK_OBJECT(b), "clicked", (GtkSignalFunc)cp_delete, NULL);
    }

    gtk_widget_show_all(vbox);

    return vbox;
}

static void
about_ami(void)
{
    GtkWidget *win, *label;
    GString *str;

    win = gtk_window_new(GTK_WINDOW_DIALOG);
    gtk_window_set_title(GTK_WINDOW(win), _("About Ami"));
    gtk_container_set_border_width(GTK_CONTAINER(win), 5);
    str = g_string_new(NULL);
    g_string_sprintf(str, _("Ami Version %s\n"), VERSION);
    g_string_append(str, 
	_("\nAmi written by HWANG Chi-deok <hwang@mizi.co.kr> "
	"built upon gtk+\n is a free Korean X input method server\n"
	"distributed under GNU General Public License V2. \n\n"
        "A special thanks goes to Peter who began the gtk+ project\n"
			  "and Owen who's made it as it is now.\n"
	"Please, visit <http://www.gtk.org> for more info."));
    label = gtk_label_new(str->str);
    g_string_free(str, TRUE);
    gtk_container_add(GTK_CONTAINER(win), label);
    gtk_widget_show_all(win);
}

static void warn_no_effect(char *message)
{
    /* FIXME */
    g_print("%s\n", message);
}

void
cp_quit(void)
{
    IMCloseIM(xims);
    xims = NULL;
    gdk_flush(); /* ??? */
    gtk_main_quit();
}

static gint cp_delete( GtkWidget *widget, void *data )
{
    gtk_widget_hide(cp_win); /* hide cp_win */
    return TRUE;
}
